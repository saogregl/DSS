#include "DSSPhysicsBody.h"


DSSPhysicsBody::DSSPhysicsBody() : angularOffset(0),
								   lagrangeOffset(0),
								   positionOffset(0)
{
	bodyVelocity.orientation = Eigen::Quaternion<double>(0, 0, 0, 0);
	bodyVelocity.position = Eigen::Vector3d(0, 0, 0);
	bodyAcceleration.orientation = Eigen::Quaternion<double>(0, 0, 0, 0);
	bodyAcceleration.position = Eigen::Vector3d(0, 0, 0);

}


DSSPhysicsBody::~DSSPhysicsBody() = default;


Eigen::Vector3d DSSPhysicsBody::getVelocity() const
{
	return bodyVelocity.position;
}

Eigen::Vector3d DSSPhysicsBody::getAcceleration() const
{
	return bodyAcceleration.position;
}


void DSSPhysicsBody::setPosition(const Eigen::Vector3d& _position)
{
	bodyPosition.position = _position;
}

void DSSPhysicsBody::transformChildToParent(const DSSSpatialDescriptor & frameToTransformLocal,
												  DSSSpatialDescriptor & resultingFrameParent) const
{

	//Transform the position of the coordinate frame to the parent
	Eigen::Vector3d position =
		DSSTransform::transformChildToParent(frameToTransformLocal.getPosition(),
		                                     this->bodyPosition.position,
		                                     this->bodyPosition.orientation);

	//set the new position and orientation as vector+quat
	resultingFrameParent.setCoordinateSystem(position,this->bodyPosition.orientation * frameToTransformLocal.orientation);
	//update rotationMatrix
}


void DSSPhysicsBody::transformParentToChild(const DSSSpatialDescriptor &frameToTransformLocal,
                                            DSSSpatialDescriptor &resultingFrameParent) const {

    Eigen::Vector3d position =
            DSSTransform::transformParentToChild(frameToTransformLocal.getPosition(),
                                                 this->bodyPosition.position,
                                                 this->bodyPosition.orientation.toRotationMatrix());

    //set the new position and orientation as vector+quat
    resultingFrameParent.setCoordinateSystem(position,
                                             this->bodyPosition.orientation * frameToTransformLocal.orientation);
    //update rotationMatrix

}


void DSSPhysicsBody::syncRotationMatrix()
{
	rotationMatrix = bodyPosition.orientation.toRotationMatrix();
}


int DSSPhysicsBody::getOffp() const
{
	return positionOffset;
}

int DSSPhysicsBody::getOffw() const
{
	return angularOffset;
}

int DSSPhysicsBody::getOffl() const
{
	return lagrangeOffset;
}

void DSSPhysicsBody::setOffp(const int _offp)
{
	positionOffset = _offp;
}

void DSSPhysicsBody::setOffw(const int _offw)
{
	angularOffset = _offw;
}

void DSSPhysicsBody::setOffl(const int _offl)
{
	lagrangeOffset = _offl;
}

Eigen::Vector3d DSSPhysicsBody::getOmegaL() const
{
	Eigen::Quaterniond tempQuat = this->bodyVelocity.getOrientation();
	Eigen::Matrix<double, 3, 4> Gmatrix;
	Eigen::Vector3d omegaLocal;
	DSSTransform::buildQuatGMatrix(this->bodyPosition.orientation, Gmatrix);

	omegaLocal(0, 0) = Gmatrix(0, 0)*tempQuat.w() + Gmatrix(0, 1)*tempQuat.x() + Gmatrix(0, 2)*tempQuat.y() + Gmatrix(0, 3)*tempQuat.z();
	omegaLocal(1, 0) = Gmatrix(1, 0)*tempQuat.w() + Gmatrix(1, 1)*tempQuat.x() + Gmatrix(1, 2)*tempQuat.y() + Gmatrix(1, 3)*tempQuat.z();
	omegaLocal(2, 0) = Gmatrix(2, 0)*tempQuat.w() + Gmatrix(2, 1)*tempQuat.x() + Gmatrix(2, 2)*tempQuat.y() + Gmatrix(2, 3)*tempQuat.z();


	return (static_cast<double>(2.0) * omegaLocal);
}

Eigen::Vector3d DSSPhysicsBody::getOmegaG() const
{
	Eigen::Quaternion<double> tempQuat = this->bodyVelocity.getOrientation();
	Eigen::Matrix<double, 3, 4> Ematrix;
	Eigen::Vector3d omegaGlobal;

	DSSTransform::buildQuatEMatrix(this->bodyPosition.getOrientation(), Ematrix);

	omegaGlobal(0, 0) = Ematrix(0, 0)*tempQuat.w() + Ematrix(0, 1)*tempQuat.x() + Ematrix(0, 2)*tempQuat.y() + Ematrix(0, 3)*tempQuat.z();
	omegaGlobal(1, 0) = Ematrix(1, 0)*tempQuat.w() + Ematrix(1, 1)*tempQuat.x() + Ematrix(1, 2)*tempQuat.y() + Ematrix(1, 3)*tempQuat.z();
	omegaGlobal(2, 0) = Ematrix(2, 0)*tempQuat.w() + Ematrix(2, 1)*tempQuat.x() + Ematrix(2, 2)*tempQuat.y() + Ematrix(2, 3)*tempQuat.z();


	return (static_cast<double>(2.0) * omegaGlobal);
}

Eigen::Vector3d DSSPhysicsBody::getAlfaL() const
{
	Eigen::Quaternion<double> tempQuat = this->bodyAcceleration.getOrientation();
	Eigen::Matrix<double, 3, 4> Gmatrix;
	Eigen::Vector3d localAlfa;
	DSSTransform::buildQuatGMatrix(this->bodyPosition.getOrientation(), Gmatrix);

	localAlfa(0, 0) = Gmatrix(0, 0)*tempQuat.w() + Gmatrix(0, 1)*tempQuat.x() + Gmatrix(0, 2)*tempQuat.y() + Gmatrix(0, 3)*tempQuat.z();
	localAlfa(1, 0) = Gmatrix(1, 0)*tempQuat.w() + Gmatrix(1, 1)*tempQuat.x() + Gmatrix(1, 2)*tempQuat.y() + Gmatrix(1, 3)*tempQuat.z();
	localAlfa(2, 0) = Gmatrix(2, 0)*tempQuat.w() + Gmatrix(2, 1)*tempQuat.x() + Gmatrix(2, 2)*tempQuat.y() + Gmatrix(2, 3)*tempQuat.z();

	return (static_cast<double>(2.0) * localAlfa);

}

Eigen::Vector3d DSSPhysicsBody::getAlfaG() const
{
	Eigen::Quaternion<double> tempQuat = this->bodyAcceleration.getOrientation();
	Eigen::Matrix<double, 3, 4> Ematrix;
	Eigen::Vector3d alfaGlobal;

	DSSTransform::buildQuatEMatrix(this->bodyPosition.getOrientation(), Ematrix);

	alfaGlobal(0, 0) = Ematrix(0, 0)*tempQuat.w() + Ematrix(0, 1)*tempQuat.x() + Ematrix(0, 2)*tempQuat.y() + Ematrix(0, 3)*tempQuat.z();
	alfaGlobal(1, 0) = Ematrix(1, 0)*tempQuat.w() + Ematrix(1, 1)*tempQuat.x() + Ematrix(1, 2)*tempQuat.y() + Ematrix(1, 3)*tempQuat.z();
	alfaGlobal(2, 0) = Ematrix(2, 0)*tempQuat.w() + Ematrix(2, 1)*tempQuat.x() + Ematrix(2, 2)*tempQuat.y() + Ematrix(2, 3)*tempQuat.z();


	return (static_cast<double>(2.0) * alfaGlobal);

}

void DSSPhysicsBody::setVelocity(const Eigen::Vector3d& _velocity)
{
	bodyVelocity.position = _velocity;
}

void DSSPhysicsBody::setAcceleration(const Eigen::Vector3d& _acceleration)
{
	bodyAcceleration.position = _acceleration;
}

void DSSPhysicsBody::setOmegaL(const Eigen::Vector3d& wl)
{

	const double e0 = this->bodyPosition.orientation.w();
	const double e1 = this->bodyPosition.orientation.x();
	const double e2 = this->bodyPosition.orientation.y();
	const double e3 = this->bodyPosition.orientation.z();
	this->bodyVelocity.orientation.w() = 0.5*(-e1*wl.x() - e2*wl.y() - e3*wl.z());
	this->bodyVelocity.orientation.x() = 0.5*( e0*wl.x() - e3*wl.y() + e2*wl.z());
	this->bodyVelocity.orientation.y() = 0.5*( e3*wl.x() + e0*wl.y() - e1*wl.z());
	this->bodyVelocity.orientation.z() = 0.5*(-e2*wl.x() + e1*wl.y() + e0*wl.z());


}

void DSSPhysicsBody::setOmegaG(const Eigen::Vector3d& wg)
{
	const double e0 = this->bodyPosition.orientation.w();
	const double e1 = this->bodyPosition.orientation.x();
	const double e2 = this->bodyPosition.orientation.y();
	const double e3 = this->bodyPosition.orientation.z();
	this->bodyVelocity.orientation.w() = 0.5*(-e1*wg.x() - e2*wg.y() - e3*wg.z());
	this->bodyVelocity.orientation.x() = 0.5*( e0*wg.x() + e3*wg.y() - e2*wg.z());
	this->bodyVelocity.orientation.y() = 0.5*(-e3*wg.x() + e0*wg.y() + e1*wg.z());
	this->bodyVelocity.orientation.z() = 0.5*( e2*wg.x() - e1*wg.y() + e0*wg.z());

}

void DSSPhysicsBody::setAlfaL(const Eigen::Vector3d& al)
{
	const double e0 = this->bodyPosition.orientation.w();
	const double e1 = this->bodyPosition.orientation.x();
	const double e2 = this->bodyPosition.orientation.y();
	const double e3 = this->bodyPosition.orientation.z();

	this->bodyAcceleration.orientation = this->bodyVelocity.orientation*
		this->bodyPosition.orientation.conjugate()*this->bodyVelocity.orientation;

	this->bodyAcceleration.orientation.w() += 0.5*(-e1*al.x() - e2*al.y() - e3*al.z());
	this->bodyAcceleration.orientation.x() += 0.5*( e0*al.x() - e3*al.y() + e2*al.z());
	this->bodyAcceleration.orientation.y() += 0.5*( e3*al.x() + e0*al.y() - e1*al.z());
	this->bodyAcceleration.orientation.z() += 0.5*(-e2*al.x() + e1*al.y() + e0*al.z());;

}

void DSSPhysicsBody::setAlfaG(const Eigen::Vector3d& ag)
{
	const double e0 = this->bodyPosition.orientation.w();
	const double e1 = this->bodyPosition.orientation.x();
	const double e2 = this->bodyPosition.orientation.y();
	const double e3 = this->bodyPosition.orientation.z();

	this->bodyAcceleration.orientation = this->bodyVelocity.orientation*
		this->bodyPosition.orientation.conjugate()*this->bodyVelocity.orientation;

	this->bodyAcceleration.orientation.w() += 0.5*(-e1*ag.x() - e2*ag.y() - e3*ag.z());
	this->bodyAcceleration.orientation.x() += 0.5*( e0*ag.x() + e3*ag.y() - e2*ag.z());
	this->bodyAcceleration.orientation.y() += 0.5*(-e3*ag.x() + e0*ag.y() + e1*ag.z());
	this->bodyAcceleration.orientation.z() += 0.5*( e2*ag.x() - e1*ag.y() + e0*ag.z());

}

