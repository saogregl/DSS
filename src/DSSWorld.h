#pragma once

#include "Core/DSSAssembly.h"
#include "Core/DSSSolverInterface.h"
#include "DSSRigidBody.h"
#include <memory>
#include "Timesteppers/DSSTimestepper.h"
#include "DSSIntegrable.h"
#include "Solvers/DSSSolverSOR.h"
#include <eigen3/Eigen/IterativeLinearSolvers>
#include <eigen3/Eigen/src/Core/util/ForwardDeclarations.h>


//This class represents a multibody system. It holds all Rigid Body
//objects and has methods to setup and solve the system dynamics.
//It also controls certain properties such as gravity and holds
// pointers to timesteppers, variables, constraints, etc.



class DSSWorld : public DSSAssembly, public DSSIntegrable {

public:
    DSSWorld();
    ~DSSWorld() override;

    void solveDynamics(double time);

    void simulate();


    //Gets the number of coordinates for each vector.
    //Note that if x uses quaternions and v/a uses angular velocities,
    //ncoords_x != ncoords_v
    int GetNcoords_x() override;
    int GetNcoords_v() override;
    int GetNcoords_a() override;
    //returns the number of constraints in the system.
    int getNconstr() override;

    //fills the state descriptors position (pos) and vel (velocity) with the current
    //positions and velocities of all rigid bodies.
    void retrieveState(DSSStateDescriptor &pos,
                       DSSStateDescriptorDelta &vel,
                       double time) override;

    //after a solution step, this function is called to update the rigid bodies positions and orientations
    //with the ones found by the solver.
    void disperseState(const DSSStateDescriptor &pos,
                       const DSSStateDescriptorDelta &vel,
                       double time) override;

    //after a solution step, this function is called to update the reaction forces (constraints add reaction forces)
    void disperseReactions(const Eigen::VectorXd &L) override;

    //fills the state descriptor acceleration (a) with the current accelerations of all rigid bodies.
    void retrieveAcceleration(DSSStateDescriptorDelta &a) override;

    //after a solution step, this function can be called to update the accelerations for each rigid body
    void disperseAcceleration(const DSSStateDescriptorDelta &a) override;

    //Loads the force vector with all the forces applied to all rigid bodies.
    void loadForceVector(Eigen::VectorXd &Vec, double c) override;

    //Loads the Mass*velocity vector needed by some timesteppers.
    void loadMvVector(Eigen::VectorXd &Vec, const Eigen::VectorXd &velocity, double c) override;

    //loads the Cq (jacobian) * L (lagrange multipliers) needed by some timesteppers.
    void LoadCqLVector(Eigen::VectorXd &Vec, const Eigen::VectorXd L, double c) override;

    //Loads the residuals (constraint violation) vectors.
    void LoadConstraintResiduals(Eigen::VectorXd &Vec, double c) override;

    //Loads the constraint time derivatives vectors.
    void LoadConstrTimeDerivatives(Eigen::VectorXd &Vec, double c) override;

    //returns the solver interface, which is responsible for holding all body variables (mass, inertia, etc)
    DSSSolverInterface* getSolverInterface() { return solverInterface.get(); };

    void convertAngularVel(const DSSStateDescriptorDelta &VAng, DSSStateDescriptorDelta &Vpdot) override;

    //increments the states, takes care of the fact that X doesn't have the same number of coordinates of V.
    void stateIncrement(DSSStateDescriptor &XNew,
                        const DSSStateDescriptor &X,
                        const DSSStateDescriptorDelta &Dy) override;

    //If explicit matrices are needed, this function can be called to load them.
    virtual void structureProblem(Eigen::SparseMatrix<double> *jacobianMat,
                                  Eigen::SparseMatrix<double> *massMat,
                                  Eigen::Matrix<double, Eigen::Dynamic, 1> *generalizedForceVec,
                                  Eigen::Matrix<double, Eigen::Dynamic, 1> *residualVec) override;


    //solves for an increment in speed.
    virtual void stateSolveDelta(DSSStateDescriptorDelta &Dv,
                                 Eigen::VectorXd &L,
                                 Eigen::VectorXd &R,
                                 Eigen::VectorXd Qc,
                                 const DSSStateDescriptor &q,
                                 const DSSStateDescriptorDelta &qdot,
                                 Eigen::MatrixXd &jacobianMatrix, Eigen::MatrixXd &massMatrix) override;

    //Returns the current time step.
    double getTimeStep() const { return step; }

    //Returns the simulation current time.
    double &getCurrentTime() { return currentTime; }

    //Returns the time at which the simulation will end.
    double getEndTime() const { return endTime; }

    //returns the gravity vector.
    Eigen::Vector3d getGravity() const { return gravityAccelerationVector; };

private:

    int currentStep;

    double currentTime;
    double endTime;
    double step;

    //in case adaptive integration schemes are used;
    double minTimeStep;
    double maxTimeStep;

    int maxNumIter;

    Eigen::Vector3d gravityAccelerationVector;

    //Solver interface. Holds all body variables such as mass, also holds a list to all constraints applied in the system.
    std::shared_ptr<DSSSolverInterface> solverInterface;
    //The timestepper, this object is responsible for
    std::shared_ptr<DSSTimestepper> timestepper;
    //eigen linear solver.
    //TODO: Change this to a iterative solver pointer and use the initializer list to set the solver type.
    Eigen::SparseQR<Eigen::SparseMatrix<double>,Eigen::NaturalOrdering<int>> solver;
    DSSSolverSOR solverSOR;

};

