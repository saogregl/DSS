#pragma once
#include "Core/DSSStateDescriptor.h"
#include <eigen3/Eigen/src/Core/util/ForwardDeclarations.h>
#include <eigen3/Eigen/Sparse>

class DSSStateDescriptor;
class DSSStateDescriptorDelta;

class DSSIntegrable
{
public:
	DSSIntegrable();
	virtual ~DSSIntegrable();

	virtual int GetNcoords_x() = 0;
	virtual int GetNcoords_v() { return GetNcoords_x(); }
	virtual int GetNcoords_a() { return GetNcoords_v(); }
	virtual int getNconstr() = 0;

	virtual void loadForceVector(Eigen::VectorXd& Vec, double c){};
	virtual void loadMvVector(Eigen::VectorXd& Vec,const Eigen::VectorXd& velocity, double c){};
	virtual void LoadCqLVector(Eigen::VectorXd& Vec,const Eigen::VectorXd L, double c) {};
	virtual void LoadConstraintResiduals(Eigen::VectorXd& Vec, double c) {};
	virtual void LoadConstrTimeDerivatives(Eigen::VectorXd& Vec, double c) {};

	virtual void structureProblem(Eigen::SparseMatrix<double>* jacobianMat,
	                              Eigen::SparseMatrix<double>* massMat,
	                              Eigen::Matrix<double, Eigen::Dynamic, 1>* generalizedForceVec,
	                              Eigen::Matrix<double, Eigen::Dynamic, 1>* residualVec)
	{
	}

	virtual void stateSolveDelta(DSSStateDescriptorDelta& Dv,
	                             Eigen::VectorXd& L,
	                             Eigen::VectorXd& R,
	                             Eigen::VectorXd Qc,
	                             const DSSStateDescriptor& q,
	                             const DSSStateDescriptorDelta& qdot,
								 Eigen::MatrixXd& jacobianMatrix,
								 Eigen::MatrixXd& massMatrix)
	{
		
	};



	virtual void retrieveState(DSSStateDescriptor& pos,
	                           DSSStateDescriptorDelta& vel,
	                           double time)
	{
	};

	virtual void disperseState(const DSSStateDescriptor& pos,
	                           const DSSStateDescriptorDelta& vel,
	                           double time)
	{
	};

	virtual void disperseReactions(const Eigen::VectorXd& L){}

	virtual void retrieveAcceleration(DSSStateDescriptorDelta& pos)
	{
		
	};
	virtual void disperseAcceleration(const DSSStateDescriptorDelta& a)
	{
		
	};

	virtual void stateIncrement(DSSStateDescriptor& XNew, const DSSStateDescriptor& X, const DSSStateDescriptorDelta& Dy)
	{
		
	};

	virtual void convertAngularVel(const DSSStateDescriptorDelta& VAng, DSSStateDescriptorDelta& Vpdot){ };


	virtual void stateSetup(DSSStateDescriptor& X, DSSStateDescriptorDelta& V, DSSStateDescriptorDelta& A);
};
