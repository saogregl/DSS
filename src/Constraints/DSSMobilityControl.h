#pragma once

#include "../debugFiles.h"
#include "../Core/DSSBodyVariables.h"

class DSSMobilityControl
{
protected:
	// Jacobian Storage (for each body)
	Eigen::Matrix<double, 1, 6> Cq_bi;
	Eigen::Matrix<double, 1, 6> Cq_bj;

	Eigen::Matrix<double, 6, 1> auxiliaryMassCqi;
	Eigen::Matrix<double, 6, 1> auxiliaryMassCqj;



	DSSBodyVariables*			refToBodyi;
	DSSBodyVariables*			refToBodyj;
	
	double						constraintViolation;
    double                      constraintReaction; //Lagrange Multiplier


	int							constraintOffset;

	double 						g_i;              ///<  'g_i' product [Cq_i]*[invM_i]*[Cq_i]' (+cfm) (this is from chrono)
    double                      b_i;  			 ///< The 'b_i' right term in [Cq_i]*q+b_i=0 , note: c_i= [Cq_i]*q


public:

	Eigen::Matrix<double, 1, 6>* getCq1();
	Eigen::Matrix<double, 1, 6>* getCq2();
	Eigen::Matrix<double, 6, 1>* getAuxiliaryMassCqi();
	Eigen::Matrix<double, 6, 1>* getAuxiliaryMassCqj();

	void updateAuxVectors();
	void incrementGenVariable(double increment); //the increment is usually a -reaction force.
	double calculateCq_q ( );

	double getResidual() const;
    double getReaction() const;
    double getgi() const;
    double getbi() const;


	void setbi(double val);
	void setgi(double val);
	void setCq1(const int& index, const double& value);
	void setCq2(const int& index, const double& value);
    void setReaction(const double lMult) {constraintReaction = lMult; };
	void setViolation(const double& value);
	void setOffset(const int& _offset);
	void setReferences(DSSBodyVariables* _refToi, DSSBodyVariables* refToj);


	void BuildJacobian(Eigen::SparseMatrix<double>& storage, const int& rowNum);
	void multiplyAndAdd(Eigen::VectorXd& result, double factor);

	DSSMobilityControl();
	~DSSMobilityControl();

	
};

