//
// Created by saogregl on 22/12/17.
//

#include "DSSConstraintTranslational.h"

void DSSConstraintTranslational::injectMobilizers(DSSSolverInterface &solver_interface) {
    solver_interface.insertMobilizer(&dotConstraintxz);
    solver_interface.insertMobilizer(&dotConstraintyz);
    solver_interface.insertMobilizer(&dotConstraintxd);
    solver_interface.insertMobilizer(&dotConstraintyd);
    solver_interface.insertMobilizer(&dotConstraintxx);

}

void DSSConstraintTranslational::update(double time) {

    currentTime = time;
    //Creates absJointFrames (the frames used to initialize the joint are assumed to be in local coordinates)
    auto absFrameBodyi = DSSAuxiliaryFrame();
    auto absFrameBodyj = DSSAuxiliaryFrame();

    //set the absolute frames by transforming the local ones to absolute coordinates
    Bodyi->transformChildToParent(definitionFrameBodyi, absFrameBodyi);
    Bodyj->transformChildToParent(definitionFrameBodyj, absFrameBodyj);

    //Update the local rotation matrices (the rotations are stored in quaternions, the matrix needs to be calculated from there)
    definitionFrameBodyi.setRotationMatrix(definitionFrameBodyi.buildRotationMatrixFromQuat());
    definitionFrameBodyj.setRotationMatrix(definitionFrameBodyj.buildRotationMatrixFromQuat());

    //Update the absolute rotation matrices (the rotations are stored in quaternions, the matrix needs to be calculated from there)
    absFrameBodyi.setRotationMatrix(absFrameBodyi.buildRotationMatrixFromQuat());
    absFrameBodyj.setRotationMatrix(absFrameBodyj.buildRotationMatrixFromQuat());

    //Initialize the violations to the current value. Not really needed because we'll call update at some point before
    //solution starts but i'd rather do it anyway to avoid uninitialized values

    dij = absFrameBodyj.getPosition() - absFrameBodyi.getPosition();
    auto absXVersori = absFrameBodyi.getxAxis();
    auto absYVersori = absFrameBodyi.getyAxis();
    auto absXVersorj = absFrameBodyj.getxAxis();
    auto absZVersorj = absFrameBodyj.getzAxis();
    auto locXVersori = definitionFrameBodyi.getxAxis();
    auto locXVersorj = definitionFrameBodyj.getxAxis();
    auto locYVersori = definitionFrameBodyi.getyAxis();
    auto locZVersorj = definitionFrameBodyj.getzAxis();



    //Jacobian for the constraint dot(xi,zj)
    {
        //Needed Temporary Vectors
        auto temp1 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersori);
        auto temp2 = Bodyj->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locZVersorj);
        auto hatVectortempi = temp1.transpose()*absZVersorj;
        auto hatVectortempj = temp2.transpose()*absXVersori;

        //Dot constraint 1 jacobian body 1
        dotConstraintxz.setCq1(0,0);
        dotConstraintxz.setCq1(1,0);
        dotConstraintxz.setCq1(2,0);
        dotConstraintxz.setCq1(3,-hatVectortempi.x());
        dotConstraintxz.setCq1(4,-hatVectortempi.y());
        dotConstraintxz.setCq1(5,-hatVectortempi.z());
        //Dot constraint 1 jacobian body 2
        dotConstraintxz.setCq2(0,0);
        dotConstraintxz.setCq2(1,0);
        dotConstraintxz.setCq2(2,0);
        dotConstraintxz.setCq2(3,-hatVectortempj.x());
        dotConstraintxz.setCq2(4,-hatVectortempj.y());
        dotConstraintxz.setCq2(5,-hatVectortempj.z());
    }

    //Jacobian for the constraint dot1(yi,zj)

    {
        //Needed Temporary Vectors
        auto temp1 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locYVersori);
        auto temp2 = Bodyj->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locZVersorj);
        auto hatVectortempi = temp1.transpose()*absZVersorj;
        auto hatVectortempj = temp2.transpose()*absYVersori;

        //Dot constraint 1 jacobian body 1
        dotConstraintyz.setCq1(0,0);
        dotConstraintyz.setCq1(1,0);
        dotConstraintyz.setCq1(2,0);
        dotConstraintyz.setCq1(3,-hatVectortempi.x());
        dotConstraintyz.setCq1(4,-hatVectortempi.y());
        dotConstraintyz.setCq1(5,-hatVectortempi.z());
        //Dot constraint 1 jacobian body 2
        dotConstraintyz.setCq2(0,0);
        dotConstraintyz.setCq2(1,0);
        dotConstraintyz.setCq2(2,0);
        dotConstraintyz.setCq2(3,-hatVectortempj.x());
        dotConstraintyz.setCq2(4,-hatVectortempj.y());
        dotConstraintyz.setCq2(5,-hatVectortempj.z());
    }

    //Jacobian for the constraint dot2(xi,dij)
    {
        //Jacobian for position of dot 2 = -(A_i*x'_i)^T
        auto phi_rij = Bodyi->getRotationMatrix() * locXVersori;

        //Jacobian for virtual rotation of dot 2
        //for body i  = (x'_i * s'_tildeiP - dij^T * Ai * x'tilde_i)
        auto temp1 = locXVersori.transpose() * definitionFrameBodyi.getSkewSymmetricPos();
        auto temp2 = dij.transpose()*Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersori);
        auto phi_pi = temp1 - temp2;

        auto temp3 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersori);
        auto phi_pj = -temp1*Bodyj->getRotationMatrix()*definitionFrameBodyj.getSkewSymmetricPos();

        //Dot constraint 2 jacobian body 1
        dotConstraintxd.setCq1(0,-phi_rij.x());
        dotConstraintxd.setCq1(1,-phi_rij.y());
        dotConstraintxd.setCq1(2,-phi_rij.z());
        dotConstraintxd.setCq1(3, phi_pi.x());
        dotConstraintxd.setCq1(4, phi_pi.y());
        dotConstraintxd.setCq1(5, phi_pi.z());
        //Dot constraint 2 jacobian body 2
        dotConstraintxd.setCq2(0,-phi_rij.x());
        dotConstraintxd.setCq2(1,-phi_rij.y());
        dotConstraintxd.setCq2(2,-phi_rij.z());
        dotConstraintxd.setCq2(3,-phi_pj.x());
        dotConstraintxd.setCq2(4,-phi_pj.y());
        dotConstraintxd.setCq2(5,-phi_pj.z());
    }

    //Jacobian for the constraint dot2(yi,dij)
    {
        //Jacobian for position of dot 2 = -(A_i*y'_i)^T
        auto phi_rij = Bodyi->getRotationMatrix() * locYVersori;
        //Jacobian for virtual rotation of dot 2
        auto tinyHeart1 = locYVersori.transpose() * definitionFrameBodyi.getSkewSymmetricPos();
        auto tinyHeart2 = dij.transpose() * Bodyi->getRotationMatrix() * DSSTransform::makeSkewsymmetric(locYVersori);
        //for body i  = (x'_i * s'_tildeiP - dij^T * Ai * x'tilde_i) = tinyHeart1 - tinyHeart2
        auto phi_pi = tinyHeart1 - tinyHeart2;
        //for body j = - a'i^T * Ai^T * Aj * s'tildej
        auto temp3 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locYVersori);
        auto phi_pj = tinyHeart1 * Bodyj->getRotationMatrix() * definitionFrameBodyj.getSkewSymmetricPos();
        //Dot constraint 2 jacobian body 1
        dotConstraintyd.setCq1(0,-phi_rij.x());
        dotConstraintyd.setCq1(1,-phi_rij.y());
        dotConstraintyd.setCq1(2,-phi_rij.z());
        dotConstraintyd.setCq1(3, phi_pi.x());
        dotConstraintyd.setCq1(4, phi_pi.y());
        dotConstraintyd.setCq1(5, phi_pi.z());
        //Dot constraint 2 jacobian body 2
        dotConstraintyd.setCq2(0,-phi_rij.x());
        dotConstraintyd.setCq2(1,-phi_rij.y());
        dotConstraintyd.setCq2(2,-phi_rij.z());
        dotConstraintyd.setCq2(3,-phi_pj.x());
        dotConstraintyd.setCq2(4,-phi_pj.y());
        dotConstraintyd.setCq2(5,-phi_pj.z());
    }

    //Jacobian for the constraint dot1(xi,xj)
    {
        //Needed Temporary Vectors
        auto temp1 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersori);
        auto temp2 = Bodyj->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersorj);
        auto hatVectortempi = temp1.transpose()*absXVersorj;
        auto hatVectortempj = temp2.transpose()*absXVersori;

        //Dot constraint 1 jacobian body 1
        dotConstraintxx.setCq1(0,0);
        dotConstraintxx.setCq1(1,0);
        dotConstraintxx.setCq1(2,0);
        dotConstraintxx.setCq1(3,-hatVectortempi.x());
        dotConstraintxx.setCq1(4,-hatVectortempi.y());
        dotConstraintxx.setCq1(5,-hatVectortempi.z());
        //Dot constraint 1 jacobian body 2
        dotConstraintxx.setCq2(0,0);
        dotConstraintxx.setCq2(1,0);
        dotConstraintxx.setCq2(2,0);
        dotConstraintxx.setCq2(3,-hatVectortempj.x());
        dotConstraintxx.setCq2(4,-hatVectortempj.y());
        dotConstraintxx.setCq2(5,-hatVectortempj.z());
    }

    dotConstraintxz.setViolation(absXVersori.dot(absZVersorj));
    dotConstraintyz.setViolation(absYVersori.dot(absZVersorj));
    dotConstraintxd.setViolation(absXVersori.dot(dij));
    dotConstraintyd.setViolation(absYVersori.dot(dij));
    dotConstraintxx.setViolation(absXVersori.dot(absXVersorj));

}

void DSSConstraintTranslational::initialize(std::shared_ptr<DSSRigidBody> _body1,
                                            std::shared_ptr<DSSRigidBody> _body2,
                                            const DSSAuxiliaryFrame &_localFrameOnBody1,
                                            const DSSAuxiliaryFrame &_localFrameOnBody2) {

    //Cylindrical joints allow for rotation around a common axis and translation along that axis
    this->nOfDOFsConstrained = 5;
    this->nOfDOFsLeft = 1;
    //get body pointers
    Bodyi = _body1.get();
    Bodyj = _body2.get();

    //Set references (so the mobilizers know if the body is active and can access variables such as speed)
    dotConstraintxz.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    dotConstraintyz.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    dotConstraintxd.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    dotConstraintyd.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());

    //Set local frame references
    definitionFrameBodyi = _localFrameOnBody1;
    definitionFrameBodyj = _localFrameOnBody2;

    //Creates absJointFrames (the frames used to initialize the joint are assumed to be in local coordinates)
    auto absFrameBodyi = DSSAuxiliaryFrame();
    auto absFrameBodyj = DSSAuxiliaryFrame();

    //set the absolute frames by transforming the local ones to absolute coordinates
    Bodyi->transformChildToParent(definitionFrameBodyi, absFrameBodyi);
    Bodyj->transformChildToParent(definitionFrameBodyj, absFrameBodyj);

    //Update the local rotation matrices (the rotations are stored in quaternions)
    definitionFrameBodyi.setRotationMatrix(definitionFrameBodyi.buildRotationMatrixFromQuat());
    definitionFrameBodyj.setRotationMatrix(definitionFrameBodyj.buildRotationMatrixFromQuat());

    //Update the absolute rotation matrices (the rotations are stored in quaternions, the matrix needs to be calculated from there)
    absFrameBodyi.setRotationMatrix(absFrameBodyi.buildRotationMatrixFromQuat());
    absFrameBodyj.setRotationMatrix(absFrameBodyj.buildRotationMatrixFromQuat());

    //set the violations to the current value. Not really needed because we'll call update at some point before
    //the solver starts but i'd rather do it anyway to avoid uninitialized values

    dij = absFrameBodyj.getPosition() - absFrameBodyi.getPosition();
    auto absXVersori = absFrameBodyi.getxAxis();
    auto absYVersori = absFrameBodyi.getyAxis();
    auto absXVersorj = absFrameBodyj.getxAxis();
    auto absZVersorj = absFrameBodyj.getzAxis();

    //Initialize violations
    dotConstraintxz.setViolation(absXVersori.dot(absZVersorj));
    dotConstraintyz.setViolation(absYVersori.dot(absZVersorj));
    dotConstraintxd.setViolation(absXVersori.dot(dij));
    dotConstraintyd.setViolation(absYVersori.dot(dij));
    dotConstraintxx.setViolation(absXVersori.dot(absXVersorj));
}

void
DSSConstraintTranslational::bodyLoadCqLVector(Eigen::VectorXd &Vec,
                                              unsigned int off,
                                              const Eigen::VectorXd &L,
                                              const double c)
{
    //Fills result vector Vec with the Cq*L term needed by some timesteppers.
    dotConstraintxz.multiplyAndAdd(Vec, L.coeff(off + 0)*c);
    dotConstraintyz.multiplyAndAdd(Vec, L.coeff(off + 1)*c);
    dotConstraintxd.multiplyAndAdd(Vec, L.coeff(off + 2)*c);
    dotConstraintyd.multiplyAndAdd(Vec, L.coeff(off + 3)*c);
    dotConstraintxx.multiplyAndAdd(Vec, L.coeff(off + 4)*c);
}

void DSSConstraintTranslational::bodyLoadConstraintResiduals(Eigen::VectorXd &Vec,
                                                             unsigned int off,
                                                             const double c)
{
    //Fills C vector (Vec) with the constraint residuals. Needed by most timesteppers.
    Vec.coeffRef(off + 0) = dotConstraintxz.getResidual();
    Vec.coeffRef(off + 1) = dotConstraintyz.getResidual();
    Vec.coeffRef(off + 2) = dotConstraintxd.getResidual();
    Vec.coeffRef(off + 3) = dotConstraintyd.getResidual();
    Vec.coeffRef(off + 4) = dotConstraintxx.getResidual();

}

void DSSConstraintTranslational::bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec,
                                                               unsigned int off,
                                                               const double c) {

}

void DSSConstraintTranslational::scatterBodyLagrangian(const int offl, Eigen::VectorXd &L) {
    lagrangeMultipliers.coeffRef(0) = L.coeff(offl + 0);
    lagrangeMultipliers.coeffRef(1) = L.coeff(offl + 1);
    lagrangeMultipliers.coeffRef(2) = L.coeff(offl + 2);
    lagrangeMultipliers.coeffRef(3) = L.coeff(offl + 3);
    lagrangeMultipliers.coeffRef(4) = L.coeff(offl + 4);
}

void DSSConstraintTranslational::retrieveBodyLagrangian(const int offl, Eigen::VectorXd &L) {
    L.coeffRef(0) = lagrangeMultipliers.coeff(offl + 0);
    L.coeffRef(1) = lagrangeMultipliers.coeff(offl + 1);
    L.coeffRef(2) = lagrangeMultipliers.coeff(offl + 2);
    L.coeffRef(3) = lagrangeMultipliers.coeff(offl + 3);
    L.coeffRef(4) = lagrangeMultipliers.coeff(offl + 4);

}
