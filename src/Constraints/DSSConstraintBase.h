#pragma once

#include "../debugFiles.h"
#include "../DSSRigidBody.h"


class DSSConstraintBase : public DSSPhysicsObject
{
protected: 

	//Note that lagrange multipliers are not the reaction forces/torques, but you can
	//calculate reactions from lagrange multipliers

	Eigen::VectorXd reactionForces;
	Eigen::VectorXd reactionTorques;
	int				nOfDOFsConstrained;
	int				nOfDOFsLeft;
	int				offsetp;
	int				offsetw;
	int				offsetl;


public:
	DSSRigidBody*	Bodyi;
	DSSRigidBody*	Bodyj;
	
	int getDOFConstrained() const;
	int getDOFLeft() const;

	void setOffp(const int& _offp);
	void setOffw(const int& _offw);
	void setOffl(const int& _offl);
	int getOffp() const;
	int getOffw() const;
	int getOffl() const;


    DSSConstraintBase();

    virtual ~DSSConstraintBase() override;
};

