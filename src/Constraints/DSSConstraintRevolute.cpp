#include "DSSConstraintRevolute.h"

//later change to shared ptr to avoid memory leak problems;
void DSSConstraintRevolute::injectMobilizers(DSSSolverInterface &solver_interface)
{
	solver_interface.insertMobilizer(&sphericalConstraint_x);
	solver_interface.insertMobilizer(&sphericalConstraint_y);
	solver_interface.insertMobilizer(&sphericalConstraint_z);
	solver_interface.insertMobilizer(&sphericalConstraint_dot1);
	solver_interface.insertMobilizer(&sphericalConstraint_dot2);

}

DSSConstraintRevolute::DSSConstraintRevolute() : currentTime(0),
                                                 clamp_value(0.6),
                                                 clamp(true)
{
}


DSSConstraintRevolute::~DSSConstraintRevolute()
{
}

void DSSConstraintRevolute::initialize(std::shared_ptr<DSSRigidBody> _body1,
                                       std::shared_ptr<DSSRigidBody> _body2,
                                       const DSSAuxiliaryFrame & _localFrameOnBody1,
                                       const DSSAuxiliaryFrame & _localFrameOnBody2)
{
	this->nOfDOFsConstrained = 5;
	this->nOfDOFsLeft = 1;

	Bodyi = _body1.get();
	Bodyj = _body2.get();


	sphericalConstraint_x.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
	sphericalConstraint_y.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
	sphericalConstraint_z.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
	sphericalConstraint_dot1.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
	sphericalConstraint_dot2.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());


	localFrameOnBody1 = _localFrameOnBody1;
	localFrameOnBody2 = _localFrameOnBody2;

	auto absFrameBody1 = DSSAuxiliaryFrame();
	auto absFrameBody2 = DSSAuxiliaryFrame();

	Bodyi->transformChildToParent(localFrameOnBody1, absFrameBody1);
	Bodyj->transformChildToParent(localFrameOnBody2, absFrameBody2);

	localFrameOnBody1.setRotationMatrix(localFrameOnBody1.buildRotationMatrixFromQuat());
	localFrameOnBody2.setRotationMatrix(localFrameOnBody2.buildRotationMatrixFromQuat());


	absFrameBody1.setRotationMatrix(absFrameBody1.buildRotationMatrixFromQuat());
	absFrameBody2.setRotationMatrix(absFrameBody2.buildRotationMatrixFromQuat());
	
	auto absx_versor1 = absFrameBody1.getxAxis();
	auto absy_versor1 = absFrameBody1.getyAxis();
	auto absz_versor2 = absFrameBody2.getzAxis();

	auto locx_versor1 = localFrameOnBody1.getxAxis();
	auto locy_versor1 = localFrameOnBody1.getyAxis();
	auto locz_versor2 = localFrameOnBody2.getxAxis();


	//Set constraint violations for all 5 DOFS constrained.
	sphericalConstraint_x.setViolation(absFrameBody2.getPosition().x() - absFrameBody1.getPosition().x());
	sphericalConstraint_y.setViolation(absFrameBody2.getPosition().y() - absFrameBody1.getPosition().y());
	sphericalConstraint_z.setViolation(absFrameBody2.getPosition().z() - absFrameBody1.getPosition().z());
	sphericalConstraint_dot1.setViolation(absz_versor2.dot(absx_versor1));
	sphericalConstraint_dot2.setViolation(absz_versor2.dot(absy_versor1));	


    std::cout << sphericalConstraint_x.getResidual() << std::endl;
    std::cout << sphericalConstraint_y.getResidual() << std::endl;
    std::cout << sphericalConstraint_z.getResidual() << std::endl;
	std::cout << sphericalConstraint_dot1.getResidual() << std::endl;
	std::cout << sphericalConstraint_dot2.getResidual() << std::endl;


}


void DSSConstraintRevolute::update(double time)
{
	auto absFrameBody1 = DSSAuxiliaryFrame();
	auto absFrameBody2 = DSSAuxiliaryFrame();

	Bodyi->transformChildToParent(localFrameOnBody1, absFrameBody1);
	Bodyj->transformChildToParent(localFrameOnBody2, absFrameBody2);

    Bodyi->syncRotationMatrix();
    Bodyj->syncRotationMatrix();
    absFrameBody1.setRotationMatrix(absFrameBody1.buildRotationMatrixFromQuat());
	absFrameBody2.setRotationMatrix(absFrameBody2.buildRotationMatrixFromQuat());

	localFrameOnBody1.setRotationMatrix(localFrameOnBody1.buildRotationMatrixFromQuat());
	localFrameOnBody2.setRotationMatrix(localFrameOnBody2.buildRotationMatrixFromQuat());

	auto tilde_pos1 = DSSTransform::makeSkewsymmetric(localFrameOnBody1.position);
	auto tilde_pos2 = DSSTransform::makeSkewsymmetric(localFrameOnBody2.position);
	//localFrameOnBody1.getSkewSymmetricPos();
	Eigen::Matrix3d phi_pi1 = Bodyi->getRotationMatrix()*tilde_pos1;
	Eigen::Matrix3d phi_pi2 = Bodyj->getRotationMatrix()*tilde_pos2;
	//relative to position
	sphericalConstraint_x.setCq1(0, -1);
	sphericalConstraint_x.setCq2(0, 1);
	sphericalConstraint_x.setCq1(1, 0);
	sphericalConstraint_x.setCq2(1, 0);
	sphericalConstraint_x.setCq1(2, 0);
	sphericalConstraint_x.setCq2(2, 0);
	//relative to virtual rotation
	sphericalConstraint_x.setCq1(3, phi_pi1(0, 0));
	sphericalConstraint_x.setCq2(3, -phi_pi2(0, 0));
	sphericalConstraint_x.setCq1(4, phi_pi1(0, 1));
	sphericalConstraint_x.setCq2(4, -phi_pi2(0, 1));
	sphericalConstraint_x.setCq1(5, phi_pi1(0, 2));
	sphericalConstraint_x.setCq2(5, -phi_pi2(0, 2));
	//constraint in y
	//relative to position
	sphericalConstraint_y.setCq1(0, 0);
	sphericalConstraint_y.setCq2(0, 0);
	sphericalConstraint_y.setCq1(1, -1);
	sphericalConstraint_y.setCq2(1, 1);
	sphericalConstraint_y.setCq1(2, 0);
	sphericalConstraint_y.setCq2(2, 0);
	//relative to virtual rotation
	sphericalConstraint_y.setCq1(3, phi_pi1(1, 0));
	sphericalConstraint_y.setCq2(3, -phi_pi2(1, 0));
	sphericalConstraint_y.setCq1(4, phi_pi1(1, 1));
	sphericalConstraint_y.setCq2(4, -phi_pi2(1, 1));
	sphericalConstraint_y.setCq1(5, phi_pi1(1, 2));
	sphericalConstraint_y.setCq2(5, -phi_pi2(1, 2));
	//constraint in z
	//relative to position
	sphericalConstraint_z.setCq1(0, 0);
	sphericalConstraint_z.setCq2(0, 0);
	sphericalConstraint_z.setCq1(1, 0);
	sphericalConstraint_z.setCq2(1, 0);
	sphericalConstraint_z.setCq1(2, -1);
	sphericalConstraint_z.setCq2(2, 1);
	//relative to virtual rotation
	sphericalConstraint_z.setCq1(3, phi_pi1(2, 0));
	sphericalConstraint_z.setCq2(3, -phi_pi2(2, 0));
	sphericalConstraint_z.setCq1(4, phi_pi1(2, 1));
	sphericalConstraint_z.setCq2(4, -phi_pi2(2, 1));
	sphericalConstraint_z.setCq1(5, phi_pi1(2, 2));
	sphericalConstraint_z.setCq2(5, -phi_pi2(2, 2));

	auto absx_versor1 = absFrameBody1.getxAxis();
	auto absy_versor1 = absFrameBody1.getyAxis();
	auto absz_versor2 = absFrameBody2.getzAxis();
	auto locx_versor1 = localFrameOnBody1.getxAxis();
	auto locy_versor1 = localFrameOnBody1.getyAxis();
	auto locz_versor2 = localFrameOnBody2.getzAxis();

	absx_versor1.normalize();
	absy_versor1.normalize();
	absz_versor2.normalize();
	locx_versor1.normalize();
	locy_versor1.normalize();
	locz_versor2.normalize();

	//Dot constraint 1.
	absFrameBody2.setRotationMatrix(absFrameBody2.buildRotationMatrixFromQuat());
	{
		auto temp1 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locx_versor1);
		auto temp2 = Bodyj->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locz_versor2);
		auto hatVectortemp1 = temp1.transpose()*absz_versor2;
		auto hatVectortemp2 = temp2.transpose()*absx_versor1;

		sphericalConstraint_dot1.setCq1(0, 0);
		sphericalConstraint_dot1.setCq1(1, 0);
		sphericalConstraint_dot1.setCq1(2, 0);
		sphericalConstraint_dot1.setCq1(3, -hatVectortemp1.x());
		sphericalConstraint_dot1.setCq1(4, -hatVectortemp1.y());
		sphericalConstraint_dot1.setCq1(5, -hatVectortemp1.z());
		sphericalConstraint_dot1.setCq2(0, 0);
		sphericalConstraint_dot1.setCq2(1, 0);
		sphericalConstraint_dot1.setCq2(2, 0);
		sphericalConstraint_dot1.setCq2(3, -hatVectortemp2.x());
		sphericalConstraint_dot1.setCq2(4, -hatVectortemp2.y());
		sphericalConstraint_dot1.setCq2(5, -hatVectortemp2.z());
	}
	//Dot constraint 2.
	{
		auto temp1 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locy_versor1);
		auto temp2 = Bodyj->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locz_versor2);
		auto hatVectortemp1 = temp1.transpose()*absz_versor2;
		auto hatVectortemp2 = temp2.transpose()*absy_versor1;

		sphericalConstraint_dot2.setCq1(0, 0);
		sphericalConstraint_dot2.setCq1(1, 0);
		sphericalConstraint_dot2.setCq1(2, 0);
		sphericalConstraint_dot2.setCq1(3, -hatVectortemp1.x());
		sphericalConstraint_dot2.setCq1(4, -hatVectortemp1.y());
		sphericalConstraint_dot2.setCq1(5, -hatVectortemp1.z());
		sphericalConstraint_dot2.setCq2(0, 0);
		sphericalConstraint_dot2.setCq2(1, 0);
		sphericalConstraint_dot2.setCq2(2, 0);
		sphericalConstraint_dot2.setCq2(3, -hatVectortemp2.x());
		sphericalConstraint_dot2.setCq2(4, -hatVectortemp2.y());
		sphericalConstraint_dot2.setCq2(5, -hatVectortemp2.z());
	}

	//Set constraint violations for all 5 DOFS constrained.
	sphericalConstraint_x.setViolation(absFrameBody2.getPosition().x() - absFrameBody1.getPosition().x());
	sphericalConstraint_y.setViolation(absFrameBody2.getPosition().y() - absFrameBody1.getPosition().y());
	sphericalConstraint_z.setViolation(absFrameBody2.getPosition().z() - absFrameBody1.getPosition().z());
	sphericalConstraint_dot1.setViolation(absz_versor2.dot(absx_versor1));
	sphericalConstraint_dot2.setViolation(absz_versor2.dot(absy_versor1));

}

void DSSConstraintRevolute::bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned off, const Eigen::VectorXd &L, double c)
{
	sphericalConstraint_x.multiplyAndAdd(Vec, L.coeff(off + 0)*c);
	sphericalConstraint_y.multiplyAndAdd(Vec, L.coeff(off + 1)*c);
	sphericalConstraint_z.multiplyAndAdd(Vec, L.coeff(off + 2)*c);
	sphericalConstraint_dot1.multiplyAndAdd(Vec, L.coeff(off + 3)*c);
	sphericalConstraint_dot2.multiplyAndAdd(Vec, L.coeff(off + 4)*c);

}

void DSSConstraintRevolute::bodyLoadConstraintResiduals(Eigen::VectorXd &Vec, unsigned off, double c)
{
	//Lets assume clamp

	auto viol_x  =  c * sphericalConstraint_x.getResidual();
	auto viol_y  =  c * sphericalConstraint_y.getResidual();
	auto viol_z  =  c * sphericalConstraint_z.getResidual();
	auto viol_d1 =  c * sphericalConstraint_dot1.getResidual();
	auto viol_d2 =  c * sphericalConstraint_dot2.getResidual();

	if(clamp)
	{
	viol_x 	= std::min(std::max(viol_x, -0.6), 0.6);
	viol_y 	= std::min(std::max(viol_y, -0.6), 0.6);
	viol_z 	= std::min(std::max(viol_z, -0.6), 0.6);
	viol_d1 = std::min(std::max(viol_d1,-0.6), 0.6);
	viol_d2 = std::min(std::max(viol_d2,-0.6), 0.6);
	}


	Vec.coeffRef(off + 0) += viol_x;
    Vec.coeffRef(off + 1) += viol_y;
    Vec.coeffRef(off + 2) += viol_z;
    Vec.coeffRef(off + 3) += viol_d1;
    Vec.coeffRef(off + 4) += viol_d2;

}

void DSSConstraintRevolute::bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec, unsigned off, double c)
{
	// for non-running constraint: 
	// Ct = 0; 

}

void DSSConstraintRevolute::retrieveBodyLagrangian(const int offl, Eigen::VectorXd &L) {

	L.coeffRef(offl + 0) = lagrangeMultipliers.coeff(0);
	L.coeffRef(offl + 1) = lagrangeMultipliers.coeff(1);
	L.coeffRef(offl + 2) = lagrangeMultipliers.coeff(2);
	L.coeffRef(offl + 3) = lagrangeMultipliers.coeff(3);
	L.coeffRef(offl + 4) = lagrangeMultipliers.coeff(4);

}

void DSSConstraintRevolute::scatterBodyLagrangian(const int offl, Eigen::VectorXd &L)
{
    lagrangeMultipliers.coeffRef(0) = L.coeff(offl + 0);
    lagrangeMultipliers.coeffRef(1) = L.coeff(offl + 1);
    lagrangeMultipliers.coeffRef(2) = L.coeff(offl + 2);
    lagrangeMultipliers.coeffRef(3) = L.coeff(offl + 3);
    lagrangeMultipliers.coeffRef(4) = L.coeff(offl + 4);

}

void DSSConstraintRevolute::objPrepareSolverDescriptor(const int offv,
                                                       const DSSStateDescriptorDelta &vel,
                                                       const Eigen::VectorXd &Residuals,
                                                       const int offL,
                                                       const Eigen::VectorXd &L,
                                                       const Eigen::VectorXd Qc) {

	sphericalConstraint_x.setReaction(L(offL + 0));
	sphericalConstraint_y.setReaction(L(offL + 1));
	sphericalConstraint_z.setReaction(L(offL + 2));
	sphericalConstraint_dot1.setReaction(L(offL + 3));
	sphericalConstraint_dot2.setReaction(L(offL + 4));

	sphericalConstraint_x.setbi(Qc(offL + 0));
	sphericalConstraint_y.setbi(Qc(offL + 1));
	sphericalConstraint_z.setbi(Qc(offL + 2));
	sphericalConstraint_dot1.setbi(Qc(offL + 3));
	sphericalConstraint_dot2.setbi(Qc(offL + 4));



}

void DSSConstraintRevolute::objUpdateFromSolverDescriptor(const int offv,
                                                          DSSStateDescriptorDelta &vel,
                                                          const int offL,
                                                          Eigen::VectorXd &L) {

	L(offL + 0) = sphericalConstraint_x.getReaction();
	L(offL + 1) = sphericalConstraint_y.getReaction();
	L(offL + 2) = sphericalConstraint_z.getReaction();
	L(offL + 3) = sphericalConstraint_dot1.getReaction();
	L(offL + 4) = sphericalConstraint_dot2.getReaction();

}
