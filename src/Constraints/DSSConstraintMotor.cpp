#include "DSSConstraintMotor.h"
#include "../DSSFunctionConstant.h"


DSSMotor::DSSMotor() : currentTime(0) {
    motorVariables.setBodyMass(1);
    auxiliaryAcceleration = 0;
    auxiliarySpeed = 0;
    avoidAngleDrift = true;
    motorFunction = std::make_shared<DSSFunctionConstant>(1.0);
}


DSSMotor::~DSSMotor() = default;

void DSSMotor::injectMobilizers(DSSSolverInterface &solver_interface) {
    solver_interface.insertMobilizer(&relativeRotConstraint);
}

void DSSMotor::update(double time) {
    DSSConstraintMate::update(time);

    auto absFrameBody1 = DSSAuxiliaryFrame();
    auto absFrameBody2 = DSSAuxiliaryFrame();

    Bodyi->transformChildToParent(localFrameOnBody1, absFrameBody1);
    Bodyj->transformChildToParent(localFrameOnBody2, absFrameBody2);

    Bodyi->syncRotationMatrix();
    Bodyj->syncRotationMatrix();
    absFrameBody1.setRotationMatrix(absFrameBody1.buildRotationMatrixFromQuat());
    absFrameBody2.setRotationMatrix(absFrameBody2.buildRotationMatrixFromQuat());

    auto frame12 = DSSAuxiliaryFrame();
    absFrameBody2.transformParentToChild(absFrameBody1, frame12);

    auto rotatingFrame2 = DSSAuxiliaryFrame();
    double aux_rotation;

    if (this->avoidAngleDrift) {
        aux_rotation = auxiliarySpeed + rotationOffset;
    }

    auto axisAngle = Eigen::AngleAxisd(aux_rotation, Eigen::Vector3d::UnitZ());
    rotatingFrame2.setOrientation(absFrameBody2.getOrientation() * Eigen::Quaterniond(axisAngle));

    auto rotatingFrame12 = DSSAuxiliaryFrame();
    rotatingFrame2.transformParentToChild(absFrameBody1, rotatingFrame12);


    Eigen::Matrix3d Jw1, Jw2;
    Eigen::Matrix3d mtempM, mtempQ;

    Eigen::Matrix3d absPlaneRotating = rotatingFrame2.getRotationMatrix();

    Jw1 = absPlaneRotating * Bodyi->getRotationMatrix();
    Jw2 = absPlaneRotating * Bodyj->getRotationMatrix();

    Jw2 = -Jw2;

    mtempM = DSSTransform::makeSkewsymmetric(absFrameBody1.getOrientation().vec() * 0.5);
    mtempM(0, 0) = 0.5 * rotatingFrame12.getOrientation().w();
    mtempM(1, 1) = 0.5 * rotatingFrame12.getOrientation().w();
    mtempM(2, 2) = 0.5 * rotatingFrame12.getOrientation().w();
    mtempQ = mtempM.transpose() * Jw1;
    Jw1 = mtempQ;
    mtempQ = mtempM.transpose() * Jw2;
    Jw2 = mtempQ;


    //Constraints in RX
    //Body i
    constraint_rx.setViolation(rotatingFrame12.getOrientation().x());
    constraint_rx.getCq1()->coeffRef(0, 0) = 0;
    constraint_rx.getCq1()->coeffRef(0, 1) = 0;
    constraint_rx.getCq1()->coeffRef(0, 2) = 0;
    constraint_rx.getCq1()->coeffRef(0, 3) = Jw1(0, 0);
    constraint_rx.getCq1()->coeffRef(0, 4) = Jw1(0, 1);
    constraint_rx.getCq1()->coeffRef(0, 5) = Jw1(0, 2);
    //Body j
    constraint_rx.getCq2()->coeffRef(0, 0) = 0;
    constraint_rx.getCq2()->coeffRef(0, 1) = 0;
    constraint_rx.getCq2()->coeffRef(0, 2) = 0;
    constraint_rx.getCq2()->coeffRef(0, 3) = Jw2(0, 0);
    constraint_rx.getCq2()->coeffRef(0, 4) = Jw2(0, 1);
    constraint_rx.getCq2()->coeffRef(0, 5) = Jw2(0, 2);


    //Constraints in RY
    //Body i
    constraint_ry.setViolation(rotatingFrame12.getOrientation().y());
    constraint_ry.getCq1()->coeffRef(0, 0) = 0;
    constraint_ry.getCq1()->coeffRef(0, 1) = 0;
    constraint_ry.getCq1()->coeffRef(0, 2) = 0;
    constraint_ry.getCq1()->coeffRef(0, 3) = Jw1(1, 0);
    constraint_ry.getCq1()->coeffRef(0, 4) = Jw1(1, 1);
    constraint_ry.getCq1()->coeffRef(0, 5) = Jw1(1, 2);
    //Body j
    constraint_ry.getCq2()->coeffRef(0, 0) = 0;
    constraint_ry.getCq2()->coeffRef(0, 1) = 0;
    constraint_ry.getCq2()->coeffRef(0, 2) = 0;
    constraint_ry.getCq2()->coeffRef(0, 3) = Jw2(1, 0);
    constraint_ry.getCq2()->coeffRef(0, 4) = Jw2(1, 1);
    constraint_ry.getCq2()->coeffRef(0, 5) = Jw2(1, 2);

    //Constraints in RY
    //Body i
    constraint_ry.setViolation(rotatingFrame12.getOrientation().z());
    constraint_rz.getCq1()->coeffRef(0, 0) = 0;
    constraint_rz.getCq1()->coeffRef(0, 1) = 0;
    constraint_rz.getCq1()->coeffRef(0, 2) = 0;
    constraint_rz.getCq1()->coeffRef(0, 3) = Jw1(1, 0);
    constraint_rz.getCq1()->coeffRef(0, 4) = Jw1(1, 1);
    constraint_rz.getCq1()->coeffRef(0, 5) = Jw1(1, 2);
    //Body j
    constraint_rz.getCq2()->coeffRef(0, 0) = 0;
    constraint_rz.getCq2()->coeffRef(0, 1) = 0;
    constraint_rz.getCq2()->coeffRef(0, 2) = 0;
    constraint_rz.getCq2()->coeffRef(0, 3) = Jw2(1, 0);
    constraint_rz.getCq2()->coeffRef(0, 4) = Jw2(1, 1);
    constraint_rz.getCq2()->coeffRef(0, 5) = Jw2(1, 2);


}


void DSSMotor::scatterBodyLagrangian(const int offl, Eigen::VectorXd &L) {
    lagrangeMultipliers.coeffRef(0) = L.coeffRef(offl);
}

void DSSMotor::bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned off, const Eigen::VectorXd &L, double c) {
    relativeRotConstraint.multiplyAndAdd(Vec, L(off + 0) * c);

}

void DSSMotor::bodyLoadConstraintResiduals(Eigen::VectorXd &Vec, unsigned off, double c) {
    Vec.coeffRef(off + 0) += relativeRotConstraint.getResidual();

}

void DSSMotor::bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec, unsigned off, double c) {
    Vec.coeffRef(off + 0) += c * (angularVelocity);

}

void DSSMotor::setMotorActuatorF(std::shared_ptr<DSSFunction> func) {
    motorFunction = func;
}

double DSSMotor::getTorque() {
    return 0;
}
