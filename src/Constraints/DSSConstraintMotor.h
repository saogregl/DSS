#pragma once

#include "DSSConstraintBase.h"
#include "DSSMobilityControl.h"
#include "../DSSFunction.h"
#include "DSSConstraintMate.h"

class DSSMotor :
        public DSSConstraintMate
{
private: 

	DSSMobilityControl relativeRotConstraint;		//dot2 constraint

	DSSAuxiliaryFrame localFrameOnBody1;
	DSSAuxiliaryFrame localFrameOnBody2;

    //The function can represent ang. position, velocity, torque, etc.
    std::shared_ptr<DSSFunction> motorFunction;

	
	Eigen::Matrix<double, 1, 1> lagrangeMultipliers;

	double currentTime;
	double angularVelocity;
    double angularPosition;
    double angularPositionPeriodic;
    double angularAcceleration;

    DSSBodyVariables motorVariables;
    bool avoidAngleDrift;
    double auxiliarySpeed;
    double auxiliaryAcceleration;
    double rotationOffset;



public:

    DSSMotor();

    ~DSSMotor() override;

    virtual double getTotalMotorRotation() { return angularPosition; };

    virtual double getMotorRotation() { return angularPositionPeriodic; };

    virtual double getMotorAngularVelocity() { return angularVelocity; };

    virtual double getMotorAngularAcceleration() { return angularAcceleration; };

    virtual double getTorque();




	void injectMobilizers(DSSSolverInterface& solver_interface) override;
	void update(double time) override;

    void scatterBodyLagrangian(int offl,
                               Eigen::VectorXd &L) override;

	void bodyLoadCqLVector(Eigen::VectorXd &Vec,
                           unsigned int off,
                           const Eigen::VectorXd &L,
                           double c) override;

	void bodyLoadConstraintResiduals(Eigen::VectorXd& Vec,
									 unsigned int off,
									 double c) override;
	
	void bodyLoadConstrTimeDerivatives(Eigen::VectorXd& Vec,
									   unsigned int off,
									   double c) override;

    void setMotorActuatorF(std::shared_ptr<DSSFunction> func);


};

