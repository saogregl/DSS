//
// Created by saogregl on 28/12/17.
//

#include "DSSConstraintCylindrical.h"

void DSSCylindricalJoint::injectMobilizers(DSSSolverInterface &solver_interface) {
    solver_interface.insertMobilizer(&dotConstraintxz);
    solver_interface.insertMobilizer(&dotConstraintyz);
    solver_interface.insertMobilizer(&dotConstraintxd);
    solver_interface.insertMobilizer(&dotConstraintyd);
}

DSSCylindricalJoint::DSSCylindricalJoint() : currentTime(0) {

}




void DSSCylindricalJoint::initialize(std::shared_ptr<DSSRigidBody> _body1,
                                     std::shared_ptr<DSSRigidBody> _body2,
                                     const DSSAuxiliaryFrame &_localFrameOnBody1,
                                     const DSSAuxiliaryFrame &_localFrameOnBody2) {

    //Cylindrical joints allow for rotation around a common axis and translation along that axis
    this->nOfDOFsConstrained = 4;
    this->nOfDOFsLeft = 2;
    //get body pointers
    Bodyi = _body1.get();
    Bodyj = _body2.get();

    //Set references (so the mobilizers know if the body is active and can access variables such as speed)
    dotConstraintxz.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    dotConstraintyz.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    dotConstraintxd.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    dotConstraintyd.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());

    //Set local frame references
    definitionFrameBodyi = _localFrameOnBody1;
    definitionFrameBodyj = _localFrameOnBody2;

    //Creates absJointFrames (the frames used to initialize the joint are assumed to be in local coordinates)
    auto absFrameBodyi = DSSAuxiliaryFrame();
    auto absFrameBodyj = DSSAuxiliaryFrame();

    //set the absolute frames by transforming the local ones to absolute coordinates
    Bodyi->transformChildToParent(definitionFrameBodyi, absFrameBodyi);
    Bodyj->transformChildToParent(definitionFrameBodyj, absFrameBodyj);

    //Update the local rotation matrices (the rotations are stored in quaternions, the matrix needs to be calculated from there)
    definitionFrameBodyi.setRotationMatrix(definitionFrameBodyi.buildRotationMatrixFromQuat());
    definitionFrameBodyj.setRotationMatrix(definitionFrameBodyj.buildRotationMatrixFromQuat());

    //Update the absolute rotation matrices (the rotations are stored in quaternions, the matrix needs to be calculated from there)
    absFrameBodyi.setRotationMatrix(absFrameBodyi.buildRotationMatrixFromQuat());
    absFrameBodyj.setRotationMatrix(absFrameBodyj.buildRotationMatrixFromQuat());

    //Initialize the violations to the current value. Not really needed because we'll call update at some point before
    //solution starts but i'd rather do it anyway to avoid uninitialized values

    dij = absFrameBodyj.getPosition() - absFrameBodyi.getPosition();
    auto absXVersori = absFrameBodyi.getxAxis();
    auto absYVersori = absFrameBodyi.getyAxis();
    auto absZVersorj = absFrameBodyj.getzAxis();


    //Initialize violations
    dotConstraintxz.setViolation(absXVersori.dot(absZVersorj));
    dotConstraintyz.setViolation(absYVersori.dot(absZVersorj));
    dotConstraintxd.setViolation(absXVersori.dot(dij));
    dotConstraintyd.setViolation(absYVersori.dot(dij));


}


void DSSCylindricalJoint::update(double time) {

    //Creates absJointFrames (the frames used to initialize the joint are assumed to be in local coordinates)
    auto absFrameBodyi = DSSAuxiliaryFrame();
    auto absFrameBodyj = DSSAuxiliaryFrame();

    //set the absolute frames by transforming the local ones to absolute coordinates
    Bodyi->transformChildToParent(definitionFrameBodyi, absFrameBodyi);
    Bodyj->transformChildToParent(definitionFrameBodyj, absFrameBodyj);

    definitionFrameBodyi.setRotationMatrix(definitionFrameBodyi.buildRotationMatrixFromQuat());
    definitionFrameBodyj.setRotationMatrix(definitionFrameBodyj.buildRotationMatrixFromQuat());

    //Update the absolute rotation matrices (the rotations are stored in quaternions, the matrix needs to be calculated from there)
    absFrameBodyi.setRotationMatrix(absFrameBodyi.buildRotationMatrixFromQuat());
    absFrameBodyj.setRotationMatrix(absFrameBodyj.buildRotationMatrixFromQuat());

    //needed vectors
    dij = absFrameBodyj.getPosition() - absFrameBodyi.getPosition();
    auto absXVersori = absFrameBodyi.getxAxis();
    auto absYVersori = absFrameBodyi.getyAxis();
    auto absZVersorj = absFrameBodyj.getzAxis();
    auto locXVersori = definitionFrameBodyi.getxAxis();
    auto locYVersori = definitionFrameBodyi.getyAxis();
    auto locZVersorj = definitionFrameBodyj.getzAxis();

    absFrameBodyj.setRotationMatrix(absFrameBodyj.buildRotationMatrixFromQuat());
    absFrameBodyi.setRotationMatrix(absFrameBodyj.buildRotationMatrixFromQuat());

    //Jacobian for the constraint dot(xi,zj)
    {
        //Needed Temporary Vectors
        auto temp1 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersori);
        auto temp2 = Bodyj->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locZVersorj);
        auto hatVectortempi = temp1.transpose()*absZVersorj;
        auto hatVectortempj = temp2.transpose()*absXVersori;

        //Dot constraint 1 jacobian body 1
        dotConstraintxz.setCq1(0,0);
        dotConstraintxz.setCq1(1,0);
        dotConstraintxz.setCq1(2,0);
        dotConstraintxz.setCq1(3,-hatVectortempi.x());
        dotConstraintxz.setCq1(4,-hatVectortempi.y());
        dotConstraintxz.setCq1(5,-hatVectortempi.z());
        //Dot constraint 1 jacobian body 2
        dotConstraintxz.setCq2(0,0);
        dotConstraintxz.setCq2(1,0);
        dotConstraintxz.setCq2(2,0);
        dotConstraintxz.setCq2(3,-hatVectortempj.x());
        dotConstraintxz.setCq2(4,-hatVectortempj.y());
        dotConstraintxz.setCq2(5,-hatVectortempj.z());
    }

    //Jacobian for the constraint dot1(yi,zj)

    {
        //Needed Temporary Vectors
        auto temp1 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locYVersori);
        auto temp2 = Bodyj->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locZVersorj);
        auto hatVectortempi = temp1.transpose()*absZVersorj;
        auto hatVectortempj = temp2.transpose()*absYVersori;

        //Dot constraint 1 jacobian body 1
        dotConstraintyz.setCq1(0,0);
        dotConstraintyz.setCq1(1,0);
        dotConstraintyz.setCq1(2,0);
        dotConstraintyz.setCq1(3,-hatVectortempi.x());
        dotConstraintyz.setCq1(4,-hatVectortempi.y());
        dotConstraintyz.setCq1(5,-hatVectortempi.z());
        //Dot constraint 1 jacobian body 2
        dotConstraintyz.setCq2(0,0);
        dotConstraintyz.setCq2(1,0);
        dotConstraintyz.setCq2(2,0);
        dotConstraintyz.setCq2(3,-hatVectortempj.x());
        dotConstraintyz.setCq2(4,-hatVectortempj.y());
        dotConstraintyz.setCq2(5,-hatVectortempj.z());
    }

    //Jacobian for the constraint dot(xi,dij)
    {
        //Jacobian for position of dot 2 = -(A_i*x'_i)^T
        auto phi_rij = Bodyi->getRotationMatrix() * locXVersori;
        //Jacobian for virtual rotation of dot 2
        //for body i  = (x'_i * s'_tildeiP - dij^T * Ai * x'tilde_i)
        auto littleHeart1 = locXVersori.transpose() * definitionFrameBodyi.getSkewSymmetricPos();
        auto littleHeart2 = dij.transpose()*Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersori);
        auto phi_pi = littleHeart1 - littleHeart2;

        auto littleHeart3 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locXVersori);
        auto phi_pj = -littleHeart1*Bodyj->getRotationMatrix()*definitionFrameBodyj.getSkewSymmetricPos();

        //Dot constraint 2 jacobian body 1
        dotConstraintxd.setCq1(0,-phi_rij.x());
        dotConstraintxd.setCq1(1,-phi_rij.y());
        dotConstraintxd.setCq1(2,-phi_rij.z());
        dotConstraintxd.setCq1(3, phi_pi.x());
        dotConstraintxd.setCq1(4, phi_pi.y());
        dotConstraintxd.setCq1(5, phi_pi.z());
        //Dot constraint 2 jacobian body 2
        dotConstraintxd.setCq2(0,-phi_rij.x());
        dotConstraintxd.setCq2(1,-phi_rij.y());
        dotConstraintxd.setCq2(2,-phi_rij.z());
        dotConstraintxd.setCq2(3,-phi_pj.x());
        dotConstraintxd.setCq2(4,-phi_pj.y());
        dotConstraintxd.setCq2(5,-phi_pj.z());
    }

    {
        //Jacobian for position of dot 2 = -(A_i*y'_i)^T
        auto phi_rij = Bodyi->getRotationMatrix() * locYVersori;
        //Jacobian for virtual rotation of dot 2
        auto littleHeart1 = locYVersori.transpose() * definitionFrameBodyi.getSkewSymmetricPos();
        auto littleHeart2 = dij.transpose()*Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locYVersori);
        //for body i  = (x'_i * s'_tildeiP - dij^T * Ai * x'tilde_i) = littleHeart1 - littleHeart2
        auto phi_pi = littleHeart1 - littleHeart2;
        //for body j = - a'i^T * Ai^T * Aj * s'tildej
        auto littleHeart3 = Bodyi->getRotationMatrix()*DSSTransform::makeSkewsymmetric(locYVersori);
        auto phi_pj = littleHeart1*Bodyj->getRotationMatrix()*definitionFrameBodyj.getSkewSymmetricPos();
        //Dot constraint 2 jacobian body 1
        dotConstraintyd.setCq1(0,-phi_rij.x());
        dotConstraintyd.setCq1(1,-phi_rij.y());
        dotConstraintyd.setCq1(2,-phi_rij.z());
        dotConstraintyd.setCq1(3, phi_pi.x());
        dotConstraintyd.setCq1(4, phi_pi.y());
        dotConstraintyd.setCq1(5, phi_pi.z());
        //Dot constraint 2 jacobian body 2
        dotConstraintyd.setCq2(0,-phi_rij.x());
        dotConstraintyd.setCq2(1,-phi_rij.y());
        dotConstraintyd.setCq2(2,-phi_rij.z());
        dotConstraintyd.setCq2(3,-phi_pj.x());
        dotConstraintyd.setCq2(4,-phi_pj.y());
        dotConstraintyd.setCq2(5,-phi_pj.z());
    }
    dotConstraintxz.setViolation(absXVersori.dot(absZVersorj));
    dotConstraintyz.setViolation(absYVersori.dot(absZVersorj));
    dotConstraintxd.setViolation(absXVersori.dot(dij));
    dotConstraintyd.setViolation(absYVersori.dot(dij));
}

void DSSCylindricalJoint::bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned int off, const Eigen::VectorXd &L, double c) {

    dotConstraintxz.multiplyAndAdd(Vec, L.coeff(off + 0)*c);
    dotConstraintyz.multiplyAndAdd(Vec, L.coeff(off + 1)*c);
    dotConstraintxd.multiplyAndAdd(Vec, L.coeff(off + 2)*c);
    dotConstraintyd.multiplyAndAdd(Vec, L.coeff(off + 3)*c);
}

void DSSCylindricalJoint::bodyLoadConstraintResiduals(Eigen::VectorXd &Vec, unsigned int off, double c) {
    Vec.coeffRef(off + 0) += c*dotConstraintxz.getResidual();
    Vec.coeffRef(off + 1) += c*dotConstraintyz.getResidual();
    Vec.coeffRef(off + 2) += c*dotConstraintxd.getResidual();
    Vec.coeffRef(off + 3) += c*dotConstraintyd.getResidual();

}

void DSSCylindricalJoint::scatterBodyLagrangian(const int offl, Eigen::VectorXd &L) {
    lagrangeMultipliers.coeffRef(0) = L.coeff(offl + 0);
    lagrangeMultipliers.coeffRef(1) = L.coeff(offl + 1);
    lagrangeMultipliers.coeffRef(2) = L.coeff(offl + 2);
    lagrangeMultipliers.coeffRef(3) = L.coeff(offl + 3);

}

void DSSCylindricalJoint::bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec, unsigned int off, double c) {
//TODO Implement this load function

}

void DSSCylindricalJoint::retrieveBodyLagrangian(const int offl, Eigen::VectorXd &L) {
    L.coeffRef(offl + 0) = lagrangeMultipliers.coeff(0);
    L.coeffRef(offl + 1) = lagrangeMultipliers.coeff(1);
    L.coeffRef(offl + 2) = lagrangeMultipliers.coeff(2);
    L.coeffRef(offl + 3) = lagrangeMultipliers.coeff(3);
}


DSSCylindricalJoint::~DSSCylindricalJoint() = default;
