//
// Created by saogregl on 09/06/18.
//

#include "DSSConstraintMate.h"

DSSConstraintMate::DSSConstraintMate() : clamp_value(0.6), clamp(true), currentTime(0) {

}

DSSConstraintMate::~DSSConstraintMate() {

}

bool DSSConstraintMate::isConstrainedX() {
    return true;
}

bool DSSConstraintMate::isConstrainedY() {
    return true;
}

bool DSSConstraintMate::isConstrainedZ() {
    return true;
}

bool DSSConstraintMate::isConstrainedRX() {
    return true;
}

bool DSSConstraintMate::isConstrainedRY() {
    return true;
}

bool DSSConstraintMate::isConstrainedRZ() {
    return true;
}

void DSSConstraintMate::initialize(std::shared_ptr<DSSRigidBody> _body1,
                                   std::shared_ptr<DSSRigidBody> _body2,
                                   const DSSAuxiliaryFrame &_localFrameOnBody1,
                                   const DSSAuxiliaryFrame &_localFrameOnBody2) {


    this->nOfDOFsConstrained = 5;
    this->nOfDOFsLeft = 1;

    Bodyi = _body1.get();
    Bodyj = _body2.get();

    constraint_x.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    constraint_y.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    constraint_z.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    constraint_rx.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());
    constraint_ry.setReferences(&Bodyi->getBodyVariable(), &Bodyj->getBodyVariable());

    localFrameOnBody1 = _localFrameOnBody1;
    localFrameOnBody2 = _localFrameOnBody2;

    auto absFrameBody1 = DSSAuxiliaryFrame();
    auto absFrameBody2 = DSSAuxiliaryFrame();

    Bodyi->transformChildToParent(localFrameOnBody1, absFrameBody1);
    Bodyj->transformChildToParent(localFrameOnBody2, absFrameBody2);

    localFrameOnBody1.setRotationMatrix(localFrameOnBody1.buildRotationMatrixFromQuat());
    localFrameOnBody2.setRotationMatrix(localFrameOnBody2.buildRotationMatrixFromQuat());


}

void DSSConstraintMate::update(double time) {


    auto aframe = DSSAuxiliaryFrame();
    auto aframe2 = DSSAuxiliaryFrame();

    Bodyi->transformChildToParent(localFrameOnBody1, aframe);
    Bodyj->transformChildToParent(localFrameOnBody2, aframe2);

    Bodyi->syncRotationMatrix();
    Bodyj->syncRotationMatrix();
    aframe.setRotationMatrix(aframe.buildRotationMatrixFromQuat());
    aframe2.setRotationMatrix(aframe2.buildRotationMatrixFromQuat());

    localFrameOnBody1.setRotationMatrix(localFrameOnBody1.buildRotationMatrixFromQuat());
    localFrameOnBody2.setRotationMatrix(localFrameOnBody2.buildRotationMatrixFromQuat());

    aframe.setRotationMatrix(aframe.buildRotationMatrixFromQuat());
    aframe2.setRotationMatrix(aframe2.buildRotationMatrixFromQuat());

    auto p1_abs = aframe.getPosition();
    auto p2_abs = aframe2.getPosition();


    DSSAuxiliaryFrame bframe = DSSAuxiliaryFrame();
    Bodyj->transformParentToChild(aframe, bframe);
    aframe2.transformParentToChild(bframe, aframe);

    // Now 'aframe' contains the position/rotation of frame 1 respect to frame 2, in frame 2 coords.

    Eigen::Matrix3d Jx1, Jx2, Jr1, Jr2, Jw1, Jw2;
    Eigen::Matrix3d mtempM, mtempQ;
    aframe2.setRotationMatrix(aframe2.buildRotationMatrixFromQuat());

    auto absplane = Bodyj->getRotationMatrix() * aframe2.getRotationMatrix();

    Jx1 = absplane.transpose();
    Jx2 = absplane.transpose();
    Jx2 = -Jx2;

    Jw1 = absplane * Bodyi->getRotationMatrix();
    Jw2 = absplane * Bodyj->getRotationMatrix();

    mtempM = localFrameOnBody1.getSkewSymmetricPos();
    Jr1 = Jw1 * mtempM;
    Jr1 = -Jr1;

    mtempM = localFrameOnBody2.getSkewSymmetricPos();
    Jr2 = Jw2 * mtempM;
    Jr2 = -Jr2;

    auto p2p1_base2 = (Bodyj->getRotationMatrix()) * ((p1_abs - p2_abs));
    mtempM = DSSTransform::makeSkewsymmetric(p2p1_base2);
    mtempQ = localFrameOnBody2.getRotationMatrix() * mtempM;

    Jr2 = Jr2 + mtempQ;

    Jw2 = -Jw2;

    // Premultiply by Jw1 and Jw2 by  0.5*[Fp(q_resid)]' to get residual as imaginary part of a quaternion.
    // For small misalignment this effect is almost insignificant cause [Fp(q_resid)]=[I],
    // but otherwise it is needed (if you want to use the stabilization term - if not, you can live without).
    mtempM = DSSTransform::makeSkewsymmetric(aframe.getOrientation().vec() * 0.5);
    mtempM(0, 0) = 0.5 * aframe.getOrientation().w();
    mtempM(1, 1) = 0.5 * aframe.getOrientation().w();
    mtempM(2, 2) = 0.5 * aframe.getOrientation().w();
    mtempQ = mtempM.transpose() * Jw1;
    Jw1 = mtempQ;
    mtempQ = mtempM.transpose() * Jw2;
    Jw2 = mtempQ;


    //Constraints in X , Body i
    constraint_x.setViolation(aframe.getPosition().x());
    constraint_x.getCq1()->coeffRef(0, 0) = Jx1(0, 0);
    constraint_x.getCq1()->coeffRef(0, 1) = Jx1(0, 1);
    constraint_x.getCq1()->coeffRef(0, 2) = Jx1(0, 2);
    constraint_x.getCq1()->coeffRef(0, 3) = Jr1(0, 0);
    constraint_x.getCq1()->coeffRef(0, 4) = Jr1(0, 1);
    constraint_x.getCq1()->coeffRef(0, 5) = Jr1(0, 2);
    //Body j
    constraint_x.getCq2()->coeffRef(0, 0) = Jx2(0, 0);
    constraint_x.getCq2()->coeffRef(0, 1) = Jx2(0, 1);
    constraint_x.getCq2()->coeffRef(0, 2) = Jx2(0, 2);
    constraint_x.getCq2()->coeffRef(0, 3) = Jr2(0, 0);
    constraint_x.getCq2()->coeffRef(0, 4) = Jr2(0, 1);
    constraint_x.getCq2()->coeffRef(0, 5) = Jr2(0, 2);

    //Constraints in Y
    //Body i
    constraint_y.setViolation(aframe.getPosition().y());
    constraint_y.getCq1()->coeffRef(0, 0) = Jx1(1, 0);
    constraint_y.getCq1()->coeffRef(0, 1) = Jx1(1, 1);
    constraint_y.getCq1()->coeffRef(0, 2) = Jx1(1, 2);
    constraint_y.getCq1()->coeffRef(0, 3) = Jr1(1, 0);
    constraint_y.getCq1()->coeffRef(0, 4) = Jr1(1, 1);
    constraint_y.getCq1()->coeffRef(0, 5) = Jr1(1, 2);
    //Body j
    constraint_y.getCq2()->coeffRef(0, 0) = Jx2(1, 0);
    constraint_y.getCq2()->coeffRef(0, 1) = Jx2(1, 1);
    constraint_y.getCq2()->coeffRef(0, 2) = Jx2(1, 2);
    constraint_y.getCq2()->coeffRef(0, 3) = Jr2(1, 0);
    constraint_y.getCq2()->coeffRef(0, 4) = Jr2(1, 1);
    constraint_y.getCq2()->coeffRef(0, 5) = Jr2(1, 2);


    //Constraints in Z
    //Body i
    constraint_z.setViolation(aframe.getPosition().z());
    constraint_z.getCq1()->coeffRef(0, 0) = Jx1(2, 0);
    constraint_z.getCq1()->coeffRef(0, 1) = Jx1(2, 1);
    constraint_z.getCq1()->coeffRef(0, 2) = Jx1(2, 2);
    constraint_z.getCq1()->coeffRef(0, 3) = Jr1(2, 0);
    constraint_z.getCq1()->coeffRef(0, 4) = Jr1(2, 1);
    constraint_z.getCq1()->coeffRef(0, 5) = Jr1(2, 2);
    //Body j
    constraint_z.getCq2()->coeffRef(0, 0) = Jx2(2, 0);
    constraint_z.getCq2()->coeffRef(0, 1) = Jx2(2, 1);
    constraint_z.getCq2()->coeffRef(0, 2) = Jx2(2, 2);
    constraint_z.getCq2()->coeffRef(0, 3) = Jr2(2, 0);
    constraint_z.getCq2()->coeffRef(0, 4) = Jr2(2, 1);
    constraint_z.getCq2()->coeffRef(0, 5) = Jr2(2, 2);


    //Constraints in RX
    //Body i
    constraint_rx.setViolation(aframe.getOrientation().x());
    constraint_rx.getCq1()->coeffRef(0, 0) = 0;
    constraint_rx.getCq1()->coeffRef(0, 1) = 0;
    constraint_rx.getCq1()->coeffRef(0, 2) = 0;
    constraint_rx.getCq1()->coeffRef(0, 3) = Jw1(0, 0);
    constraint_rx.getCq1()->coeffRef(0, 4) = Jw1(0, 1);
    constraint_rx.getCq1()->coeffRef(0, 5) = Jw1(0, 2);
    //Body j
    constraint_rx.getCq2()->coeffRef(0, 0) = 0;
    constraint_rx.getCq2()->coeffRef(0, 1) = 0;
    constraint_rx.getCq2()->coeffRef(0, 2) = 0;
    constraint_rx.getCq2()->coeffRef(0, 3) = Jw2(0, 0);
    constraint_rx.getCq2()->coeffRef(0, 4) = Jw2(0, 1);
    constraint_rx.getCq2()->coeffRef(0, 5) = Jw2(0, 2);


    //Constraints in RY
    //Body i
    constraint_ry.setViolation(aframe.getOrientation().y());
    constraint_ry.getCq1()->coeffRef(0, 0) = 0;
    constraint_ry.getCq1()->coeffRef(0, 1) = 0;
    constraint_ry.getCq1()->coeffRef(0, 2) = 0;
    constraint_ry.getCq1()->coeffRef(0, 3) = Jw1(1, 0);
    constraint_ry.getCq1()->coeffRef(0, 4) = Jw1(1, 1);
    constraint_ry.getCq1()->coeffRef(0, 5) = Jw1(1, 2);
    //Body j
    constraint_ry.getCq2()->coeffRef(0, 0) = 0;
    constraint_ry.getCq2()->coeffRef(0, 1) = 0;
    constraint_ry.getCq2()->coeffRef(0, 2) = 0;
    constraint_ry.getCq2()->coeffRef(0, 3) = Jw2(1, 0);
    constraint_ry.getCq2()->coeffRef(0, 4) = Jw2(1, 1);
    constraint_ry.getCq2()->coeffRef(0, 5) = Jw2(1, 2);




    //Constraints in RZ
    //Body i
    constraint_rz.setViolation(aframe.getOrientation().z());
    constraint_rz.getCq1()->coeffRef(0, 0) = 0;
    constraint_rz.getCq1()->coeffRef(0, 1) = 0;
    constraint_rz.getCq1()->coeffRef(0, 2) = 0;
    constraint_rz.getCq1()->coeffRef(0, 3) = Jw1(2, 0);
    constraint_rz.getCq1()->coeffRef(0, 4) = Jw1(2, 1);
    constraint_rz.getCq1()->coeffRef(0, 5) = Jw1(2, 2);
    //Body j
    constraint_rz.getCq2()->coeffRef(0, 0) = 0;
    constraint_rz.getCq2()->coeffRef(0, 1) = 0;
    constraint_rz.getCq2()->coeffRef(0, 2) = 0;
    constraint_rz.getCq2()->coeffRef(0, 3) = Jw2(2, 0);
    constraint_rz.getCq2()->coeffRef(0, 4) = Jw2(2, 1);
    constraint_rz.getCq2()->coeffRef(0, 5) = Jw2(2, 2);


}

void DSSConstraintMate::injectMobilizers(DSSSolverInterface &solver_interface) {

    solver_interface.insertMobilizer(&constraint_x);
    solver_interface.insertMobilizer(&constraint_y);
    solver_interface.insertMobilizer(&constraint_z);
    solver_interface.insertMobilizer(&constraint_rx);
    solver_interface.insertMobilizer(&constraint_ry);
    solver_interface.insertMobilizer(&constraint_rz);

}

void DSSConstraintMate::scatterBodyLagrangian(const int offl, Eigen::VectorXd &L) {
    lagrangeMultipliers.coeffRef(0) = L.coeff(offl + 0);
    lagrangeMultipliers.coeffRef(1) = L.coeff(offl + 1);
    lagrangeMultipliers.coeffRef(2) = L.coeff(offl + 2);
    lagrangeMultipliers.coeffRef(3) = L.coeff(offl + 3);
    lagrangeMultipliers.coeffRef(4) = L.coeff(offl + 4);
    lagrangeMultipliers.coeffRef(5) = L.coeff(offl + 5);

}

void DSSConstraintMate::retrieveBodyLagrangian(const int offl, Eigen::VectorXd &L) {
    L.coeffRef(offl + 0) = lagrangeMultipliers.coeff(0);
    L.coeffRef(offl + 1) = lagrangeMultipliers.coeff(1);
    L.coeffRef(offl + 2) = lagrangeMultipliers.coeff(2);
    L.coeffRef(offl + 3) = lagrangeMultipliers.coeff(3);
    L.coeffRef(offl + 4) = lagrangeMultipliers.coeff(4);
    L.coeffRef(offl + 5) = lagrangeMultipliers.coeff(5);


}

void DSSConstraintMate::bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned int off, const Eigen::VectorXd &L, double c) {
    constraint_x.multiplyAndAdd(Vec, L.coeff(off + 0) * c);
    constraint_y.multiplyAndAdd(Vec, L.coeff(off + 1) * c);
    constraint_z.multiplyAndAdd(Vec, L.coeff(off + 2) * c);
    constraint_rx.multiplyAndAdd(Vec, L.coeff(off + 3) * c);
    constraint_ry.multiplyAndAdd(Vec, L.coeff(off + 4) * c);
    constraint_rz.multiplyAndAdd(Vec, L.coeff(off + 5) * c);

}

void DSSConstraintMate::bodyLoadConstraintResiduals(Eigen::VectorXd &Vec, unsigned int off, double c) {
    //Lets assume clamp

    auto viol_x = c * constraint_x.getResidual();
    auto viol_y = c * constraint_y.getResidual();
    auto viol_z = c * constraint_z.getResidual();
    auto viol_d1 = c * constraint_rx.getResidual();
    auto viol_d2 = c * constraint_ry.getResidual();
    auto viol_d3 = c * constraint_rz.getResidual();

    if (clamp) {
        viol_x = std::min(std::max(viol_x, -0.6), 0.6);
        viol_y = std::min(std::max(viol_y, -0.6), 0.6);
        viol_z = std::min(std::max(viol_z, -0.6), 0.6);
        viol_d1 = std::min(std::max(viol_d1, -0.6), 0.6);
        viol_d2 = std::min(std::max(viol_d2, -0.6), 0.6);
        viol_d3 = std::min(std::max(viol_d3, -0.6), 0.6);

    }


    Vec.coeffRef(off + 0) += viol_x;
    Vec.coeffRef(off + 1) += viol_y;
    Vec.coeffRef(off + 2) += viol_z;
    Vec.coeffRef(off + 3) += viol_d1;
    Vec.coeffRef(off + 4) += viol_d2;
    Vec.coeffRef(off + 5) += viol_d3;

}

void DSSConstraintMate::bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec, unsigned int off, double c) {

//do nothing, there is no time derivative to calculate

}

void DSSConstraintMate::objPrepareSolverDescriptor(const int offv, const DSSStateDescriptorDelta &vel,
                                                   const Eigen::VectorXd &Residuals, const int offL,
                                                   const Eigen::VectorXd &L, const Eigen::VectorXd Qc) {
    constraint_x.setReaction(L(offL + 0));
    constraint_y.setReaction(L(offL + 1));
    constraint_z.setReaction(L(offL + 2));
    constraint_rx.setReaction(L(offL + 3));
    constraint_ry.setReaction(L(offL + 4));
    constraint_rz.setReaction(L(offL + 5));

    constraint_x.setbi(Qc(offL + 0));
    constraint_y.setbi(Qc(offL + 1));
    constraint_z.setbi(Qc(offL + 2));
    constraint_rx.setbi(Qc(offL + 3));
    constraint_ry.setbi(Qc(offL + 4));
    constraint_rz.setbi(Qc(offL + 5));

}

void DSSConstraintMate::objUpdateFromSolverDescriptor(const int offv, DSSStateDescriptorDelta &vel, const int offL,
                                                      Eigen::VectorXd &L) {
    L(offL + 0) = constraint_x.getReaction();
    L(offL + 1) = constraint_y.getReaction();
    L(offL + 2) = constraint_z.getReaction();
    L(offL + 3) = constraint_rx.getReaction();
    L(offL + 4) = constraint_ry.getReaction();
    L(offL + 5) = constraint_rz.getReaction();

}
