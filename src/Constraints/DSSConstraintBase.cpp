#include "DSSConstraintBase.h"


DSSConstraintBase::DSSConstraintBase() : offsetp(0),
                                         offsetw(0),
                                         offsetl(0),
                                         nOfDOFsConstrained(0),
                                         nOfDOFsLeft(6) {
}


DSSConstraintBase::~DSSConstraintBase() {
}

int DSSConstraintBase::getDOFConstrained() const {
    return nOfDOFsConstrained;
}

int DSSConstraintBase::getDOFLeft() const {
    return nOfDOFsLeft;
}

void DSSConstraintBase::setOffp(const int &_offp) {
    offsetp = _offp;
}


void DSSConstraintBase::setOffw(const int &_offw) {
    offsetw = _offw;
}


void DSSConstraintBase::setOffl(const int &_offl) {
    offsetl = _offl;
}

int DSSConstraintBase::getOffp() const {
    return offsetp;
}


int DSSConstraintBase::getOffw() const {
    return offsetw;
}


int DSSConstraintBase::getOffl() const {
    return offsetl;
}

