#pragma once
#include "DSSMobilityControl.h"
#include "../Core/DSSAuxiliaryFrame.h"

class DSSConstraintSpherical
{
protected: 
	DSSMobilityControl sphericalConstraint_x;	//x1-x2=0
	DSSMobilityControl sphericalConstraint_y;	//y1-y2=0
	DSSMobilityControl sphericalConstraint_z;	//z1-z2=0

	DSSAuxiliaryFrame localFrameOnBody1;
	DSSAuxiliaryFrame localFrameOnBody2;

	Eigen::Matrix<double, 3, 1,Eigen::DontAlign> lagrangeMultipliers;



public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    DSSConstraintSpherical();

    ~DSSConstraintSpherical();

	void buildJacobian();

	void buildResiduals();



};

