#pragma once
#include "DSSMobilityControl.h"
#include "../DSSRigidBody.h"
#include "DSSConstraintBase.h"
#include <memory>
#include "../debugFiles.h"
#include "../Core/DSSAuxiliaryFrame.h"


class DSSConstraintRevolute : public DSSConstraintBase
{
private:

	DSSMobilityControl sphericalConstraint_x;		//x2-x1=0
	DSSMobilityControl sphericalConstraint_y;		//y2-y1=0
	DSSMobilityControl sphericalConstraint_z;		//z2-z1=0
	DSSMobilityControl sphericalConstraint_dot1;	//dot(fi,hj)
	DSSMobilityControl sphericalConstraint_dot2;	//dot(gi,hj)


	DSSAuxiliaryFrame localFrameOnBody1;
	DSSAuxiliaryFrame localFrameOnBody2;

	double clamp_value;
	bool clamp;

	Eigen::Matrix<double, 5, 1> lagrangeMultipliers;

	double currentTime;


public:


	void injectMobilizers(DSSSolverInterface& solver_interface) override;
	void update(double time) override;

    DSSConstraintRevolute();

    ~DSSConstraintRevolute() override;

	void initialize(std::shared_ptr<DSSRigidBody> _body1,
	                std::shared_ptr<DSSRigidBody> _body2,
	                const DSSAuxiliaryFrame& _localFrameOnBody1,
	                const DSSAuxiliaryFrame& _localFrameOnBody2);

    void scatterBodyLagrangian(const int offl, Eigen::VectorXd& L) override;
	void retrieveBodyLagrangian(const int offl, Eigen::VectorXd& L) override;

    void bodyLoadCqLVector(Eigen::VectorXd& Vec, unsigned int off, const Eigen::VectorXd& L, double c) override;
	void bodyLoadConstraintResiduals(Eigen::VectorXd& Vec, unsigned int off, double c) override;
	void bodyLoadConstrTimeDerivatives(Eigen::VectorXd& Vec, unsigned int off, double c) override;

	void objPrepareSolverDescriptor(const int offv,
									const DSSStateDescriptorDelta &vel,
									const Eigen::VectorXd &Residuals,
									const int offL,
									const Eigen::VectorXd &L,
									const Eigen::VectorXd Qc) override;

	void objUpdateFromSolverDescriptor(const int offv,
									   DSSStateDescriptorDelta &vel,
									   const int offL,
									   Eigen::VectorXd &L) override;


};
