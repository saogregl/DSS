#include "DSSMobilityControl.h"


Eigen::Matrix<double, 1, 6> *DSSMobilityControl::getCq1() {
    return &Cq_bi;
}

Eigen::Matrix<double, 1, 6> *DSSMobilityControl::getCq2() {
    return &Cq_bj;
}

double DSSMobilityControl::getResidual() const {
    return constraintViolation;
}


double DSSMobilityControl::getReaction() const {
    return constraintReaction;
}

double DSSMobilityControl::getgi() const {
    return g_i;
}

double DSSMobilityControl::getbi() const {
    return b_i;
}

void DSSMobilityControl::setCq1(const int &index, const double &value) {
    Cq_bi(0, index) = value;
}

void DSSMobilityControl::setCq2(const int &index, const double &value) {
    Cq_bj(0, index) = value;
}

void DSSMobilityControl::setViolation(const double &_value) {
    constraintViolation = _value;
}

void DSSMobilityControl::setOffset(const int &_offset) {
    constraintOffset = _offset;
}

void DSSMobilityControl::setReferences(DSSBodyVariables *_refToi, DSSBodyVariables *_refToj) {
    refToBodyi = _refToi;
    refToBodyj = _refToj;
}

void DSSMobilityControl::multiplyAndAdd(Eigen::VectorXd &result, double factor) {
    int offi = refToBodyi->getOffset();
    int offj = refToBodyj->getOffset();

    if ((refToBodyi->isEnabled())) {
        for (int i = 0; i < 6; i++) {
            result(offi + i) += Cq_bi(i) * factor;
        }
    };

    if ((refToBodyj->isEnabled())) {
        for (int j = 0; j < 6; j++) {
            result(offj + j) += Cq_bj(j) * factor;
        }
    };
}

DSSMobilityControl::DSSMobilityControl() : constraintViolation(0),
                                           constraintOffset(0),
                                           g_i(0),
                                           b_i(0),
                                           constraintReaction(0)

{
}


DSSMobilityControl::~DSSMobilityControl()
= default;

void DSSMobilityControl::BuildJacobian(Eigen::SparseMatrix<double> &storage, const int &rowNum) {
    if ((refToBodyi->isEnabled()) == 1) {
        //TODO CHange insert here to coeffref
        storage.coeffRef(rowNum, refToBodyi->getOffset() + 0) = Cq_bi.coeff(0, 0 + 0);
        storage.coeffRef(rowNum, refToBodyi->getOffset() + 1) = Cq_bi.coeff(0, 0 + 1);
        storage.coeffRef(rowNum, refToBodyi->getOffset() + 2) = Cq_bi.coeff(0, 0 + 2);
        storage.coeffRef(rowNum, refToBodyi->getOffset() + 3) = Cq_bi.coeff(0, 0 + 3);
        storage.coeffRef(rowNum, refToBodyi->getOffset() + 4) = Cq_bi.coeff(0, 0 + 4);
        storage.coeffRef(rowNum, refToBodyi->getOffset() + 5) = Cq_bi.coeff(0, 0 + 5);
    };

    if ((refToBodyj->isEnabled()) == 1) {
        storage.coeffRef(rowNum, refToBodyj->getOffset() + 0) = Cq_bj.coeff(0, 0 + 0);
        storage.coeffRef(rowNum, refToBodyj->getOffset() + 1) = Cq_bj.coeff(0, 0 + 1);
        storage.coeffRef(rowNum, refToBodyj->getOffset() + 2) = Cq_bj.coeff(0, 0 + 2);
        storage.coeffRef(rowNum, refToBodyj->getOffset() + 3) = Cq_bj.coeff(0, 0 + 3);
        storage.coeffRef(rowNum, refToBodyj->getOffset() + 4) = Cq_bj.coeff(0, 0 + 4);
        storage.coeffRef(rowNum, refToBodyj->getOffset() + 5) = Cq_bj.coeff(0, 0 + 5);
    };

}

Eigen::Matrix<double, 6, 1> *DSSMobilityControl::getAuxiliaryMassCqi() {
    return &auxiliaryMassCqi;
}

Eigen::Matrix<double, 6, 1> *DSSMobilityControl::getAuxiliaryMassCqj() {
    return &auxiliaryMassCqj;
}

void DSSMobilityControl::updateAuxVectors() {
    //AuxiliaryMassCqi = M^-1 * Cq_i
    if(refToBodyi->isEnabled())
    {
        Eigen::Matrix<double,1,6> tempi;
        tempi = Cq_bi.transpose();
        refToBodyi->invMassMultV(auxiliaryMassCqi, tempi);
    }
    if(refToBodyj->isEnabled())
    {
        Eigen::Matrix<double,1,6> tempj;
        tempj = Cq_bj.transpose();
        refToBodyj->invMassMultV(auxiliaryMassCqj, tempj);
    }
    // 2- Compute g_i = [Cq_i]*[invM_i]*[Cq_i]' + cfm_i //THis is from Chrono
    //Set to zero
    g_i = 0;
    //Calculate
    if(refToBodyi->isEnabled())
    {
        g_i = Cq_bi * auxiliaryMassCqi;
    }
    if(refToBodyj->isEnabled())
    {
        g_i += Cq_bj * auxiliaryMassCqj;
    }
}


void DSSMobilityControl::incrementGenVariable(double increment) {
    if(refToBodyi->isEnabled())
    {
        refToBodyi->getVariable() += auxiliaryMassCqi * increment;
    }
    if(refToBodyj->isEnabled())
    {
        refToBodyj->getVariable() += auxiliaryMassCqj * increment;
    }

}

double DSSMobilityControl::calculateCq_q() {
    double cq_q = 0.0;

    if(refToBodyj->isEnabled())
    {
        cq_q += Cq_bi * refToBodyi->getVariable();
    }
    if(refToBodyj->isEnabled())
    {
        cq_q += Cq_bj * refToBodyj->getVariable();
    }
    return cq_q;

}

void DSSMobilityControl::setbi(const double val) {
    b_i = val;

}

void DSSMobilityControl::setgi(const double val) {
    g_i = val;
}


