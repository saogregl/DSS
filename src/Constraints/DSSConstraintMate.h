//
// Created by saogregl on 09/06/18.
//

#ifndef DSS_DSSCONSTRAINTMATE_H
#define DSS_DSSCONSTRAINTMATE_H


#include "DSSMobilityControl.h"
#include "../DSSRigidBody.h"
#include "DSSConstraintBase.h"
#include <memory>
#include "../debugFiles.h"
#include "../Core/DSSAuxiliaryFrame.h"


//Class for Mate constraint. The constrained DOF'S can be selected.

class DSSConstraintMate : public DSSConstraintBase {
protected:

    DSSMobilityControl constraint_x;        //x2-x1=0
    DSSMobilityControl constraint_y;        //y2-y1=0
    DSSMobilityControl constraint_z;        //z2-z1=0
    DSSMobilityControl constraint_rx;    //dot(fi,hj)
    DSSMobilityControl constraint_ry;    //dot(gi,hj)
    DSSMobilityControl constraint_rz;    //dot(gi,hj)

    DSSAuxiliaryFrame localFrameOnBody1;
    DSSAuxiliaryFrame localFrameOnBody2;

    Eigen::Matrix<double, 6, 1> lagrangeMultipliers;
    double currentTime;

    double clamp_value;
    bool clamp;


public:
    DSSConstraintMate();

    ~DSSConstraintMate() override;

    bool isConstrainedX();

    bool isConstrainedY();

    bool isConstrainedZ();

    bool isConstrainedRX();

    bool isConstrainedRY();

    bool isConstrainedRZ();


    void update(double time) override;

    void initialize(std::shared_ptr<DSSRigidBody> _body1,
                    std::shared_ptr<DSSRigidBody> _body2,
                    const DSSAuxiliaryFrame &_localFrameOnBody1,
                    const DSSAuxiliaryFrame &_localFrameOnBody2);


    void injectMobilizers(DSSSolverInterface &solver_interface) override;

    void scatterBodyLagrangian(const int offl, Eigen::VectorXd &L) override;

    void retrieveBodyLagrangian(const int offl, Eigen::VectorXd &L) override;

    void bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned int off, const Eigen::VectorXd &L, double c) override;

    void bodyLoadConstraintResiduals(Eigen::VectorXd &Vec, unsigned int off, double c) override;

    void bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec, unsigned int off, double c) override;

    void objPrepareSolverDescriptor(const int offv,
                                    const DSSStateDescriptorDelta &vel,
                                    const Eigen::VectorXd &Residuals,
                                    const int offL,
                                    const Eigen::VectorXd &L,
                                    const Eigen::VectorXd Qc) override;

    void objUpdateFromSolverDescriptor(const int offv,
                                       DSSStateDescriptorDelta &vel,
                                       const int offL,
                                       Eigen::VectorXd &L) override;

};


#endif //DSS_DSSCONSTRAINTMATE_H
