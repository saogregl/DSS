//
// Created by saogregl on 22/12/17.
//

#ifndef DSS_DSSTRANSLATIONALJOINT_H
#define DSS_DSSTRANSLATIONALJOINT_H
#include "DSSMobilityControl.h"
#include "../Core/DSSAuxiliaryFrame.h"
#include "../Core/DSSSolverInterface.h"
#include "../DSSRigidBody.h"
#include "DSSConstraintBase.h"

class DSSConstraintTranslational : public DSSConstraintBase {
public:

    void injectMobilizers(DSSSolverInterface& solver_interface) override;

    void update(double time) override;


    void initialize(std::shared_ptr<DSSRigidBody> _body1,
                    std::shared_ptr<DSSRigidBody> _body2,
                    const DSSAuxiliaryFrame& _localFrameOnBody1,
                    const DSSAuxiliaryFrame& _localFrameOnBody2);

    void bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned int off, const Eigen::VectorXd &L, const double c) override;
    void bodyLoadConstraintResiduals(Eigen::VectorXd &Vec, unsigned int off, const double c) override;
    void bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec, unsigned int off, const double c) override;
    void scatterBodyLagrangian(const int offl, Eigen::VectorXd& L) override;
    void retrieveBodyLagrangian(const int offl, Eigen::VectorXd& L) override;




private:
    DSSMobilityControl dotConstraintxz;	            //dot(xi, zj)
    DSSMobilityControl dotConstraintyz;	            //dot(yi, zj)
    DSSMobilityControl dotConstraintxd;	            //dot(xi,dij)
    DSSMobilityControl dotConstraintyd;	            //dot(yi,dij)
    DSSMobilityControl dotConstraintxx;	            //dot(xi, xj)


    DSSAuxiliaryFrame definitionFrameBodyi;
    DSSAuxiliaryFrame definitionFrameBodyj;

    Eigen::Vector3d dij;


    Eigen::Matrix<double,5,1> lagrangeMultipliers;

    double currentTime;


};


#endif //DSS_DSSTRANSLATIONALJOINT_H
