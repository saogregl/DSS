//
// Created by saogregl on 28/12/17.
//

#ifndef DSS_DSSCYLINDRICALJOINT_H
#define DSS_DSSCYLINDRICALJOINT_H

#include "DSSConstraintBase.h"

class DSSCylindricalJoint : public DSSConstraintBase
{
private:

    DSSMobilityControl dotConstraintxz;	            //dot(xi,zj)
    DSSMobilityControl dotConstraintyz;	            //dot(yi,zj)
    DSSMobilityControl dotConstraintxd;	            //dot(xi,dij)
    DSSMobilityControl dotConstraintyd;	            //dot(yi,dij)

    DSSAuxiliaryFrame definitionFrameBodyi;         //Local Joint frame on body i
    DSSAuxiliaryFrame definitionFrameBodyj;         //Local Joint frame on body j

    Eigen::Vector3d dij;                            //Cylindrical link direction vector
    Eigen::Matrix<double, 4, 1> lagrangeMultipliers;//Used to calculate the reaction forces.

    double currentTime;


public:



    void injectMobilizers(DSSSolverInterface& solver_interface) override;
    void update(double time) override;

    DSSCylindricalJoint();
    ~DSSCylindricalJoint() override;

    void initialize(std::shared_ptr<DSSRigidBody> _body1,
                    std::shared_ptr<DSSRigidBody> _body2,
                    const DSSAuxiliaryFrame& _localFrameOnBody1,
                    const DSSAuxiliaryFrame& _localFrameOnBody2);


    void scatterBodyLagrangian(const int offl, Eigen::VectorXd& L) override;
    void retrieveBodyLagrangian(const int offl, Eigen::VectorXd& L) override;

    void bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned int off, const Eigen::VectorXd &L, double c) override;
    void bodyLoadConstraintResiduals(Eigen::VectorXd& Vec, unsigned int off, double c) override;
    void bodyLoadConstrTimeDerivatives(Eigen::VectorXd& Vec, unsigned int off, double c) override;


};


#endif //DSS_DSSCYLINDRICALJOINT_H
