#include "DSSFunction.h"
#include "DSSMath.h"


DSSFunction::DSSFunction()
{
}


DSSFunction::~DSSFunction()
{
}

double DSSFunction::ydt(const double x) const {
    return (((y(x + BDF_STEP_LOW) - y(x)) / BDF_STEP_LOW));
}

double DSSFunction::ydtdt(const double x) const {
    return ((ydt(x + BDF_STEP_LOW) - ydt(x)) / BDF_STEP_LOW);
}

double DSSFunction::returnMaxValue(const double x1, const double x2) {
    return 0;
}

double DSSFunction::returnMinValue(const double x1, const double x2) {
    return 0;
}
