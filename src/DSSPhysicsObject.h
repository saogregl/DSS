#pragma once

#include "Core/DSSSolverInterface.h"
#include "Core/DSSStateDescriptor.h"

class DSSWorld;

class DSSPhysicsObject
{
protected:
	DSSWorld* world;

public:
	DSSPhysicsObject();
	virtual ~DSSPhysicsObject();

	DSSWorld* getWorld() const { return world; }
	void setWorld(DSSWorld* c_world);




	virtual void retrieveBodyState(const int& offp,
	                               DSSStateDescriptor& pos,
	                               const int& offv,
	                               DSSStateDescriptorDelta& vel,
	                               double time)
	{
	};

	virtual void disperseBodyState(const int& offp,
	                               const DSSStateDescriptor& pos,
	                               const int& offv,
	                               const DSSStateDescriptorDelta& vel,
	                               double time)
	{
	};

	virtual void retrieveBodyAcceleration(const int& offa,
	                                      DSSStateDescriptorDelta& acc)
	{
	};

	virtual void disperseBodyAcceleration(const int& offa,
	                                      const DSSStateDescriptorDelta& acc)
	{
	};

	virtual void bodyStateIncrement(const int& offp,
	                                DSSStateDescriptor& pos_new,
	                                const DSSStateDescriptor& pos_old,
	                                const int& offv,
	                                const DSSStateDescriptorDelta& vel)
	{
	};

	virtual void positionIncrement(const int& offp,
	                               DSSStateDescriptor& pos_new,
	                               const int& offv,
	                               DSSStateDescriptorDelta& Dx)
	{
	};

	virtual void objPrepareSolverDescriptor(const int offv,
											const DSSStateDescriptorDelta& vel,
											const Eigen::VectorXd& Residuals,
											const int offL,
											const Eigen::VectorXd& L,
											const Eigen::VectorXd Qc){};

	virtual void objUpdateFromSolverDescriptor(const int offv,
											   DSSStateDescriptorDelta& vel,
											   const int offL,
											   Eigen::VectorXd& L) {};

	virtual void retrieveBodyLagrangian(const int offl, Eigen::VectorXd &L){};
	virtual void scatterBodyLagrangian(const int offl, Eigen::VectorXd &L) {};
	virtual void injectVariables(DSSSolverInterface& solver_interface) {};
	virtual void injectMobilizers(DSSSolverInterface& solver_interface) {};
	virtual void update(double time) = 0 ;
	virtual void bodyLoadForceVector(Eigen::VectorXd& Vec, unsigned int off, double c) {};
	virtual void bodyLoadMvVector(Eigen::VectorXd& Vec, unsigned int off, const Eigen::VectorXd& velocity, double c){ };
	virtual void bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned int off, const Eigen::VectorXd &L, const double c){ };
	virtual void bodyLoadConstraintResiduals(Eigen::VectorXd &Vec, unsigned int off, const double c){ };
	virtual void bodyLoadConstrTimeDerivatives(Eigen::VectorXd &Vec, unsigned int off, const double c){ };;

};

