#include "DSSPhysicsObject.h"



DSSPhysicsObject::DSSPhysicsObject() : world(nullptr)
{

}

DSSPhysicsObject::~DSSPhysicsObject()
= default;

void DSSPhysicsObject::setWorld(DSSWorld *c_world) {
    world = c_world;
}
