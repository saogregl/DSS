//
// Created by saogregl on 10/06/18.
//

#ifndef DSS_DSSFUNCTIONCONSTANT_H
#define DSS_DSSFUNCTIONCONSTANT_H


#include "DSSFunction.h"

class DSSFunctionConstant : public DSSFunction {
private:
    double constant;


public:
    DSSFunctionConstant();

    DSSFunctionConstant(const double &mconstant);

    ~DSSFunctionConstant() override;


    virtual double y(const double x) const override;

    virtual double ydt(const double x) const override;

    virtual double ydtdt(const double x) const override;

    void setConstant(const double &mconstant);


};


#endif //DSS_DSSFUNCTIONCONSTANT_H
