#pragma once
#include <vector>
#include <eigen3/Eigen/Dense>
#include "Core/DSSSpatialDescriptor.h"
#include "debugFiles.h"
#include "DSSPhysicsObject.h"


class DSSPhysicsBody : public DSSPhysicsObject
{
protected: 
	DSSSpatialDescriptor    bodyPosition;
	DSSSpatialDescriptor    bodyVelocity;
	DSSSpatialDescriptor    bodyAcceleration;
	Eigen::Matrix3d         rotationMatrix;

	int                     positionOffset;
	int                     angularOffset;
	int                     lagrangeOffset;

public:


	DSSPhysicsBody();
	virtual ~DSSPhysicsBody();


	Eigen::Vector3d& getPosition() {return bodyPosition.position; };
	Eigen::Quaterniond& getOrientation() { return bodyPosition.orientation; };
	const Eigen::Vector3d& getPosition() const { return bodyPosition.position; };
	const Eigen::Quaterniond& getOrientation() const {return bodyPosition.orientation; };

	Eigen::Vector3d getVelocity() const;
	Eigen::Vector3d getAcceleration() const;

	Eigen::Vector3d getOmegaL() const;
	Eigen::Vector3d getOmegaG() const;
	Eigen::Vector3d getAlfaL() const;
	Eigen::Vector3d getAlfaG() const;

	void setOmegaL(const Eigen::Vector3d& wl);
	void setOmegaG(const Eigen::Vector3d& wg);
	void setAlfaL(const Eigen::Vector3d& al);
	void setAlfaG(const Eigen::Vector3d& ag);


	void setPosition(const Eigen::Vector3d& _position);
	void setVelocity(const Eigen::Vector3d& _velocity);
	void setAcceleration(const Eigen::Vector3d& _acceleration);


	void transformChildToParent(const DSSSpatialDescriptor& frameToTransformLocal,
		DSSSpatialDescriptor& resultingFrameParent)const;

    void transformParentToChild(const DSSSpatialDescriptor &frameToTransformLocal,
                                DSSSpatialDescriptor &resultingFrameParent) const;

	void syncRotationMatrix();
	/*
	virtual void retrieveBodyState();
	virtual void disperseBodyState();
	virtual void retrieveBodyAcceleration();
	virtual void disperseBodyAcceleration();
	virtual void bodyStateIncrement();
	virtual void retrieveBodyLagrangian();
	*/


	int getOffp() const;
	int getOffw() const;
	int getOffl() const;

	void setOffp(const int _offp);
	void setOffw(const int _offw);
	void setOffl(const int _offl);



};

