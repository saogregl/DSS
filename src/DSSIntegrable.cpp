#include "DSSIntegrable.h"



DSSIntegrable::DSSIntegrable()
= default;


DSSIntegrable::~DSSIntegrable()
= default;

void DSSIntegrable::stateSetup(DSSStateDescriptor& X, DSSStateDescriptorDelta& V, DSSStateDescriptorDelta& A)
{
	X.reset(GetNcoords_x(), 1);
	X.storage.setZero(GetNcoords_x());
	V.reset(GetNcoords_v(), 1);
	V.storage.setZero(GetNcoords_v());
	A.reset(GetNcoords_a(), 1);
	A.storage.setZero(GetNcoords_a());

}
