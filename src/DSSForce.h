#pragma once
#include <eigen3/Eigen/Core>
#include "Core/DSSAuxiliaryFrame.h"

class DSSRigidBody;

class DSSForce
{
public:
	DSSForce();
	~DSSForce();
	
	
	//get functions for application points
	Eigen::Vector3d getApplicationPointRel() const { return applicPointRel; };
	Eigen::Vector3d getApplicationPointAbs() const { return applicPointAbs; };
	//set functions for application points
	void setApplicationPointRel(Eigen::Vector3d appRel);
	void setApplicationPointAbs(Eigen::Vector3d appAbs);

	//get functions for force vectors
	Eigen::Vector3d getForceRel() const { return forceVecRel; };
	Eigen::Vector3d getForceAbs() const { return forceVecAbs; };
	
	//set force direction
	void setDirRel(Eigen::Vector3d dirRel);
	void setDirAbs(Eigen::Vector3d dirAbs);
	
	//getBody
	DSSRigidBody* getBody() const { return body; };
	void setBody(DSSRigidBody *parentBody) { body = parentBody; };
	
	//adjust force
	void setForce(double force);
	double getForce() const { return forceMagnitude; };
	
	//get lagrangian force
	Eigen::Matrix<double, 6, 1>* getQf();

    //Calculations
    void updateState(double time);
    void setBodyForces(Eigen::Vector3d lagrangianForce, Eigen::Vector3d lagrangianTorque);


private:
	DSSRigidBody* body;
	Eigen::Vector3d applicPointRel;
	Eigen::Vector3d applicPointAbs;

	Eigen::Vector3d forceVecRel;
	Eigen::Vector3d forceVecAbs;

	Eigen::Vector3d forceDirRel;
	Eigen::Vector3d forceDirAbs;


	Eigen::Matrix<double,6,1>* Qf;


	double forceMagnitude;


};

