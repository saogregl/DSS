#include "DSSForce.h"

#include "DSSRigidBody.h"


DSSForce::DSSForce() 	:body(nullptr),
						applicPointRel(0, 0, 0),
						applicPointAbs(0, 0, 0),
						forceVecRel(0, 0, 0),
						forceVecAbs(0, 0, 0),
						forceMagnitude(0)
{
	Qf = new Eigen::Matrix<double, 6, 1>();
}


DSSForce::~DSSForce()
{
	delete Qf;
}

void DSSForce::setApplicationPointRel(Eigen::Vector3d appRel)
{
	applicPointRel = std::move(appRel);
}

void DSSForce::setApplicationPointAbs(Eigen::Vector3d appAbs)
{
	applicPointAbs = std::move(appAbs);
}

void DSSForce::setDirRel(Eigen::Vector3d dirRel)
{
	forceDirRel = std::move(dirRel);
}

void DSSForce::setDirAbs(Eigen::Vector3d dirAbs)
{
	forceDirAbs = std::move(dirAbs);
}

void DSSForce::setForce(double force)
{
	forceMagnitude = force;
}

void DSSForce::updateState(double time)
{
	DSSRigidBody * body1 = this->getBody();
	body1->syncRotationMatrix();

	//The user should specify a direction vector for the force and the magnitude.
	//The direction vector should be given in local body coordinates.
	forceDirAbs = body1->getRotationMatrix()*forceDirRel;
	forceVecAbs = forceMagnitude*forceDirAbs;
	forceVecRel = body1->getRotationMatrix().transpose()*forceVecAbs;

	//The application point is assumed to be given in local coordinates
	Qf->coeffRef(0) = forceVecAbs.x();
	Qf->coeffRef(1) = forceVecAbs.y();
	Qf->coeffRef(2) = forceVecAbs.z();

	Eigen::Matrix3d applicPointSkewSymm = DSSTransform::makeSkewsymmetric(applicPointRel);
	Eigen::Vector3d Qf_w = applicPointSkewSymm*body1->getRotationMatrix().transpose()*forceVecAbs;

	Qf->coeffRef(3) = Qf_w.x();
	Qf->coeffRef(4) = Qf_w.y();
	Qf->coeffRef(5) = Qf_w.z();



}

Eigen::Matrix<double, 6, 1>* DSSForce::getQf()
{
	return Qf;
}

void DSSForce::setBodyForces(Eigen::Vector3d lagrangianForce, Eigen::Vector3d lagrangianTorque) {
	//Get the relative application point and make it's skew symmetric matrix for the cross product.
	auto relApplicPointStar = DSSTransform::makeSkewsymmetric(this->getApplicationPointRel());
	//force is the absolute force, nothing to do here;
	lagrangianForce  = this->getForceAbs();
	//the force is given by -relApplicPoint x relForce which can be written using the cross product matrix
	lagrangianTorque = (-1.0f)*relApplicPointStar * this->getForceRel();

}
