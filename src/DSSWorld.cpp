#include "DSSWorld.h"
#include "Timesteppers/DSSTimestepperNewmark.h"
#include "Timesteppers/DSSTimestepperEulerImp.h"
#include <iostream>
#include "Eigen/Dense"
#include <fstream>


DSSWorld::DSSWorld() : currentStep(0),
					   currentTime(0),
					   step(0.01),
					   endTime(2),
					   maxTimeStep(100),
					   minTimeStep(2),
					   gravityAccelerationVector(Eigen::Vector3d(0,0,-9.8)),
					   maxNumIter(150)
                       //solverInterface(std::make_shared<DSSSolverInterface>())

{
	timestepper = std::make_shared<DSSTimestepperEulerImp>(this);
	solverInterface = std::make_shared<DSSSolverInterface>();
	world = this;
}


DSSWorld::~DSSWorld()
= default;

void DSSWorld::solveDynamics(double time)
{
	//updates Jacobians for links; updates auxiliary coordinate systems i.e.
	//csys used to create links.
	update(time);

	//Set offsets; counts how many variables, links and bodies are there in the system.
	//setup() needs to be called before prepareSolverInterface() 
	setup();
	currentStep++;

	//injects variables from bodies to the solver interface. This needs to be done so the 
	//solver can access variables such as mass and inertia from rigidbody objects. This also
	//injects mobilizers (where the jacobians are stored) so that the interface
	//can prepare the global jacobian from the individual ones.
	prepareSolverInterface(*solverInterface);
	solverInterface->updateCounters();


	//Discretize and solve the problem integrating the index-3 DAE directly.
	timestepper->advanceSolution(step);


}

int DSSWorld::GetNcoords_x()
{
	return nGeneralizedCoord;
}

int DSSWorld::GetNcoords_v()
{
	return nGeneralizedCoord_w;

}

int DSSWorld::GetNcoords_a()
{
	return nGeneralizedCoord_w;
}

int DSSWorld::getNconstr()
{
	return nScalarConstraints;
}


void DSSWorld::retrieveState(DSSStateDescriptor& pos, DSSStateDescriptorDelta& vel, double time)
{
	DSSAssembly::retrieveBodyState(0, pos, 0, vel, time);
}

void DSSWorld::disperseState(const DSSStateDescriptor& pos,const DSSStateDescriptorDelta& vel, double time)
{
	DSSAssembly::disperseBodyState(0, pos, 0, vel, time);
	currentTime = time;
}

void DSSWorld::retrieveAcceleration(DSSStateDescriptorDelta& a)
{
	DSSAssembly::retrieveBodyAcceleration(0, a);
}

void DSSWorld::disperseAcceleration(const DSSStateDescriptorDelta& a)
{
	DSSAssembly::disperseBodyAcceleration(0, a);

}

void DSSWorld::convertAngularVel(const DSSStateDescriptorDelta& VAng, DSSStateDescriptorDelta& Vpdot)
{
	DSSAssembly::convertBodyAngularVel(VAng, Vpdot);
}

void DSSWorld::stateIncrement(DSSStateDescriptor& XNew, const DSSStateDescriptor& X, const DSSStateDescriptorDelta& Dy)
{
	DSSAssembly::bodyStateIncrement(0, XNew, X, 0, Dy);
}

void DSSWorld::structureProblem(Eigen::SparseMatrix<double>* jacobianMat, Eigen::SparseMatrix<double>* massMat, Eigen::Matrix<double, Eigen::Dynamic, 1>* generalizedForceVec, Eigen::Matrix<double, Eigen::Dynamic, 1>* residualVec)
{
	getSolverInterface()->structureProblem(jacobianMat, massMat, generalizedForceVec, residualVec);
}

void DSSWorld::loadForceVector(Eigen::VectorXd& Vec, double c)
{
	DSSAssembly::bodyLoadForceVector(Vec,0,c);
}

void DSSWorld::loadMvVector(Eigen::VectorXd& Vec, const Eigen::VectorXd& velocity, double c)
{
	DSSAssembly::bodyLoadMvVector(Vec,0,velocity,c);

}

void DSSWorld::LoadCqLVector(Eigen::VectorXd& Vec, const Eigen::VectorXd L, double c)
{
	DSSAssembly::bodyLoadCqLVector(Vec, 0, L, c);
}

void DSSWorld::LoadConstraintResiduals(Eigen::VectorXd& Vec, double c)
{
	DSSAssembly::bodyLoadConstraintResiduals(Vec, 0, c);
}

void DSSWorld::LoadConstrTimeDerivatives(Eigen::VectorXd& Vec, double c)
{
	DSSAssembly::bodyLoadConstrTimeDerivatives(Vec, 0, c);
}

void DSSWorld::stateSolveDelta(DSSStateDescriptorDelta& Dv,
                               Eigen::VectorXd& L,
                               Eigen::VectorXd& R, // change to const ref
                               Eigen::VectorXd Qc, // should be const ref
                               const DSSStateDescriptor& q,
                               const DSSStateDescriptorDelta& qdot,
                               Eigen::MatrixXd& jacobianMatrix,
						       Eigen::MatrixXd& massMatrix)
{


	objPrepareSolverDescriptor(0,Dv,R,0,L,Qc);
	solverSOR.solve(*solverInterface);
	objUpdateFromSolverDescriptor(0,Dv,0,L);

}

void DSSWorld::disperseReactions(const Eigen::VectorXd& L)
{
	//DSSAssembly::disper
}

void DSSWorld:: simulate()
{
	setup();

	while(currentTime < endTime)
	{
		solveDynamics(currentTime);

	}
}
