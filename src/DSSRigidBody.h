#pragma once

#include "Core/DSSBodyVariables.h"
#include "DSSPhysicsBody.h"
#include "debugFiles.h"
#include <vector>
#include <memory>
#include "DSSForce.h"
#include <eigen3/Eigen/Geometry>


//class DSSWorld;

class DSSRigidBody : public DSSPhysicsBody
{
private:
	friend class DSSWorld;

protected:
	std::vector<std::shared_ptr<DSSSpatialDescriptor>> childReferenceFrameList;
	std::vector<std::shared_ptr<DSSForce>> childForceList;


	DSSBodyVariables generalizedVariables;

	int rigidBodyndof = 7;
	int rigidBodyndof_w = 6;
	bool fixed = false;



public:
	Eigen::Vector3d absForce;
	Eigen::Vector3d relTorque;
	Eigen::Vector3d gyroscopicTorque;

	DSSBodyVariables& getBodyVariable();

	void updateForces(double time);
	void setFixed();
	bool isFixed() const;
	bool isEnabled() const;

	void setOrientation(const Eigen::Quaterniond& ori);
	Eigen::Matrix3d& getRotationMatrix();
	const Eigen::Matrix3d& getRotationMatrix() const;


	void setBodyMass(const double _mass);
	void setBodyInertia(Eigen::Matrix3d inertia);
	double getBodyMass();
	Eigen::Matrix3d getBodyInertia() const;

	int getndof_w() const;
	int getndof() const;

	virtual void injectVariables(DSSSolverInterface& solver_interface) override;
	void update(double time) override;
	void gyroscopicTorqueUpdate();
	//Functions for data exchange between solver interface and objects.

	void retrieveBodyState(const int& offp,
	                       DSSStateDescriptor& pos,
	                       const int& offv,
	                       DSSStateDescriptorDelta& vel,
	                       double time) override;

	void disperseBodyState(const int& offp,
	                       const DSSStateDescriptor& pos,
	                       const int& offv,
	                       const DSSStateDescriptorDelta& vel,
	                       double time) override;

	void retrieveBodyAcceleration(const int& offa,
	                              DSSStateDescriptorDelta& acc) override;


	void disperseBodyAcceleration(const int& offa,
	                              const DSSStateDescriptorDelta& acc) override;

	void bodyStateIncrement(const int& offp,
	                        DSSStateDescriptor& pos_new,
	                        const DSSStateDescriptor& pos_old,
	                        const int& offv,
							const DSSStateDescriptorDelta& vel) override;

	void scatterBodyLagrangian(const int offl,
							   Eigen::VectorXd &L) override;

	virtual void positionIncrement(const int& offp,
	                               DSSStateDescriptor& pos_new,
	                               const int& offv,
	                               DSSStateDescriptorDelta& Dx) override;

	void bodyLoadForceVector(Eigen::VectorXd& Vec,
                             unsigned int off,
                             double c) override;

	void bodyLoadMvVector(Eigen::VectorXd& Vec, unsigned int off,
                          const Eigen::VectorXd& velocity,
                          double c) override;

	void objPrepareSolverDescriptor(const int offv,
											const DSSStateDescriptorDelta& vel,
											const Eigen::VectorXd& Residuals,
											const int offL,
											const Eigen::VectorXd& L,
											const Eigen::VectorXd Qc) override;

	void objUpdateFromSolverDescriptor(const int offv,
									   DSSStateDescriptorDelta &vel,
									   const int offL,
									   Eigen::VectorXd &L) override;



	DSSRigidBody();


	~DSSRigidBody() override;
};

