//
// Created by saogregl on 06/01/18.
//

#ifndef DSS_DSSSOLVERSOR_H
#define DSS_DSSSOLVERSOR_H
#include "DSSSolver.h"

class DSSSolverSOR: public DSSSolver {
private:

    double omega;
    double sharpness;


public:
    DSSSolverSOR();
    ~DSSSolverSOR() override;
    bool solve(DSSSolverInterface& solverInterface) override;
    bool compute(DSSSolverInterface& solverInterface) override;


};


#endif //DSS_DSSSOLVERSOR_H
