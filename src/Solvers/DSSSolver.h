//
// Created by saogregl on 06/01/18.
//

#ifndef DSS_DSSSOLVER_H
#define DSS_DSSSOLVER_H

#include "../Core/DSSSolverInterface.h"

class DSSSolver {
protected:

    int maxNumIterations;
    int currentStep;

    double precision;

public:


    virtual bool solve(DSSSolverInterface& solverInterface) = 0;
    virtual bool compute(DSSSolverInterface& solverInterface) = 0;

    int getMaxNumIterations() const {return maxNumIterations;};
    int getCurrentStep() const {return currentStep; };
    double getPrecision() const {return precision; };

    void setPrecision(double tol ) { precision = tol; };
    void setMaxNumIterations(int niter) {maxNumIterations = niter; };



    DSSSolver();
    virtual ~DSSSolver();

};


#endif //DSS_DSSSOLVER_H
