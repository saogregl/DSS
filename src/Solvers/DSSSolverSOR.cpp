//
// Created by saogregl on 06/01/18.
//

#include "DSSSolverSOR.h"

bool DSSSolverSOR::solve(DSSSolverInterface &solverInterface) {
    auto problemVariables  = solverInterface.getVariables();
    auto problemMobilizers = solverInterface.getMobilizers();
    double maxResidual = 0;
    double maxDeltaLambda = 0;
    currentStep = 0;

    // 1 Update needed data.
    for (auto im : problemMobilizers )
    {
        im->updateAuxVectors();
    }


    // 2 Calculate initial guess (A = M^-1 * F) BUT THOSE ARE SPEEDS WHAT

    for (auto iv : problemVariables )
    {
        if(iv->isEnabled())
        {
            iv->invMassMultV(iv->getVariable(),iv->getForces());
        }
    }


    // 3 set reaction forces to zero

    for (auto im : problemMobilizers)
    {
        im->setReaction(0.0);
    }

    // 4)  Perform the iteration loops for the Symmetric Over-relaxation with omega = 1
    for (int iteration = 0; iteration < maxNumIterations; iteration++)
    {
        double maxResidual = 0;
        double maxDeltaLambda = 0;

        for (auto im : problemMobilizers) {
            //calculate linearized residual
            double currentResidual = im->calculateCq_q() + im->getbi();
            //absolute value
            double absResidual = std::abs(currentResidual);

            //delta_lambda = -(omega/g_i) * ([Cq_i]*q + b_i + cfm_i*l_i ) (This is the SOR Iteration)
            double deltaReaction = (-1.0) * (omega / im->getgi()) * currentResidual;


            // update:   lambda += delta_lambda;
            double previousReaction = im->getReaction();
            im->setReaction(previousReaction + deltaReaction);
            double newReaction = im->getReaction();
            double deltaReactionNew = newReaction - previousReaction; //not needed, same as deltaReaction

            //increment *speed
            im->incrementGenVariable(deltaReactionNew);
            //record current residuals and deltaLambdas
            maxDeltaLambda = std::fmax(maxDeltaLambda,std::fabs(deltaReactionNew));
            maxResidual = std::fmax(maxResidual,absResidual);

        }

        currentStep++;
        if(maxResidual < precision)
            break;
    }


    return true;
}

bool DSSSolverSOR::compute(DSSSolverInterface &solverInterface) {


    return true;
}

DSSSolverSOR::DSSSolverSOR() : omega(1),
                               sharpness(1)
{

}
DSSSolverSOR::~DSSSolverSOR() = default;
