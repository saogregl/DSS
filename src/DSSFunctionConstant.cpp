//
// Created by saogregl on 10/06/18.
//

#include "DSSFunctionConstant.h"

DSSFunctionConstant::~DSSFunctionConstant() {

}

DSSFunctionConstant::DSSFunctionConstant(const double &mconstant) { constant = mconstant; }

double DSSFunctionConstant::y(const double x) const {
    return constant;
}

double DSSFunctionConstant::ydt(const double x) const {
    return 0;
}

double DSSFunctionConstant::ydtdt(const double x) const {
    return 0;
}

void DSSFunctionConstant::setConstant(const double &mconstant) {
    constant = mconstant;

}


DSSFunctionConstant::DSSFunctionConstant() : constant(0) {}

