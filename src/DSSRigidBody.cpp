#include "DSSRigidBody.h"
#include <iostream>
#include <utility>


DSSBodyVariables& DSSRigidBody::getBodyVariable()
{
	return generalizedVariables;
}

void DSSRigidBody::setBodyMass(const double _mass)
{
	generalizedVariables.setBodyMass(_mass);
}

double DSSRigidBody::getBodyMass()
{
	return generalizedVariables.getBodyMass();
}

Eigen::Matrix3d DSSRigidBody::getBodyInertia() const
{
	return generalizedVariables.getInertia();
}

void DSSRigidBody::setBodyInertia(Eigen::Matrix3d inertia)
{
	generalizedVariables.getInertia() = inertia;
	generalizedVariables.getInvInertia() = inertia.inverse();


}

int DSSRigidBody::getndof_w() const
{
	return rigidBodyndof_w;
}

int DSSRigidBody::getndof() const
{
	return rigidBodyndof;
}

DSSRigidBody::DSSRigidBody() : absForce(0,0,0),
							   relTorque(0,0,0),
							   gyroscopicTorque(0,0,0),
							   fixed(false)

{
	childForceList.clear();
	childReferenceFrameList.clear();

}

DSSRigidBody::~DSSRigidBody() = default;

void DSSRigidBody::setOrientation(const Eigen::Quaterniond& ori)
{
	bodyPosition.orientation = ori;
	syncRotationMatrix();
	bodyPosition.orientation.normalize();
}

Eigen::Matrix3d& DSSRigidBody::getRotationMatrix()
{
	return rotationMatrix;
}


void DSSRigidBody::injectVariables(DSSSolverInterface& solver_interface)
{
	solver_interface.insertVariable(&generalizedVariables);
}

void DSSRigidBody::update(double time)
{
	gyroscopicTorqueUpdate();
	syncRotationMatrix();
	updateForces(time);

}

void DSSRigidBody::retrieveBodyState(const int& offp,
									 DSSStateDescriptor& pos,
                                     const int& offv,
                                     DSSStateDescriptorDelta& vel,
                                     double time)
{
	pos.pasteVector<double>(this->bodyPosition.position, offp, 0);
	pos.pasteQuaternion<double>(this->bodyPosition.orientation, offp + 3,0);
	vel.pasteVector<double>(this->bodyVelocity.position, offv, 0);
	vel.pasteVector<double>(this->getOmegaL(), offv + 3, 0);
}

void DSSRigidBody::disperseBodyState(const int& offp,
                                     const DSSStateDescriptor& pos,
                                     const int& offv,
                                     const DSSStateDescriptorDelta& vel,
                                     double time)
{
	this->setPosition(pos.clipVector<double>(offp, 0));
	this->setOrientation(pos.clipQuaternion<double>(offp + 3,0));
	this->setVelocity(vel.clipVector<double>(offv,0));
	this->setOmegaL(vel.clipVector<double>(offp + 3, 0));
    this->update(time);


}

void DSSRigidBody::retrieveBodyAcceleration(const int& offa, DSSStateDescriptorDelta& acc)
{
	acc.pasteVector<double>(this->bodyAcceleration.position, offa, 0);
	acc.pasteVector<double>(this->getAlfaL(), offa + 3, 0);
}

void DSSRigidBody::disperseBodyAcceleration(const int& offa, const DSSStateDescriptorDelta& acc)
{
	this->setAcceleration(acc.clipVector<double>(offa, 0));
	this->setAlfaL(acc.clipVector<double>(offa + 3, 0));
}

void DSSRigidBody::bodyStateIncrement(const int& offp,
                                      DSSStateDescriptor& pos_new,
                                      const DSSStateDescriptor& pos_old,
                                      const int& offv,
                                      const DSSStateDescriptorDelta& vel)
{
	// X_(i+1) = X_i + Dv, where Dv depends on which integrator is being used. Dv can be V*dT. 
	pos_new(offp) = pos_old(offp) + vel(offv);
	pos_new(offp + 1) = pos_old(offp + 1) + vel(offv + 1);
	pos_new(offp + 2) = pos_old(offp + 2) + vel(offv + 2);

	Eigen::Quaternion<double> oldrot = pos_old.clipQuaternion<double>(offp + 3,0);

	Eigen::Vector3d axisAbs = rotationMatrix * vel.clipVector<double>(offv + 3, 0);
	double rotationAngle = axisAbs.norm();
	axisAbs.normalize();

	Eigen::AngleAxisd axisConstructor;
	axisConstructor.axis() = axisAbs;
	axisConstructor.angle() = rotationAngle;

 	auto deltaRot = Eigen::Quaterniond(axisConstructor);
	
	auto newRotation = deltaRot * oldrot;	//q' = q*qdelta
	pos_new.pasteQuaternion(newRotation, offp + 3, 0);
}

void DSSRigidBody::updateForces(double time) {
    auto tempForceVec = Eigen::Vector3d::Zero(3);
    auto tempForceTor = Eigen::Vector3d::Zero(3);

	absForce.setZero(3);
	relTorque.setZero(3);

    //set absForce and relTorque from each force and add it to the body force/torque.
    for (auto &fi : childForceList) {

        fi->updateState(time);
        fi->setBodyForces(tempForceVec, tempForceTor);

        absForce  += tempForceVec;
        relTorque += tempForceTor;
    }
	//this is bad
	auto gravity = Eigen::Vector3d(0,0,-9.81);
    //Adds gravity
	absForce  += gravity*this->getBodyMass();
}

void DSSRigidBody::setFixed()
{
	fixed = true;
	generalizedVariables.disable();
}

bool DSSRigidBody::isFixed() const
{
	return fixed;
}

void DSSRigidBody::positionIncrement(const int& offp, DSSStateDescriptor& pos_new, const int& offv, DSSStateDescriptorDelta& Dx)
{
	pos_new(offp) = pos_new(offp) + Dx(offv);
	pos_new(offp + 1) = pos_new(offp + 1) + Dx(offv + 1);
	pos_new(offp + 2) = pos_new(offp + 2) + Dx(offv + 2);

	Eigen::Quaternion<double> deltarot;
	Eigen::Quaternion<double> oldrot;
	Eigen::Vector3d axis = pos_new.clipVector<double>(offp + 3,1);
	axis.normalize();


}

void DSSRigidBody::bodyLoadForceVector(Eigen::VectorXd& Vec, unsigned int off, double c)
{
	Vec.coeffRef(off + 0) += c*absForce.coeff(0);
	Vec.coeffRef(off + 1) += c*absForce.coeff(1);
	Vec.coeffRef(off + 2) += c*absForce.coeff(2);
	Vec.coeffRef(off + 3) += c*(relTorque.coeff(0));//- gyroscopicTorque.coeff(0));
	Vec.coeffRef(off + 4) += c*(relTorque.coeff(1));//- gyroscopicTorque.coeff(1));
	Vec.coeffRef(off + 5) += c*(relTorque.coeff(2));//- gyroscopicTorque.coeff(2));
}

void DSSRigidBody::bodyLoadMvVector(Eigen::VectorXd& Vec, unsigned off, const Eigen::VectorXd& velocity, double c)
{

	Vec.coeffRef(off + 0) += c * getBodyMass() * velocity.coeff(off + 0);
	Vec.coeffRef(off + 1) += c * getBodyMass() * velocity.coeff(off + 1);
	Vec.coeffRef(off + 2) += c * getBodyMass() * velocity.coeff(off + 2);
	Eigen::Vector3d ITimesvel = c * getBodyInertia() * velocity.block(off + 3 , 0, 3, 1);
	Vec.block(off + 3, 0, 3, 1) += ITimesvel;
}

void DSSRigidBody::scatterBodyLagrangian(const int offl, Eigen::VectorXd &L)
{
//TODO: Implement the scatter lagrangian function, as well as calculate the reaction forces.
}

void DSSRigidBody::gyroscopicTorqueUpdate() {
	auto angularVel = this->getOmegaL();
	gyroscopicTorque = angularVel.cross(getBodyInertia() * angularVel );
}

const Eigen::Matrix3d &DSSRigidBody::getRotationMatrix() const {
    return rotationMatrix;
}

bool DSSRigidBody::isEnabled() const {
	return !fixed;
}

void DSSRigidBody::objPrepareSolverDescriptor(const int offv,
											  const DSSStateDescriptorDelta &vel,
											  const Eigen::VectorXd &Residuals,
											  const int offL,
											  const Eigen::VectorXd &L,
											  const Eigen::VectorXd Qc) {
	this->generalizedVariables.getVariable() = vel.storage.block(offv,0,6,1);
	this->generalizedVariables.getForces() = Residuals.block(offv,0,6,1);


}

void DSSRigidBody::objUpdateFromSolverDescriptor(const int offv,
												 DSSStateDescriptorDelta &vel,
												 const int offL,
												 Eigen::VectorXd &L) {

	vel.storage.coeffRef(offv + 0) = this->generalizedVariables.getVariable().coeff(0);
	vel.storage.coeffRef(offv + 1) = this->generalizedVariables.getVariable().coeff(1);
	vel.storage.coeffRef(offv + 2) = this->generalizedVariables.getVariable().coeff(2);
	vel.storage.coeffRef(offv + 3) = this->generalizedVariables.getVariable().coeff(3);
	vel.storage.coeffRef(offv + 4) = this->generalizedVariables.getVariable().coeff(4);
	vel.storage.coeffRef(offv + 5) = this->generalizedVariables.getVariable().coeff(5);

}
