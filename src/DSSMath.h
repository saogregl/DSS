//
// Created by saogregl on 09/06/18.
//

#ifndef DSS_DSSMATH_H
#define DSS_DSSMATH_H

#include <cmath>
#include <cfloat>
#include <cassert>


static const double BDF_STEP_HIGH = 0.0001;
static const double BDF_STEP_LOW = 0.0000001;
static const double BDF_STEP_VERYLOW = 1.e-10;
static const double BDF_STEP_TOOLOW = 1.e-20;

class DSSMath {

};


#endif //DSS_DSSMATH_H
