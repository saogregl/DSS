#pragma once

#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cfloat>
#include <memory.h>


class DSSFunction
{
private:


public:

    enum FunctionType {
        CUSTOM,
        CONSTANT,
        RAMP,
        SINE,
        POLY
    };

	DSSFunction();

    virtual ~DSSFunction();

    virtual double y(const double x) const = 0;

    virtual double ydt(const double x) const;

    virtual double ydtdt(const double x) const;

    virtual double returnMaxValue(const double x1, const double x2);

    virtual double returnMinValue(const double x1, const double x2);


};

