#include "DSSTimestepperEulerImp.h"
#include <iostream>
#include <fstream>


DSSTimestepperEulerImp::DSSTimestepperEulerImp(DSSIntegrable* _integrable) : DSSTimestepper(_integrable)
{
}

DSSTimestepperEulerImp::~DSSTimestepperEulerImp()
= default;

void DSSTimestepperEulerImp::advanceSolution(const double timeStep)
{

	//Notation
	// [ H    Cq' ] [Dq] = [R]
	// [ Cq   0   ] [Dl] = [Qc]

	//Where: 
	// [ M - dt*dF/dv - dt^2*dF/dx    Cq' ] [ Dv     ] = [ M*(v_old - v_new) + dt*f]
	// [ Cq                           0   ] [ -dt*Dl ] = [ C/dt + Ct ]
	//
	//Linearizing

	// [ M - dt*dF/dv - dt^2*dF/dx    Cq' ] [ v_new  ] = [ M*(v_old) + dt*f]
	// [ Cq                           0   ] [ -dt*l  ] = [ C/dt + Ct ]

	DSSIntegrable* _integrable;
	_integrable = (this->integrable);


	_integrable->stateSetup(q, qDot, qDDot);


	Dl.resize(_integrable->getNconstr(), 1);
	Dl.setZero(_integrable->getNconstr());
	L.resize(_integrable->getNconstr(), 1);
	L.setZero(_integrable->getNconstr());
	Qc.resize(_integrable->getNconstr(), 1);
	Qc.setZero(_integrable->getNconstr());
	R.resize(_integrable->GetNcoords_v(), 1);
	R.setZero(_integrable->GetNcoords_v());

	auto jacobianMat = Eigen::SparseMatrix<double>();
	auto massMat = Eigen::SparseMatrix<double>();
	Eigen::Matrix<double, Eigen::Dynamic, 1> generalizedForceVec;
	Eigen::Matrix<double, Eigen::Dynamic, 1> residualVector;



	_integrable->retrieveState(q, qDot, currentTime);

	qDotOld = qDot;

	_integrable->loadForceVector(R, timeStep);
	_integrable->loadMvVector(R,qDot.getStorage(), 1);
	_integrable->LoadConstraintResiduals(Qc,1/timeStep);
	_integrable->LoadConstrTimeDerivatives(Qc, 1);

	_integrable->structureProblem(&jacobianMat, &massMat, &generalizedForceVec, &residualVector);

	Eigen::MatrixXd jMat = Eigen::MatrixXd(jacobianMat);
	Eigen::MatrixXd mMat = Eigen::MatrixXd(massMat);


	////Call solver 
	_integrable->stateSolveDelta(qDot, L, R, Qc, q, qDot, jMat,mMat);

    //Increment Lagrangians
	L *= (- 1 / timeStep);
    //increment accelerations
	_integrable->disperseAcceleration((qDot - qDotOld) * (1 / timeStep));
	currentTime += timeStep;



    _integrable->stateIncrement(q, q, qDot*timeStep);


	_integrable->disperseState(q, qDot, currentTime);
	_integrable->disperseReactions(L);

}
//TODO Input Nconstrains and NcoordsV for this function so we don't have to instantiate the integrable
void DSSTimestepperEulerImp::setupSolutionSpace() {

	DSSIntegrable* _integrable;
	_integrable = (this->integrable);


	_integrable->stateSetup(q, qDot, qDDot);


	Dl.resize(_integrable->getNconstr(), 1);
	Dl.setZero(_integrable->getNconstr());
	L.resize(_integrable->getNconstr(), 1);
	L.setZero(_integrable->getNconstr());
	Qc.resize(_integrable->getNconstr(), 1);
	Qc.setZero(_integrable->getNconstr());
	R.resize(_integrable->GetNcoords_v(), 1);
	R.setZero(_integrable->GetNcoords_v());


}
