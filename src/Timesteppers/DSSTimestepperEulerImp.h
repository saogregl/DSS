#pragma once
#include "DSSTimestepper.h"

class DSSTimestepperEulerImp : public DSSTimestepper
{
protected: 
	DSSStateDescriptorDelta qDotOld;


	Eigen::VectorXd Dl;
	Eigen::VectorXd R;
	Eigen::VectorXd Qc;

public:
	explicit DSSTimestepperEulerImp(DSSIntegrable* _integrable);
	~DSSTimestepperEulerImp() override;

	Type getType() const override { return Type::EULER_IMPLICIT; };

	void advanceSolution(const double timeStep) override;
	void setupSolutionSpace() override;
};

