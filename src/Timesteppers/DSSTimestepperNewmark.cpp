#include "DSSTimestepperNewmark.h"



DSSTimestepperNewmark::DSSTimestepperNewmark(DSSIntegrable* m_integrable) : DSSTimestepper(m_integrable)
{
}


DSSTimestepperNewmark::DSSTimestepperNewmark() : DSSTimestepper(nullptr)
{
}


DSSTimestepperNewmark::~DSSTimestepperNewmark()
= default;

void DSSTimestepperNewmark::advanceSolution(double time)
{

	// From Project Chrono/Wisconsin univ. lectures
	// [ M - dt*gamma*dF/dv - dt^2*beta*dF/dx    Cq' ] [ Da   ] = [ -M*(a_new) + f_new + Cq*l_new ]
	// [ Cq                                      0   ] [ Dl   ] = [ 1/(beta*dt^2)*C               ] ]3

	//DSSIntegrable* _integrable = dynamic_cast<DSSIntegrable*>(this->integrable);
	//_integrable->stateSetup(q, qDot, qDDot);

	//newPos.reset(_integrable->GetNcoords_x(), 1);
	//newVel.reset(_integrable->GetNcoords_v(), 1);
	//newAcc.reset(_integrable->GetNcoords_a(), 1);
	//Da.reset(_integrable->GetNcoords_a(), 1);
	//Dl.resize(_integrable->getNconstr(), 1);
	//L.resize(_integrable->getNconstr(), 1);
	//Qc.resize(_integrable->getNconstr(), 1);
	//R.resize(_integrable->GetNcoords_v(), 1);
	//prevR.resize(_integrable->GetNcoords_v(), 1);


}
