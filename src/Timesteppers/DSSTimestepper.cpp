#include "DSSTimestepper.h"





DSSTimestepper::~DSSTimestepper()
= default;

void DSSTimestepper::advanceSolution(double time)
{

}

double DSSTimestepper::getTolerance() const
{
	return toleranceStates;
}

double DSSTimestepper::getRelTolerance() const
{
	return relTolerance;
}

void DSSTimestepper::setTolerance(const double& _tol)
{
	toleranceStates = _tol;
}

void DSSTimestepper::setRelTolerance(const double& _reltol)
{
	relTolerance = _reltol;
}

void DSSTimestepper::setIntegrableReferences(DSSIntegrable* integrable)
{
	q.setIntegrable(integrable);
	qDot.setIntegrable(integrable);
	qDDot.setIntegrable(integrable);
}
