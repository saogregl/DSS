#pragma once
#include "../DSSIntegrable.h"

class	DSSTimestepper
{
public:
	DSSTimestepper(DSSIntegrable* _integrable) : integrable(_integrable), toleranceStates(1e-10),
	                                             toleranceLagrange(1e-10), relTolerance(1e-4),
	                                             numIterations(10), numSolverCalls(0), numSetupCalls(0),currentTime(0)
	{
		setIntegrableReferences(_integrable);
	};

	enum class Type
	{
		EULER_IMPLICIT = 0,
		NEWMARK= 1,
		CUSTOM = -1
	};

	virtual ~DSSTimestepper();


	virtual void setupSolutionSpace() {};
	virtual void advanceSolution(const double time);


	double getTolerance() const;
	double getRelTolerance() const;

	virtual Type getType() const { return Type::CUSTOM; };

	int getMaxIterations() const { return numIterations; };
	void setMaxIterations(int _maxIt) { numIterations = _maxIt;  };

	void setTolerance(const double& _tol);
	void setRelTolerance(const double& _reltol);

	int getNumberOfSolverCalls() const { return numSolverCalls; };
	int getNumberOfSetupCaslls() const { return numSetupCalls;  };

	virtual DSSStateDescriptor& getPosition() { return q; };
	virtual DSSStateDescriptorDelta& getVelocity() { return qDot; };
	virtual DSSStateDescriptorDelta& getAcceleration() { return qDDot; };
	void setIntegrableReferences(DSSIntegrable* integrable);

protected:	

	DSSIntegrable* integrable;


	//Main Vectors
	DSSStateDescriptor		q;		//position vector
	DSSStateDescriptorDelta qDot;	//velocity vector
	DSSStateDescriptorDelta qDDot;  //acceleration vector
	Eigen::VectorXd L;				//Lagrange multipliers vector

	double toleranceStates;
	double toleranceLagrange;
	double relTolerance;
	
	int numIterations;
	int numSolverCalls;
	int numSetupCalls;

	double currentTime;

};

