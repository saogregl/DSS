#pragma once
#include "DSSTimestepper.h"

class DSSTimestepperNewmark : public DSSTimestepper
{
private: 
	double gamma;
	double beta;
	
	DSSStateDescriptorDelta Da;
	Eigen::VectorXd Dl;

	DSSStateDescriptor newPos;
	DSSStateDescriptorDelta newVel;
	DSSStateDescriptorDelta newAcc;

	Eigen::VectorXd R;
	Eigen::VectorXd prevR;
	Eigen::VectorXd Qc;



public:
	explicit DSSTimestepperNewmark(DSSIntegrable * m_integrable);
	DSSTimestepperNewmark();
	~DSSTimestepperNewmark();

	void advanceSolution(const double time) override;


};

