#pragma once
#include <vector>
#include "DSSBodyVariables.h"
#include "../Constraints/DSSMobilityControl.h"

class DSSSolverInterface
{
protected: 
	std::vector<DSSBodyVariables*> variableList;
	std::vector<DSSMobilityControl*> mobilityControlList;

	//Number of active constraints and degrees of freedom (total)
	int numberOfMobilizers;
	int numberOfVariables ;

public:

	DSSSolverInterface();
	virtual ~DSSSolverInterface();



	void insertVariable(DSSBodyVariables* _variable);
	void insertMobilizer(DSSMobilityControl* _mobcontrol);

	std::vector<DSSBodyVariables*>& getVariables();
	std::vector<DSSMobilityControl*>& getMobilizers();

	int countTotalDofs();
	int countNumberOfMobilizers();
	void updateCounters();

	void structureProblem(Eigen::SparseMatrix<double>* jacobianMat,
	                      Eigen::SparseMatrix<double>* massMat,
	                      Eigen::Matrix<double, Eigen::Dynamic, 1>* generalizedForceVec,
	                      Eigen::Matrix<double, Eigen::Dynamic, 1>* residualVector);
	void resetVectors();


};

