#pragma once
#include <eigen3/Eigen/Core>
#include "../DSSIntegrable.h"
#include <iostream>

class DSSIntegrable;

class DSSStateDescriptor
{
protected: 
	DSSIntegrable* integrable;
public:
	Eigen::VectorXd storage;

	
	Eigen::VectorXd& getStorage() { return storage; };
	void reset(const int& rows, const int& cols);

	template <typename Derived>
	void pasteMatrix(Eigen::MatrixBase<Derived> _matr, const int& rowToInsert, const int& columnToInsert);

	template <typename Derived>
	void pasteVector(const Eigen::Vector3d& _vec, const int& rowToInsert, const int& columnToInsert);

	template <typename Derived>
	void pasteQuaternion(const Eigen::Quaternion<Derived>& _quat, const int& rowToInsert, const int& columnToInsert);

	template <typename Derived>
	Eigen::Matrix<Derived, 3, 1> clipVector(const int& rowToClip, const int& columnToClip) const;


	template <typename Derived>
	Eigen::Matrix<Derived, 4, 1> clipQuatVector(const int& rowToClip, const int& columnToClip) const;

	template <typename Derived>
	Eigen::Matrix<Derived, Eigen::Dynamic, Eigen::Dynamic> clipMatrix(const int& rowToClip, const int& columnToClip, const int& nrowsToClip, const int& nColsToClip) const;


	template <typename Derived>
	Eigen::Quaternion<Derived> clipQuaternion(const int& rowToClip, const int& columnToClip) const;


	//Operator overloading
	DSSStateDescriptor operator+(DSSStateDescriptor& state1);
	DSSStateDescriptor operator-(DSSStateDescriptor& state1);

	DSSStateDescriptor operator*(const double c);
	DSSStateDescriptor operator/(const double c);

	//access operator
	const double& operator()(const int index) const;
	double& operator()(const int index);


	//Setting references. The State descriptor needs to know the system (or the parent integrable object) in 
	//order to invoke customized state increments (for q in quaternions, v in wvel, etc), know the number of variables, etc.
	void setIntegrable(DSSIntegrable* _integrable);
	DSSStateDescriptor();

	//void incrementState();
	DSSStateDescriptor(DSSIntegrable* _integrable);
	//CopyConstructor
	DSSStateDescriptor(const DSSStateDescriptor& obj);
	~DSSStateDescriptor();
};

template <typename Derived>
void DSSStateDescriptor::pasteMatrix(Eigen::MatrixBase<Derived> _matr, const int& rowToInsert, const int& columnToInsert)
{
	storage.block(rowToInsert, columnToInsert, _matr.rows(), _matr.cols()) = _matr;
}

template <typename Derived>
void DSSStateDescriptor::pasteVector(const Eigen::Vector3d& _vec, const int& rowToInsert, const int& columnToInsert)
{
	storage(rowToInsert + 0) = _vec.x();
	storage(rowToInsert + 1) = _vec.y();
	storage(rowToInsert + 2) = _vec.z();
}

template <typename Derived>
void DSSStateDescriptor::pasteQuaternion(const Eigen::Quaternion<Derived>& _quat, const int& rowToInsert, const int& columnToInsert)
{
	storage.coeffRef(rowToInsert + 0, columnToInsert) = _quat.w();
	storage.coeffRef(rowToInsert + 1, columnToInsert) = _quat.x();
	storage.coeffRef(rowToInsert + 2, columnToInsert) = _quat.y();
	storage.coeffRef(rowToInsert + 3, columnToInsert) = _quat.z();
}

template <typename Derived>
Eigen::Matrix<Derived,3,1> DSSStateDescriptor::clipVector(const int& rowToClip, const int& columnToClip) const
{
	Eigen::Vector3d _temp;
	
	_temp.coeffRef(0, 0) = storage(rowToClip + 0, columnToClip);
	_temp.coeffRef(1, 0) = storage(rowToClip + 1, columnToClip);
	_temp.coeffRef(2, 0) = storage(rowToClip + 2, columnToClip);
	return _temp;
}


template <typename Derived>
Eigen::Matrix<Derived, 4, 1> DSSStateDescriptor::clipQuatVector(const int& rowToClip, const int& columnToClip) const
{
	Eigen::Matrix<Derived, 4, 1> _temp;
	_temp.coeffRef(0, 0) = storage(rowToClip + 0, columnToClip);
	_temp.coeffRef(1, 0) = storage(rowToClip + 1, columnToClip);
	_temp.coeffRef(2, 0) = storage(rowToClip + 2, columnToClip);
	_temp.coeffRef(3, 0) = storage(rowToClip + 3, columnToClip);

	return _temp;
}

template <typename Derived = double>
Eigen::Quaternion<Derived> DSSStateDescriptor::clipQuaternion(const int& rowToClip, const int& columnToClip) const
{
	Eigen::Quaternion<Derived> _temp;
	_temp.w() = storage(rowToClip + 0, columnToClip);
	_temp.x() = storage(rowToClip + 1, columnToClip);
	_temp.y() = storage(rowToClip + 2, columnToClip);
	_temp.z() = storage(rowToClip + 3, columnToClip);
	return _temp;

}

class DSSStateDescriptorDelta : public DSSStateDescriptor
{
public: 

	DSSStateDescriptorDelta operator*(double c);
	DSSStateDescriptorDelta& operator=(DSSStateDescriptor other);
	DSSStateDescriptorDelta operator+(DSSStateDescriptorDelta& state1);
	DSSStateDescriptorDelta operator-(DSSStateDescriptorDelta& state1);

	DSSStateDescriptorDelta operator/(const double c);

	DSSStateDescriptorDelta();
	DSSStateDescriptorDelta(DSSIntegrable* integrable);

	~DSSStateDescriptorDelta();
};
