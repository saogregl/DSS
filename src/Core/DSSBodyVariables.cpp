#include "DSSBodyVariables.h"


double DSSBodyVariables::getBodyMass() const {
    return bodyMass;
}

void DSSBodyVariables::setBodyMass(const double mass) {
    bodyMass = mass;

    if (mass != 0) {
        bodyInvMass = 1.0 / mass;
    }
}

void DSSBodyVariables::disable() {
    disabled = true;
}

void DSSBodyVariables::BuildMassMatrix(Eigen::SparseMatrix<double> &storage, const int &row, const int &col) const {
    storage.coeffRef(row + 0, col + 0) = bodyMass;
    storage.coeffRef(row + 1, col + 1) = bodyMass;
    storage.coeffRef(row + 2, col + 2) = bodyMass;
    for (int rowNum = 0; rowNum < 3; rowNum++) {
        for (int colNum = 0; colNum < 3; colNum++) {
            storage.coeffRef(row + 3 + rowNum, col + 3 + colNum) = bodyInertia(rowNum, colNum);
        }
    }
}

int DSSBodyVariables::getDof() const {
    return ndof;
}

void DSSBodyVariables::setOffset(int off) {
    bodyOffset = off;
}

int DSSBodyVariables::getOffset() const {
    return bodyOffset;
}

DSSBodyVariables::DSSBodyVariables() : bodyMass(1),
                                       ndof(6),
                                       bodyOffset(0),
                                       bodyInvMass(1),
                                       disabled(false) {
    bodyInertia.setIdentity(3, 3);
    bodyInvInertia.setIdentity(3, 3);

}


DSSBodyVariables::~DSSBodyVariables()
= default;

bool DSSBodyVariables::isEnabled() const {
    return !disabled;
}

Eigen::Matrix3d DSSBodyVariables::getInertia() const {
    return bodyInertia;
}

Eigen::Matrix3d &DSSBodyVariables::getInertia() {
    return bodyInertia;
}

Eigen::Matrix<double, 6, 1> &DSSBodyVariables::getVariable() {
    return generalizedVariable;
}

Eigen::Matrix<double, 6, 1> &DSSBodyVariables::getForces() {
    return generalizedForces;
}

bool DSSBodyVariables::isDisabled() const {
    return disabled;
}
//TODO: Holy shit replace this by more generic functions
//Inverse of the matrix times a vector (usually the velocity)

void DSSBodyVariables::invMassMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                                    const Eigen::Matrix<double, 6, 1> &Vector) const {

    matrixToReturnAux(0) = bodyInvMass * Vector(0);

    matrixToReturnAux(1) = bodyInvMass * Vector(1);

    matrixToReturnAux(2) = bodyInvMass * Vector(2);

    matrixToReturnAux(3) = bodyInvInertia(0, 0) * Vector(3)
                           + bodyInvInertia(0, 1) * Vector(4)
                           + bodyInvInertia(0, 2) * Vector(5);

    matrixToReturnAux(4) = bodyInvInertia(1, 0) * Vector(3)
                           + bodyInvInertia(1, 1) * Vector(4)
                           + bodyInvInertia(1, 2) * Vector(5);

    matrixToReturnAux(5) = bodyInvInertia(2, 0) * Vector(3)
                           + bodyInvInertia(2, 1) * Vector(4)
                           + bodyInvInertia(2, 2) * Vector(5);
}


void DSSBodyVariables::incrInvMassMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                                        const Eigen::Matrix<double, 6, 1> &Vector) const {

    matrixToReturnAux(0) += bodyInvMass * Vector(0);

    matrixToReturnAux(1) += bodyInvMass * Vector(1);

    matrixToReturnAux(2) += bodyInvMass * Vector(2);

    matrixToReturnAux(3) += bodyInvInertia(0, 0) * Vector(3)
                            + bodyInvInertia(0, 1) * Vector(4)
                            + bodyInvInertia(0, 2) * Vector(5);

    matrixToReturnAux(4) += bodyInvInertia(1, 0) * Vector(3)
                            + bodyInvInertia(1, 1) * Vector(4)
                            + bodyInvInertia(1, 2) * Vector(5);

    matrixToReturnAux(5) += bodyInvInertia(2, 0) * Vector(3)
                            + bodyInvInertia(2, 1) * Vector(4)
                            + bodyInvInertia(2, 2) * Vector(5);

}

void DSSBodyVariables::massMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                                 const Eigen::Matrix<double, 6, 1> &Vector) const {
    matrixToReturnAux(0) = bodyMass * Vector(0);

    matrixToReturnAux(1) = bodyMass * Vector(1);

    matrixToReturnAux(2) = bodyMass * Vector(2);

    matrixToReturnAux(3) = bodyInertia(0, 0) * Vector(3)
                           + bodyInertia(0, 1) * Vector(4)
                           + bodyInertia(0, 2) * Vector(5);

    matrixToReturnAux(4) = bodyInertia(1, 0) * Vector(3)
                           + bodyInertia(1, 1) * Vector(4)
                           + bodyInertia(1, 2) * Vector(5);

    matrixToReturnAux(5) = bodyInertia(2, 0) * Vector(3)
                           + bodyInertia(2, 1) * Vector(4)
                           + bodyInertia(2, 2) * Vector(5);

}

void DSSBodyVariables::incrMassMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                                     const Eigen::Matrix<double, 6, 1> &Vector) const {

    matrixToReturnAux(0) += bodyMass * Vector(0);

    matrixToReturnAux(1) += bodyMass * Vector(1);

    matrixToReturnAux(2) += bodyMass * Vector(2);

    matrixToReturnAux(3) += bodyInertia(0, 0) * Vector(3)
                           + bodyInertia(0, 1) * Vector(4)
                           + bodyInertia(0, 2) * Vector(5);

    matrixToReturnAux(4) += bodyInertia(1, 0) * Vector(3)
                           + bodyInertia(1, 1) * Vector(4)
                           + bodyInertia(1, 2) * Vector(5);

    matrixToReturnAux(5) += bodyInertia(2, 0) * Vector(3)
                           + bodyInertia(2, 1) * Vector(4)
                           + bodyInertia(2, 2) * Vector(5);

}
