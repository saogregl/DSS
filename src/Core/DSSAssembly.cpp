#include "DSSAssembly.h"


DSSAssembly::DSSAssembly() : offsetp(0),
							 offsetw(0),
							 offsetl(0),
							 nbodies(0),
							 nDOFS(0),
							 nFixedBodies(0),
							 nGeneralizedCoord(0),
							 nGeneralizedCoord_w(0),
							 njoints(0),
							 nScalarConstraints(0)
{
}


DSSAssembly::~DSSAssembly()
{
}

void DSSAssembly::prepareSolverInterface(DSSSolverInterface& solver_interface)
{
    //resets vectors of variables and mobilizers first
	solver_interface.resetVectors();

	for (auto& it : rigidBodyList)
	{
		it->injectVariables(solver_interface);
	}
	for (auto& it : linkList)
	{
		it->injectMobilizers(solver_interface);
	}
}

void DSSAssembly::addBody(std::shared_ptr<DSSRigidBody> _body)
{
	_body->setWorld(this->getWorld());
	rigidBodyList.push_back(_body);
}

void DSSAssembly::addLink(std::shared_ptr<DSSConstraintBase> _link)
{
	linkList.push_back(_link);
}


void DSSAssembly::setup()
{
	nbodies = 0;
	njoints = 0;
	nGeneralizedCoord = 0;
	nGeneralizedCoord_w = 0;
	nScalarConstraints = 0;
	nDOFS = 0;
	nFixedBodies = 0;
	for (auto it: rigidBodyList)
	{
		auto tempBody = it;
		if (it->isFixed())
		{
			nFixedBodies++;
		}
		else
		{
			nbodies++;
			{
				tempBody->setOffp(offsetp + nGeneralizedCoord);
				tempBody->setOffw(offsetw + nGeneralizedCoord_w);
				tempBody->setOffl(offsetl + nScalarConstraints);
				tempBody->getBodyVariable().setOffset(offsetw + nGeneralizedCoord_w);
				nGeneralizedCoord += it->getndof();
				nGeneralizedCoord_w += it->getndof_w();
			}
		}
	}

	for (auto it : linkList)
	{
		auto tempLink = it;
		njoints++;

		tempLink->setOffp(0);
		tempLink->setOffw(0);
		tempLink->setOffl(offsetl + nScalarConstraints);

		nScalarConstraints += tempLink->getDOFConstrained();
	}

	nDOFS = nGeneralizedCoord_w - nScalarConstraints;
}

void DSSAssembly::retrieveBodyState(const int& offp,
                                    DSSStateDescriptor& pos,
                                    const int& offv,
                                    DSSStateDescriptorDelta& vel,
                                    double time)
{
	for (const auto &it : rigidBodyList)
	{
		if(!it->isFixed())
		{
			it->retrieveBodyState(it->getOffp(), pos, it->getOffw(), vel, time);
		}
	}
}

void DSSAssembly::disperseBodyState(const int& offp,
                                    const DSSStateDescriptor& pos,
                                    const int& offv,
                                    const DSSStateDescriptorDelta& vel,
                                    double time)
{
	for (auto &it : rigidBodyList)
	{
		if (!it->isFixed())
		{
			it->disperseBodyState(it->getOffp(), pos, it->getOffw(), vel, time);
		}
	}

}

void DSSAssembly::retrieveBodyAcceleration(const int& offa, DSSStateDescriptorDelta& acc)
{
	for(const auto &it : rigidBodyList)
	{
		if (!it->isFixed())
		{
		it->retrieveBodyAcceleration(it->getOffw(), acc);
		}
	}
}

void DSSAssembly::disperseBodyAcceleration(const int& offa, const DSSStateDescriptorDelta& acc)
{
	for (auto it : rigidBodyList)
	{
		auto tempBody = it;
		if (!tempBody->isFixed())
		{
			tempBody->disperseBodyAcceleration(it->getOffw(), acc);
		}
	}

}

void DSSAssembly::scatterBodyLagrangian(const int offl, Eigen::VectorXd &L)
{

	for (auto it : rigidBodyList)
	{
		auto tempBody = it;
		if (!tempBody->isFixed())
		{
			tempBody->scatterBodyLagrangian(it->getOffl(), L);
		}
	}


	for (auto it : linkList)
	{
        it->scatterBodyLagrangian(it->getOffl(), L);
	}

}

void DSSAssembly::convertBodyAngularVel(const DSSStateDescriptorDelta & VAng, DSSStateDescriptorDelta & Vpdot)
{
	for(auto ib : rigidBodyList)
	{
		//pdot = 0.5*G^T*w'
		Eigen::Matrix<double, 3,4> gmatrix;
		DSSTransform::buildQuatGMatrix(ib->getOrientation(), gmatrix);
		Vpdot.clipVector<double>(ib->getOffp() + 0, 0) = VAng.clipVector<double>(ib->getOffw() + 0, 0); //linear part
		Vpdot.clipQuatVector<double>(ib->getOffp() + 3, 0) = 0.5*gmatrix.transpose()*VAng.clipVector<double>(ib->getOffw()+3, 0); //Angular velocity to quaternion
	}
}

void DSSAssembly::bodyStateIncrement(const int& offp,
                                     DSSStateDescriptor& pos_new,
                                     const DSSStateDescriptor& pos_old,
                                     const int& offv,
                                     const DSSStateDescriptorDelta& vel)
{
	for (const auto &it : rigidBodyList)
	{
		if (!(it->isFixed()))
		{
			it->bodyStateIncrement(it->getOffp(), pos_new, pos_old, it->getOffw(), vel);
		}
	}
}

void DSSAssembly::update(double time)
{
	for (auto it : rigidBodyList)
	{
		it->update(time);
	}
	for (auto it : linkList)
	{
		it->update(time);
	}

}

void DSSAssembly::bodyLoadForceVector(Eigen::VectorXd& Vec, unsigned int off, double c)
{
	for (const auto &it : rigidBodyList)
	{
		if (!it->isFixed())
		{
			it->bodyLoadForceVector(Vec, it->getOffw(), c);
			//std::cout << "vec is" << Vec;
		}
	}

}


void DSSAssembly::bodyLoadMvVector(Eigen::VectorXd& Vec, unsigned off, const Eigen::VectorXd& velocity, double c)
{
	for (const auto &it : rigidBodyList)
	{
		if (!it->isFixed())
		{
			it->bodyLoadMvVector(Vec,it->getOffw(),velocity,c);

		}
	}

}

void DSSAssembly::bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned off, const Eigen::VectorXd &L, const double c)
{
	for (auto it : linkList)
	{
		it->bodyLoadCqLVector(Vec,it->getOffl(),L,c);

	}

}

void DSSAssembly::bodyLoadConstraintResiduals(Eigen::VectorXd& Vec, unsigned off, double c)
{
	for (const auto &it : linkList)
	{
		it->bodyLoadConstraintResiduals(Vec, it->getOffl(), c);
	}

}

void DSSAssembly::bodyLoadConstrTimeDerivatives(Eigen::VectorXd& Vec, unsigned off, double c)
{
	for (auto it : linkList)
	{
		it->bodyLoadConstrTimeDerivatives(Vec, it->getOffl(), c);
	}

}

void DSSAssembly::objPrepareSolverDescriptor(const int offv,
											 const DSSStateDescriptorDelta &vel,
                                             const Eigen::VectorXd &Residuals,
											 const int offL,
											 const Eigen::VectorXd &L,
                                             const Eigen::VectorXd Qc) {

	for (const auto &ib : rigidBodyList)
	{
		if(ib->isEnabled())
		{
			ib->objPrepareSolverDescriptor(ib->getOffw(),vel,Residuals,ib->getOffl(),L,Qc);
		}
	}
	for (auto it : linkList)
	{
		it->objPrepareSolverDescriptor(it->getOffw(),vel,Residuals,it->getOffl(),L,Qc);
	}


}

void DSSAssembly::objUpdateFromSolverDescriptor(const int offv,
												DSSStateDescriptorDelta &vel,
												const int offL,
												Eigen::VectorXd &L) {

	for (auto ib : rigidBodyList)
	{
		if(ib->isEnabled())
		{
			ib->objUpdateFromSolverDescriptor(ib->getOffw(),vel,ib->getOffl(),L);
		}
	}
	for (auto it : linkList)
	{
		it->objUpdateFromSolverDescriptor(it->getOffw(),vel,it->getOffl(),L);
	}

}
