#pragma once
#include "DSSSpatialDescriptor.h"
#include "../debugFiles.h"

class DSSAuxiliaryFrame : public DSSSpatialDescriptor
{

protected: 
	Eigen::Matrix3d rotationMatrix;

public:

	DSSAuxiliaryFrame(const Eigen::Vector3d& _position,
					  const Eigen::Quaterniond& _orientation);

	DSSAuxiliaryFrame();





	virtual ~DSSAuxiliaryFrame() override;


	Eigen::Matrix3d getRotationMatrix() const;
	void setRotationMatrix(const Eigen::Matrix3d& _rotationMatrix);



	Eigen::Vector3d getxAxis() const;
	Eigen::Vector3d getyAxis() const;
	Eigen::Vector3d getzAxis() const;


};

