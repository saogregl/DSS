#include <iostream>
#include "DSSSpatialDescriptor.h"

DSSSpatialDescriptor::DSSSpatialDescriptor(const Eigen::Vector3d& _position,
										   const Eigen::Quaterniond& _orientation) : position(_position), orientation(_orientation)
{

}

DSSSpatialDescriptor::DSSSpatialDescriptor() : position(0,0,0), orientation(0,0,0,1)
{
}

DSSSpatialDescriptor::~DSSSpatialDescriptor()
= default;

void DSSSpatialDescriptor::setCoordinateSystem(const Eigen::Vector3d& _position,
                                               const Eigen::Quaterniond& _orientation)
{
	setPosition(_position);
	setOrientation(_orientation);
}
/*

Eigen::Vector3d DSSSpatialDescriptor::getPosition() const
{
	return position;
}



Eigen::Quaterniond DSSSpatialDescriptor::getOrientation() const
{
	return orientation;
}
*/

void DSSSpatialDescriptor::setPosition(const Eigen::Vector3d& pos)
{
	position = pos;
}

void DSSSpatialDescriptor::setOrientation(const Eigen::Quaterniond& ori)
{
	orientation = ori;
	
}

void DSSSpatialDescriptor::transformChildToParent(const DSSSpatialDescriptor& frameToTransformLocal,
                                                  DSSSpatialDescriptor& resultingFrameParent)
{
	Eigen::Vector3d position =
		DSSTransform::transformChildToParent(frameToTransformLocal.getPosition(),
		                                     this->getPosition(),
		                                     this->getOrientation());
	resultingFrameParent.setCoordinateSystem(position,
	                                         (*this).getOrientation() * frameToTransformLocal.getOrientation());
}



void DSSSpatialDescriptor::transformParentToChild(const DSSSpatialDescriptor & frameToTransformParent, DSSSpatialDescriptor & resultingFrameLocal)
{
}

Eigen::Matrix3d DSSSpatialDescriptor::buildRotationMatrixFromQuat() const
{
	return orientation.toRotationMatrix();
}

Eigen::Matrix3d DSSSpatialDescriptor::getSkewSymmetricPos() const
{
	Eigen::Matrix3d res;

	auto x = this->position.x();
	auto y = this->position.y();
	auto z = this->position.z();

	res.coeffRef(0, 0) = 0;
	res.coeffRef(0, 1) = -z;
	res.coeffRef(0, 2) = y;
	res.coeffRef(1, 0) = z;
	res.coeffRef(1, 1) = 0;
	res.coeffRef(1, 2) = -x;
	res.coeffRef(2, 0) = -y;
	res.coeffRef(2, 1) = x;
	res.coeffRef(2, 2) = 0;

	return res;
}

const Eigen::Vector3d& DSSSpatialDescriptor::getPosition() const {
	return position;
}

const Eigen::Quaterniond& DSSSpatialDescriptor::getOrientation() const {
	return orientation;

}

