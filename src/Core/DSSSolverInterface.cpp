#include "DSSSolverInterface.h"


void DSSSolverInterface::insertVariable(DSSBodyVariables* _variable)
{
	variableList.push_back(_variable);
}

void DSSSolverInterface::insertMobilizer(DSSMobilityControl* _mobcontrol)
{
	mobilityControlList.push_back(_mobcontrol);
}

std::vector<DSSBodyVariables*>& DSSSolverInterface::getVariables()
{
	return variableList;
}

std::vector<DSSMobilityControl*>& DSSSolverInterface::getMobilizers()
{
	return mobilityControlList;
}

int DSSSolverInterface::countTotalDofs()
{
	numberOfVariables = 0;
	for (auto&& it : variableList)
	{
		if (it->isEnabled() == 1)
		{
			it->setOffset(numberOfVariables);
			numberOfVariables = numberOfVariables + it->getDof();
		}
	}
	return numberOfVariables;
}

int DSSSolverInterface::countNumberOfMobilizers()
{
	numberOfMobilizers = 0;
	for (auto&& it : mobilityControlList)
	{
		it->setOffset(numberOfMobilizers);
		numberOfMobilizers++;
	}
	return numberOfMobilizers;
}

void DSSSolverInterface::updateCounters()
{
	countNumberOfMobilizers();
	countTotalDofs();
}

void DSSSolverInterface::structureProblem(Eigen::SparseMatrix<double>* jacobianMat,
                                          Eigen::SparseMatrix<double> *massMat,
                                          Eigen::Matrix<double, Eigen::Dynamic, 1>* generalizedForceVec,
                                          Eigen::Matrix<double, Eigen::Dynamic, 1>* residualVec)
{
	int nmob = countNumberOfMobilizers();
	int nvar = countTotalDofs();

	if (jacobianMat)
	{
		jacobianMat->resize(nmob, nvar);
	}

	if (massMat)
	{
		massMat->resize(nvar, nvar);
	}
	if (residualVec)
	{
		residualVec->resize(nmob, 1);
	}
	if (generalizedForceVec)
	{
		generalizedForceVec->resize(nmob, 1);
	}

	int variableNum = 0;
	for (auto iv : variableList)
	{
		if (iv->isEnabled() == 1)
		{
			iv->BuildMassMatrix(*massMat, variableNum, variableNum);
			variableNum += iv->getDof();
		}
	}

	int constraintNum = 0;
	for (auto ic : mobilityControlList)
	{
		residualVec->coeffRef(constraintNum, 0) = ic->getResidual();
		ic->BuildJacobian(*jacobianMat, constraintNum);
		constraintNum++;
	}



}

DSSSolverInterface::DSSSolverInterface() : numberOfVariables(0),
										   numberOfMobilizers(0)
{
	variableList.clear();
	mobilityControlList.clear();
}


DSSSolverInterface::~DSSSolverInterface()
{
}

void DSSSolverInterface::resetVectors()
{
	variableList.clear();
	mobilityControlList.clear();
}
