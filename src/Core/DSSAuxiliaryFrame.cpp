#include "DSSAuxiliaryFrame.h"



Eigen::Matrix3d DSSAuxiliaryFrame::getRotationMatrix() const
{
	return rotationMatrix;
}

void DSSAuxiliaryFrame::setRotationMatrix(const Eigen::Matrix3d& _rotationMatrix)
{
	rotationMatrix = _rotationMatrix;
}

Eigen::Vector3d DSSAuxiliaryFrame::getxAxis() const
{
	return (rotationMatrix.block<3,1>(0,0));
}

Eigen::Vector3d DSSAuxiliaryFrame::getyAxis() const
{
	return (rotationMatrix.block<3, 1>(0, 1));
}

Eigen::Vector3d DSSAuxiliaryFrame::getzAxis() const
{
	return (rotationMatrix.block<3, 1>(0, 2));
}

DSSAuxiliaryFrame::DSSAuxiliaryFrame(const Eigen::Vector3d& _position,
                                     const Eigen::Quaterniond& _orientation) : DSSSpatialDescriptor(_position,_orientation)
{
	rotationMatrix = _orientation.toRotationMatrix();
}

DSSAuxiliaryFrame::DSSAuxiliaryFrame()
{
}



DSSAuxiliaryFrame::~DSSAuxiliaryFrame()
{
}
