#pragma once

#include <eigen3/Eigen/Dense>
#include "../debugFiles.h"
#include <eigen3/Eigen/SparseCore>


class DSSBodyVariables {
private:
    //Should use sparse matrix instead.
    //...nah.
    Eigen::Matrix3d bodyInertia;
    Eigen::Matrix3d bodyInvInertia;

    double bodyMass;
    double bodyInvMass;

    int bodyOffset;
    int ndof;

    bool disabled;


    //Important variables for the solver

    Eigen::Matrix<double, 6, 1> generalizedVariable;
    Eigen::Matrix<double, 6, 1> generalizedForces;


public:

    int getDof() const;

    int getOffset() const;

    void disable();

    void setOffset(int off);

    bool isEnabled() const;
    bool isDisabled() const;

    double getBodyMass() const;

    Eigen::Matrix3d getInertia() const;

    Eigen::Matrix3d &getInertia();

    Eigen::Matrix3d getInvInertia() const { return bodyInvInertia; };

    Eigen::Matrix3d &getInvInertia() { return bodyInvInertia; };


    Eigen::Matrix<double, 6, 1>& getVariable();
    Eigen::Matrix<double, 6, 1>& getForces();


    void setBodyMass(double mass);

    void BuildMassMatrix(Eigen::SparseMatrix<double> &storage,
                         const int &row, const int &col) const;

    void invMassMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                      const Eigen::Matrix<double, 6, 1> &Vector) const;

    void incrInvMassMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                      const Eigen::Matrix<double, 6, 1> &Vector) const;

    void massMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                   const Eigen::Matrix<double, 6, 1> &Vector) const;

    void incrMassMultV(Eigen::Matrix<double, 6, 1> &matrixToReturnAux,
                       const Eigen::Matrix<double, 6, 1> &Vector) const;


    DSSBodyVariables();

    ~DSSBodyVariables();
};

	