#include "DSSTransform.h"


DSSTransform::DSSTransform()
= default;


DSSTransform::~DSSTransform()
= default;

//Check this later, better not define unneeded variables
Eigen::Vector3d DSSTransform::transformChildToParent(
		const Eigen::Vector3d &childVector,
		const Eigen::Vector3d &childOriginInParent,
		const Eigen::Matrix3d &relativeRotationMatrix)
{
	Eigen::Vector3d transformedPoint;
	transformedPoint(0) = (relativeRotationMatrix(0, 0)*childVector(0) + relativeRotationMatrix(0, 1)*childVector(1) +
		relativeRotationMatrix(0, 2)*childVector(2)) + childOriginInParent(0);
	transformedPoint(1) = (relativeRotationMatrix(1, 0)*childVector(0) + relativeRotationMatrix(1, 1)*childVector(1) +
		relativeRotationMatrix(1, 2)*childVector(2)) + childOriginInParent(1);
	transformedPoint(2) = (relativeRotationMatrix(2, 0)*childVector(0) + relativeRotationMatrix(2, 1)*childVector(1) +
		relativeRotationMatrix(2, 2)*childVector(2)) + childOriginInParent(2);

	return transformedPoint;
}

Eigen::Vector3d DSSTransform::transformChildToParent(const Eigen::Vector3d &childVector,
                                                     const Eigen::Vector3d &childOriginInParent,
                                                     const Eigen::Quaterniond &relativeQuaternion)
{
	Eigen::Vector3d transformedPoint;
	Eigen::Matrix3d relativeRotationMatrix = relativeQuaternion.toRotationMatrix();

	transformedPoint(0) = (relativeRotationMatrix(0, 0)*childVector(0) + relativeRotationMatrix(0, 1)*childVector(1) +
		relativeRotationMatrix(0, 2)*childVector(2)) + childOriginInParent(0);
	transformedPoint(1) = (relativeRotationMatrix(1, 0)*childVector(0) + relativeRotationMatrix(1, 1)*childVector(1) +
		relativeRotationMatrix(1, 2)*childVector(2)) + childOriginInParent(1);
	transformedPoint(2) = (relativeRotationMatrix(2, 0)*childVector(0) + relativeRotationMatrix(2, 1)*childVector(1) +
		relativeRotationMatrix(2, 2)*childVector(2)) + childOriginInParent(2);

	return transformedPoint;
}

Eigen::Vector3d DSSTransform::transformParentToChild(const Eigen::Vector3d &parentVector,
                                                     const Eigen::Vector3d &ParentOriginInParent,
                                                     const Eigen::Matrix3d &relativeRotationMatrix)
{

	double relativex = parentVector(0) - ParentOriginInParent(0);
	double relativey = parentVector(1) - ParentOriginInParent(1);
	double relativez = parentVector(2) - ParentOriginInParent(2);


    return Eigen::Vector3d((relativeRotationMatrix(0, 0) * relativex + relativeRotationMatrix(1, 0) * relativey +
                            relativeRotationMatrix(2, 0) * relativez),
                           (relativeRotationMatrix(0, 1) * relativex + relativeRotationMatrix(1, 1) * relativey +
                            relativeRotationMatrix(2, 1) * relativez),
                           (relativeRotationMatrix(0, 2) * relativex + relativeRotationMatrix(1, 2) * relativey +
                            relativeRotationMatrix(2, 2) * relativez));
}


Eigen::Matrix3d DSSTransform::makeSkewsymmetric(const Eigen::Vector3d& vec)
{
	Eigen::Matrix3d res;
    //TODO Change this later. Also, check if it is possible to move the argument as return.
	double x = vec.x();
    double y = vec.y();
    double z = vec.z();

	res.coeffRef(0, 0) = 0;
	res.coeffRef(0, 1) = -z;
	res.coeffRef(0, 2) = y;
	res.coeffRef(1, 0) = z;
	res.coeffRef(1, 1) = 0;
	res.coeffRef(1, 2) = -x;
	res.coeffRef(2, 0) = -y;
	res.coeffRef(2, 1) = x;
	res.coeffRef(2, 2) = 0;

	return res;
}

void DSSTransform::buildQuatEMatrix(const Eigen::Quaterniond &quat,
                                    Eigen::Matrix<double, 3, 4> &Ematrix)
{
	const double e0 = quat.w();
	const double e1 = quat.x();
	const double e2 = quat.y();
	const double e3 = quat.z();

	Ematrix(0, 0) = -e1;
	Ematrix(0, 1) =  e0;
	Ematrix(0, 2) = -e3;
	Ematrix(0, 3) =  e2;

	Ematrix(1, 0) = -e2;
	Ematrix(1, 1) =  e3;
	Ematrix(1, 2) =  e0;
	Ematrix(1, 3) = -e1;

	Ematrix(2, 0) = -e3;
	Ematrix(2, 1) = -e2;
	Ematrix(2, 2) =  e1;
	Ematrix(2, 3) =  e0;


}

void DSSTransform::buildQuatGMatrix(const Eigen::Quaterniond &quat,
                                    Eigen::Matrix<double, 3, 4> &Gmatrix)
{
	const double e0 = quat.w();
	const double e1 = quat.x();
	const double e2 = quat.y();
	const double e3 = quat.z();

	Gmatrix(0, 0) = -e1;
	Gmatrix(0, 1) =  e0;
	Gmatrix(0, 2) =  e3;
	Gmatrix(0, 3) = -e2;

	Gmatrix(1, 0) = -e2;
	Gmatrix(1, 1) = -e3;
	Gmatrix(1, 2) =  e0;
	Gmatrix(1, 3) =  e1;

	Gmatrix(2, 0) = -e3;	
	Gmatrix(2, 1) =  e2;
	Gmatrix(2, 2) = -e1;
	Gmatrix(2, 3) =  e0;

}

Eigen::Quaterniond DSSTransform::quatCross(const Eigen::Quaterniond &p, const Eigen::Quaterniond &q)
{
	auto pq = Eigen::Quaterniond();
	pq.w() = p.w()*q.w() - p.x()*q.x() - p.y()*q.y() - p.z()*q.z();
	pq.x() = p.w()*q.x() + p.x()*q.w() + p.y()*q.z() - p.z()*q.y();
	pq.y() = p.w()*q.y() - p.x()*q.z() + p.y()*q.w() + p.z()*q.x();
	pq.z() = p.w()*q.z() - p.x()*q.y() + p.y()*q.x() + p.z()*q.w();
	return pq;
}


