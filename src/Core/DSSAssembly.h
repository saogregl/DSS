#pragma once

#include "../debugFiles.h"
#include <memory>
#include "../DSSRigidBody.h"
#include "../Constraints/DSSConstraintBase.h"
#include "DSSSolverInterface.h"

class DSSAssembly: public DSSPhysicsObject
{
protected: 
	DSSSolverInterface solverInterface;
	std::vector<std::shared_ptr<DSSRigidBody>> rigidBodyList;
    std::vector<std::shared_ptr<DSSConstraintBase>> linkList;

	//Just inn case the model needs to be divided in subassemblies, global offsets are stored. 
	int offsetp;
	int offsetw;
	int offsetl;

	int nbodies;
	int njoints;
	int nGeneralizedCoord;
	int nGeneralizedCoord_w;
	int nScalarConstraints;
	int nDOFS;
	int nFixedBodies;
	public:


	DSSAssembly();
	~DSSAssembly();

	virtual void prepareSolverInterface(DSSSolverInterface& solver_interface);

	void addBody(std::shared_ptr<DSSRigidBody> _body);

    void addLink(std::shared_ptr<DSSConstraintBase> _link);
	//void configureWorldReferences(DSSWorld _world);
	void setup();
	void update(double time) override;

	//Separate this in functions for pos and speed individually.
	void retrieveBodyState(const int& offp,
						   DSSStateDescriptor& pos,
						   const int& offv,
						   DSSStateDescriptorDelta& vel,
						   double time) override;

	void disperseBodyState(const int& offp,
						   const DSSStateDescriptor& pos,
						   const int& offv,
						   const DSSStateDescriptorDelta& vel,
						   double time) override;

	void retrieveBodyAcceleration(const int& offa,
								  DSSStateDescriptorDelta& acc) override;


	void disperseBodyAcceleration(const int& offa,
								  const DSSStateDescriptorDelta& acc) override;

	void scatterBodyLagrangian(const int offl, Eigen::VectorXd &L) override;


	void convertBodyAngularVel(const DSSStateDescriptorDelta& VAng,
							   DSSStateDescriptorDelta& Vpdot);

	void bodyStateIncrement(const int& offp,
							DSSStateDescriptor& pos_new,
							const DSSStateDescriptor& pos_old,
							const int& offv,
							const DSSStateDescriptorDelta& vel) override;

	void bodyLoadForceVector(Eigen::VectorXd& Vec, unsigned int off, double c) override;
	void bodyLoadMvVector(Eigen::VectorXd& Vec, unsigned int off, const Eigen::VectorXd& velocity, double c) override;
	void bodyLoadCqLVector(Eigen::VectorXd &Vec, unsigned int off, const Eigen::VectorXd &L, const double c) override;
	void bodyLoadConstraintResiduals(Eigen::VectorXd& Vec, unsigned int off, double c) override;
	void bodyLoadConstrTimeDerivatives(Eigen::VectorXd& Vec, unsigned int off, double c) override;


	void objPrepareSolverDescriptor(const int offv,
									const DSSStateDescriptorDelta &vel,
									const Eigen::VectorXd &Residuals,
									const int offL,
									const Eigen::VectorXd &L,
									const Eigen::VectorXd Qc) override;

	void objUpdateFromSolverDescriptor(const int offv,
									   DSSStateDescriptorDelta &vel,
									   const int offL,
									   Eigen::VectorXd &L) override;


	//void countConstraints();
	

};

