#pragma once
#include <eigen3/Eigen/Dense>
#include "DSSTransform.h"
#include "../debugFiles.h"


class DSSSpatialDescriptor
{
public:

	Eigen::Vector3d position;
	Eigen::Quaterniond orientation;


	DSSSpatialDescriptor(const Eigen::Vector3d& _position, //Position vector
	                     const Eigen::Quaterniond& _orientation); //quaternion for orientation

	DSSSpatialDescriptor();

	virtual ~DSSSpatialDescriptor();

	void setCoordinateSystem(const Eigen::Vector3d& _position,
	                         const Eigen::Quaterniond& _orientation);

	//Set/get Functions
	Eigen::Vector3d& getPosition() {return position; };
	Eigen::Quaterniond& getOrientation() { return orientation; };
	const Eigen::Vector3d& getPosition() const;
	const Eigen::Quaterniond& getOrientation() const;


	virtual void setPosition(const Eigen::Vector3d& pos);
	virtual void setOrientation(const Eigen::Quaterniond& ori);


	//TRANSFORM FUNCTIONS. Can be overloaded by derived objects.
	virtual void transformChildToParent(const DSSSpatialDescriptor& frameToTransformLocal,
	                                    DSSSpatialDescriptor& resultingFrameParent);

	virtual void transformParentToChild(const DSSSpatialDescriptor& frameToTransformParent,
	                                    DSSSpatialDescriptor& resultingFrameLocal);

	//Auxiliary
	Eigen::Matrix3d buildRotationMatrixFromQuat() const;
	
	Eigen::Matrix3d getSkewSymmetricPos() const;

private:

};

