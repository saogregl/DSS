#pragma once
#include <eigen3/Eigen/Dense>
#include "../debugFiles.h"

//Used to store and manipulate the position and rotation of the object.


class DSSTransform
{
public:
	
	DSSTransform();
	~DSSTransform();

	static Eigen::Vector3d transformChildToParent(const Eigen::Vector3d &childVector,
												  const Eigen::Vector3d &childOriginInParent,
												  const Eigen::Matrix3d &relativeRotationMatrix);

	static Eigen::Vector3d transformChildToParent(const Eigen::Vector3d &childVector,
                                                  const Eigen::Vector3d &childOriginInParent,
                                                  const Eigen::Quaterniond &relativeQuaternion);


	static Eigen::Vector3d transformParentToChild(const Eigen::Vector3d &parentVector,
                                                  const Eigen::Vector3d &ParentOriginInParent,
                                                  const Eigen::Matrix3d &relativeRotationMatrix);

	static Eigen::Matrix3d makeSkewsymmetric(const Eigen::Vector3d& vec);

	// 9.3.18 haug
	static void buildQuatEMatrix(const Eigen::Quaterniond &quat,
                                 Eigen::Matrix<double, 3, 4> &Ematrix);

	//check 9.3.19 haug
	static void buildQuatGMatrix(const Eigen::Quaterniond &quat,
                                 Eigen::Matrix<double, 3, 4> &Gmatrix);

	static Eigen::Quaterniond quatCross(const Eigen::Quaterniond &quatl,
                                        const Eigen::Quaterniond &quatr);

};


