#include "DSSStateDescriptor.h"

void DSSStateDescriptor::reset(const int& rows, const int& cols)
{
	storage.resize(rows);
}


DSSStateDescriptor DSSStateDescriptor::operator+(DSSStateDescriptor& state1)
{

	auto result = DSSStateDescriptor(this->integrable);
	result.getStorage() = this->getStorage() + state1.getStorage();
	return result;
}

DSSStateDescriptor DSSStateDescriptor::operator-(DSSStateDescriptor& state1)
{
	auto result = DSSStateDescriptor(this->integrable);
	result.getStorage() = this->getStorage() - state1.getStorage();
	return result;
}

DSSStateDescriptor DSSStateDescriptor::operator*(const double c)
{
	auto result = DSSStateDescriptor(this->integrable);
	result.getStorage() = this->getStorage() * c;
	return result;
}

DSSStateDescriptor DSSStateDescriptor::operator/(const double c)
{
	auto result = DSSStateDescriptor(this->integrable);
	result.getStorage() = this->getStorage() * 1/c;
	return result;
}

void DSSStateDescriptor::setIntegrable(DSSIntegrable* _integrable)
{
	integrable = _integrable;
}

DSSStateDescriptor::DSSStateDescriptor() : integrable(nullptr)
{
}

DSSStateDescriptor::DSSStateDescriptor(DSSIntegrable* _integrable) : integrable(_integrable)
{

}


DSSStateDescriptor::DSSStateDescriptor(const DSSStateDescriptor& obj) : storage(obj.storage)
{
	integrable = obj.integrable;
}

DSSStateDescriptor::~DSSStateDescriptor()
= default;

DSSStateDescriptorDelta DSSStateDescriptorDelta::operator*(double c)
{
	auto result = DSSStateDescriptorDelta(this->integrable);
	result.getStorage() = this->getStorage() * c;
	return result;
}

DSSStateDescriptorDelta& DSSStateDescriptorDelta::operator=(DSSStateDescriptor other)
{
	this->getStorage() = other.getStorage();
	return *this;
}

DSSStateDescriptorDelta DSSStateDescriptorDelta::operator+(DSSStateDescriptorDelta& state1)
{
	auto result = DSSStateDescriptorDelta(this->integrable);
	result.getStorage() = this->getStorage() + state1.getStorage();
	return result;

}

DSSStateDescriptorDelta DSSStateDescriptorDelta::operator-(DSSStateDescriptorDelta& state1)
{
	auto result = DSSStateDescriptorDelta(this->integrable);
	result.reset(this->storage.rows(), this->storage.rows());
	result.storage = this->storage - state1.storage;
	return result;

}

DSSStateDescriptorDelta DSSStateDescriptorDelta::operator/(const double c)
{
	auto result = DSSStateDescriptorDelta(this->integrable);
	result.getStorage() = this->getStorage() * 1/c;
	return result;

}

DSSStateDescriptorDelta::DSSStateDescriptorDelta()
= default;

DSSStateDescriptorDelta::DSSStateDescriptorDelta(DSSIntegrable* integrable) : DSSStateDescriptor(integrable)
{

}

DSSStateDescriptorDelta::~DSSStateDescriptorDelta()
= default;

const double& DSSStateDescriptor::operator()(const int index) const
{
	return storage.coeffRef(index);
}

double& DSSStateDescriptor::operator()(const int index)
{
	return storage.coeffRef(index);
}


