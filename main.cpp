#include <iostream>
#include <fstream>
#include "src/DSSWorld.h"
#include "src/Constraints/DSSConstraintRevolute.h"
#include "src/Constraints/DSSConstraintMotor.h"
#include <chrono>

class DSSWorld;

int main()
{




	auto start = std::chrono::high_resolution_clock::now();

    auto pendulum = std::make_shared<DSSRigidBody>();
    auto ground = std::make_shared<DSSRigidBody>();


    Eigen::Matrix3d inertia1;
    inertia1 << 0.005,  0.0,  0.0,
                0.0,    0.1,  0.0,
                0.0,    0.0,  0.1;


    Eigen::Matrix3d inertia2;
    inertia2.setIdentity();

    auto norotationAxis = Eigen::AngleAxisd(0,Eigen::Vector3d(0,1,0));
    auto noRotQuat = Eigen::Quaterniond(norotationAxis);


    auto angleAxis = Eigen::AngleAxisd(M_PI/2,Eigen::Vector3d(0,1,0));
    auto quateste = Eigen::Quaterniond(angleAxis);
    auto angleAxis2 = Eigen::AngleAxisd(M_PI / 2, Eigen::Vector3d(0, 1, 0));
    auto quateste2 = Eigen::Quaterniond(angleAxis2);
    auto angleAxis3 = Eigen::AngleAxisd(M_PI / 2, Eigen::Vector3d(1, 0, 0));
    auto quateste3 = Eigen::Quaterniond(angleAxis3);

    DSSWorld DynamicWorld;

    DynamicWorld.addBody(pendulum);
    DynamicWorld.addBody(ground);
    pendulum.get()->setBodyMass(1);
    pendulum.get()->setBodyInertia(inertia1);
    ground.get()->setBodyMass(11);
    ground.get()->setFixed();
    ground.get()->setBodyInertia(inertia2);

    pendulum.get()->setPosition(Eigen::Vector3d(0, 1, 0));
    ground.get()->setPosition(Eigen::Vector3d(0,0,0));
    ground.get()->setOrientation(noRotQuat);
    pendulum.get()->setOrientation(quateste3);
    auto joint1 = std::make_shared<DSSConstraintRevolute>();
    auto frame2 = DSSAuxiliaryFrame(Eigen::Vector3d(0, 0, 1), quateste);
    auto frame1 = DSSAuxiliaryFrame(Eigen::Vector3d(0, 0, 0), quateste2);




    DynamicWorld.addLink(joint1);
    joint1.get()->initialize(ground,
                             pendulum,
                             frame1,
                             frame2);

    std::ofstream myfile;
    myfile.open ("example.csv");
    DynamicWorld.setup();

    while(DynamicWorld.getCurrentTime() < DynamicWorld.getEndTime())
    {
        std::cout << "The Current TIme is: " << DynamicWorld.getCurrentTime() << std::endl;
        myfile << pendulum.get()->getPosition().x() << "," << pendulum.get()->getPosition().y() << "," << pendulum.get()->getPosition().z()<< "," <<
               pendulum.get()->absForce.x() <<"," << pendulum.get()->absForce.y() << "," << pendulum.get()->absForce.z() << "\n";
        DynamicWorld.solveDynamics(DynamicWorld.getCurrentTime());


    }

    auto finish = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << " s\n";

    auto motor = DSSMotor();





    return 0;

}

